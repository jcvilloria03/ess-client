# salarium-ess

> Salarium Employee Self Service

## Build Setup properly

``` bash
# copy environment file for your machine
cp .env.example .env

# modify your environment file
vim .env

or open it with your preferred text editor

# install dependencies 
npm install

# serve with hot reload at localhost:3000
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all testss
npm test
```


