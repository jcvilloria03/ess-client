'use strict'
const merge = require( 'webpack-merge' );
const prodEnv = require( './prod.env' );

module.exports = merge( prodEnv, {
    NODE_ENV: '"development"',
    ASSETS_PUBLIC_PATH: JSON.stringify( '/' )
});
