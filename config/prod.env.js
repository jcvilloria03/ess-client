'use strict'
module.exports = {
    NODE_ENV: '"production"',
    API_URL: JSON.stringify( process.env.API_URL ),
    AUTHN_API_BASE_URI: JSON.stringify( process.env.AUTHN_API_BASE_URI ),
    AUTH_DOMAIN: JSON.stringify( process.env.AUTH_DOMAIN ),
    AUTH_CLIENT_ID: JSON.stringify( process.env.AUTH_CLIENT_ID ),
    SOCKETS_URL: JSON.stringify( process.env.SOCKETS_URL ),
    ASSETS_PUBLIC_PATH: JSON.stringify( process.env.ASSETS_PUBLIC_PATH || '/' ),
    TA_URL: JSON.stringify( process.env.TA_URL || '/' ),
    SESSION_TIMEOUT_COUNTDOWN_SECONDS: JSON.stringify( process.env.SESSION_TIMEOUT_COUNTDOWN_SECONDS ),
    SESSION_IDLE_TIME_MS: JSON.stringify( process.env.SESSION_IDLE_TIME_MS ),
    INLINEMANUAL_API_KEY: JSON.stringify(process.env.INLINEMANUAL_API_KEY)
};
