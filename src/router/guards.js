import Vue from 'vue';
import { auth } from '@/utils/Authentication';
import modalCollection from '@/components/mobile/modal/Collection';
import { open } from '@/utils/helpers/urlHelpers';
import each from 'lodash-es/each';
import modulesMap from '../modules_map';

/**
 * ensures no login is set
 * @param to
 */
export function guestPage( to ) {
    const authenticatedAndEmployee = to.matched.some( ( record ) => record.meta.guestPage )
        && auth.isAuthenticated()
        && auth.isEmployee();

    if ( authenticatedAndEmployee ) {
        return Promise.reject({ name: 'dashboard' });
    }

    return Promise.resolve( to );
}

/**
 * checks page for authentication
 * @param to
 */
export async function requiresAuth( to ) {
    if ( to.matched.some( ( record ) => record.meta.requiresAuth ) ) {
        if ( !auth.isAuthenticated() ) {
            auth.clearLocalStorageExceptMessage();

            return Promise.reject({ name: 'login' });
        }

        auth.setAuthToken();

        if ( auth.userShouldChangePassword() ) {
            open( '/change-password' );
        }

        if ( !auth.hasPermissions() ) {
            await auth.fetchPermissions().then( ( permission ) => {
                auth.setPermissions( permission );
            }).catch( () => Promise.reject( '/dashboard' ) );
        }

        if ( !auth.isEmployee() || !auth.getUser() ) {
            await auth.fetchUser().then( ( user ) => {
                auth.setUserData( user );
            }).catch( () => Promise.reject( '/dashboard' ) );
        }
    }

    return Promise.resolve( to );
}

/**
 * checks auth0 account if email is verified
 * @param to
 * @param next
 */
export function requiresAccountToBeVerified( to ) {
    if ( to.matched.some( ( record ) => record.meta.requiresAccountToBeVerified ) && !auth.isEmailVerified() ) {
        return Promise.reject( '/verify' );
    }

    return Promise.resolve( to );
}

/**
 * checks if user is a team leader
 * @param  {Object}   to   destination route
 */
export async function requiresAccountToBeTeamLeader( to ) {
    const teamLeader = await auth.isTeamLeader();

    if ( to.matched.some( ( record ) => record.meta.requiresAccountToBeTeamLeader && !teamLeader ) ) {
        return Promise.reject({ name: 'dashboard' });
    }

    return Promise.resolve( to );
}

/**
 * check CRBAC permissions and if user has permissions let him go, if not, send him to unauthorized page
 * @param  {Object}   to   destination route
 */
export function checkPermissions( to ) {
    return new Promise( ( resolve, reject ) => {
        const [matchedRoute] = to.matched.slice( -1 );

        if ( matchedRoute !== undefined && matchedRoute.path !== '/unauthorized' ) {
            const modules = [];

            each( modulesMap, ( item, index ) => {
                if ( matchedRoute.path.includes( index ) ) {
                    modules.push( item );
                }
            });

            const authzPermissions = JSON.parse( localStorage.getItem( 'authz_permissions' ) );
            const authorized = authzPermissions.includes( modules.pop() );

            if ( authorized ) {
                resolve( to );
            } else {
                reject({ name: 'unauthorized' });
            }
        }

        resolve( to );
    });
}

/**
 * checks if user has permission to assign and edit shift
 * @param  {Object}   to   destination route
 */
export async function requiresLeaderToHaveAssignOrEditPermission( to ) {
    const permission = await auth.hasAssignOrEditPermission( to.params.teamId );

    if ( to.matched.some( ( record ) => record.meta.requiresLeaderToHaveAssignOrEditPermission && !permission ) ) {
        Vue.prototype.$flashMessage.show( 'Leader has no permission to assign or edit shifts.', 'error' );

        return Promise.reject( '/teams' );
    }

    return Promise.resolve( to );
}

/**
 * Clear all modals.
 */
export function clearModals() {
    modalCollection.clear();
}
