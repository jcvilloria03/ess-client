import Vue from 'vue';
import Router from 'vue-router';
import PageNotFound from '@/pages/ErrorPages/404';
import Notifications from '@/pages/Notifications';
import Dashboard from '@/pages/dashboard/Dashboard';
import Unauthorized from '@/pages/Unauthorized';
import Approvals from '@/pages/approvals/Approvals';
import ApprovalsList from '@/pages/approvals/ApprovalsList';
import Requests from '@/pages/requests/Requests';
import RequestList from '@/pages/requests/RequestList';
import LeaveRequest from '@/pages/requests/LeaveRequest';
import OvertimeRequest from '@/pages/requests/OvertimeRequest';
import UndertimeRequest from '@/pages/requests/UndertimeRequest';
import TimeDisputeRequest from '@/pages/requests/TimeDisputeRequest';
import ShiftChangeRequest from '@/pages/requests/ShiftChangeRequest';
import LeaveRequestDetails from '@/pages/requests/LeaveRequestDetails';
import OvertimeRequestDetails from '@/pages/requests/OvertimeRequestDetails';
import UndertimeRequestDetails from '@/pages/requests/UndertimeRequestDetails';
import TimeDisputeRequestDetails from '@/pages/requests/TimeDisputeRequestDetails';
import ShiftChangeRequestDetails from '@/pages/requests/ShiftChangeRequestDetails';
import AddLeaveRequest from '@/pages/AddRequests/AddLeaveRequest';
import AddOvertimeRequest from '@/pages/AddRequests/AddOvertimeRequest';
import AddUndertimeRequest from '@/pages/AddRequests/AddUndertimeRequest';
import AddTimeDisputeRequest from '@/pages/AddRequests/AddTimeDisputeRequest';
import AddShiftChangeRequest from '@/pages/AddRequests/AddShiftChangeRequest';
import Announcements from '@/pages/announcements/Announcements';
import AnnouncementList from '@/pages/announcements/AnnouncementList';
import MyAnnouncements from '@/pages/announcements/MyAnnouncements';
import OtherAnnouncements from '@/pages/announcements/OtherAnnouncements';
import CreateAnnouncement from '@/pages/announcements/CreateAnnouncement';
import MyAnnouncementDetails from '@/pages/announcements/MyAnnouncementDetails';
import MyViews from '@/pages/announcements/MyViews';
import MyResponses from '@/pages/announcements/MyResponses';
import OtherAnnouncementDetails from '@/pages/announcements/OtherAnnouncementDetails';
import AnnouncementReply from '@/pages/announcements/AnnouncementReply';
import Attendance from '@/pages/attendance/Attendance';
import Payslips from '@/pages/payslips/Payslips';
import Teams from '@/pages/teams/Teams';
import TeamList from '@/pages/teams/TeamList';
import TeamView from '@/pages/teams/TeamView';
import MemberView from '@/pages/teams/TeamMemberView';
import AssignView from '@/pages/teams/TeamAssignView';
import Calendar from '@/pages/calendar/Calendar';
import Messages from '@/pages/requests/Messages';
import Layout from '@/pages/Layout';
import Profile from '@/pages/profile/Profile';
import ChangePassword from '@/pages/profile/ChangePassword';

import { open } from '@/utils/helpers/urlHelpers';

import {
    requiresAuth,
    requiresAccountToBeVerified,
    requiresAccountToBeTeamLeader,
    requiresLeaderToHaveAssignOrEditPermission,
    checkPermissions,
    guestPage,
    clearModals
} from './guards';

Vue.use( Router );

const router = new Router({
    base: process.env.ASSETS_PUBLIC_PATH,
    mode: 'history',
    routes: [
        {
            path: '',
            component: Layout,
            redirect: '/dashboard',
            children: [
                {
                    path: '/dashboard',
                    name: 'dashboard',
                    component: Dashboard,
                    meta: {
                        requiresAuth: true,
                        requiresAccountToBeVerified: true,
                        showUnreadAnnouncements: true
                    }
                },
                {
                    path: '/unauthorized',
                    name: 'unauthorized',
                    component: Unauthorized,
                    meta: {
                        requiresAuth: true,
                        requiresAccountToBeVerified: true,
                        title: 'Unauthorized'
                    }
                },
                {
                    path: '/profile',
                    name: 'profile',
                    component: Profile,
                    meta: {
                        requiresAuth: true,
                        requiresAccountToBeVerified: true
                    }
                },
                {
                    path: '/change-password',
                    name: 'change-password',
                    component: ChangePassword,
                    meta: {
                        requiresAuth: true,
                        requiresAccountToBeVerified: true
                    }
                },
                {
                    path: '/notifications',
                    component: Notifications,
                    meta: {
                        requiresAuth: true,
                        requiresAccountToBeVerified: true
                    }
                },
                {
                    path: '/approvals',
                    component: Approvals,
                    meta: {
                        requiresAuth: true,
                        requiresAccountToBeVerified: true,
                        showUnreadAnnouncements: true
                    },
                    children: [
                        {
                            path: '',
                            name: 'approvals.index',
                            component: ApprovalsList
                        },
                        {
                            path: ':requestId',
                            name: 'approvals.leave-request.view',
                            component: ApprovalsList,
                            props: true,
                            children: [
                                {
                                    path: 'details',
                                    name: 'approvals.leave-request.details',
                                    component: ApprovalsList,
                                    props: true,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true,
                                        showUnreadAnnouncements: true
                                    }
                                },
                                {
                                    path: 'messages',
                                    name: 'approvals.leave-request.messages',
                                    component: ApprovalsList,
                                    props: true,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true,
                                        showUnreadAnnouncements: true
                                    }
                                }
                            ]
                        },
                        {
                            path: ':requestId',
                            name: 'approvals.overtime-request.view',
                            component: ApprovalsList,
                            props: true,
                            children: [
                                {
                                    path: 'details',
                                    name: 'approvals.overtime-request.details',
                                    component: ApprovalsList,
                                    props: true,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true,
                                        showUnreadAnnouncements: true
                                    }
                                },
                                {
                                    path: 'messages',
                                    name: 'approvals.overtime-request.messages',
                                    component: ApprovalsList,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true,
                                        showUnreadAnnouncements: true
                                    }
                                }
                            ]
                        },
                        {
                            path: ':requestId',
                            name: 'approvals.time-dispute-request.view',
                            component: ApprovalsList,
                            props: true,
                            children: [
                                {
                                    path: 'details',
                                    name: 'approvals.time-dispute-request.details',
                                    component: ApprovalsList,
                                    props: true,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true,
                                        showUnreadAnnouncements: true
                                    }
                                },
                                {
                                    path: 'messages',
                                    name: 'approvals.time-dispute-request.messages',
                                    component: ApprovalsList,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true,
                                        showUnreadAnnouncements: true
                                    }
                                }
                            ]
                        },
                        {
                            path: ':requestId',
                            name: 'approvals.shift-change-request.view',
                            component: ApprovalsList,
                            props: true,
                            children: [
                                {
                                    path: 'details',
                                    name: 'approvals.shift-change-request.details',
                                    component: ApprovalsList,
                                    props: true,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true,
                                        showUnreadAnnouncements: true
                                    }
                                },
                                {
                                    path: 'messages',
                                    name: 'approvals.shift-change-request.messages',
                                    component: ApprovalsList,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true,
                                        showUnreadAnnouncements: true
                                    }
                                }
                            ]
                        },
                        {
                            path: ':requestId',
                            name: 'approvals.undertime-request.view',
                            component: ApprovalsList,
                            props: true,
                            children: [
                                {
                                    path: 'details',
                                    name: 'approvals.undertime-request.details',
                                    component: ApprovalsList,
                                    props: true,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true,
                                        showUnreadAnnouncements: true
                                    }
                                },
                                {
                                    path: 'messages',
                                    name: 'approvals.undertime-request.messages',
                                    component: ApprovalsList,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true,
                                        showUnreadAnnouncements: true
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    path: '/requests',
                    component: Requests,
                    meta: {
                        requiresAuth: true,
                        requiresAccountToBeVerified: true,
                        showUnreadAnnouncements: true
                    },
                    children: [
                        {
                            path: '',
                            name: 'requests.index',
                            component: RequestList
                        },
                        {
                            path: 'leave-request/add',
                            name: 'requests.leave-request.add',
                            component: AddLeaveRequest,
                            meta: {
                                requiresAuth: true,
                                requiresAccountToBeVerified: true,
                                showUnreadAnnouncements: true
                            }
                        },
                        {
                            path: 'overtime-request/add',
                            name: 'requests.overtime.add',
                            component: AddOvertimeRequest,
                            props: true
                        },
                        {
                            path: 'time-correction-request/add',
                            name: 'requests.time-dispute.add',
                            component: AddTimeDisputeRequest,
                            props: true
                        },
                        {
                            path: 'shift-change-request/add',
                            name: 'requests.shift-change.add',
                            component: AddShiftChangeRequest,
                            props: true
                        },
                        {
                            path: 'undertime-request/add',
                            name: 'requests.undertime.add',
                            component: AddUndertimeRequest,
                            props: true
                        },
                        {
                            path: 'leave-request/:requestId',
                            name: 'requests.leave-request.view',
                            component: LeaveRequest,
                            props: true,
                            children: [
                                {
                                    path: 'details',
                                    name: 'requests.leave-request.details',
                                    component: LeaveRequestDetails,
                                    props: true,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true,
                                        showUnreadAnnouncements: true
                                    }
                                },
                                {
                                    path: 'messages',
                                    name: 'requests.leave-request.messages',
                                    component: Messages,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true,
                                        showUnreadAnnouncements: true
                                    }
                                }
                            ]
                        },
                        {
                            path: 'overtime-request/:requestId',
                            name: 'requests.overtime-request.view',
                            component: OvertimeRequest,
                            props: true,
                            children: [
                                {
                                    path: 'details',
                                    name: 'requests.overtime-request.details',
                                    component: OvertimeRequestDetails,
                                    props: true,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true,
                                        showUnreadAnnouncements: true
                                    }
                                },
                                {
                                    path: 'messages',
                                    name: 'requests.overtime-request.messages',
                                    component: Messages,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true,
                                        showUnreadAnnouncements: true
                                    }
                                }
                            ]
                        },
                        {
                            path: 'time-correction-request/:requestId',
                            name: 'requests.time-dispute-request.view',
                            component: TimeDisputeRequest,
                            props: true,
                            children: [
                                {
                                    path: 'details',
                                    name: 'requests.time-dispute-request.details',
                                    component: TimeDisputeRequestDetails,
                                    props: true,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true,
                                        showUnreadAnnouncements: true
                                    }
                                },
                                {
                                    path: 'messages',
                                    name: 'requests.time-dispute-request.messages',
                                    component: Messages,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true,
                                        showUnreadAnnouncements: true
                                    }
                                }
                            ]
                        },
                        {
                            path: 'shift-change-request/:requestId',
                            name: 'requests.shift-change-request.view',
                            component: ShiftChangeRequest,
                            props: true,
                            children: [
                                {
                                    path: 'details',
                                    name: 'requests.shift-change-request.details',
                                    component: ShiftChangeRequestDetails,
                                    props: true,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true,
                                        showUnreadAnnouncements: true
                                    }
                                },
                                {
                                    path: 'messages',
                                    name: 'requests.shift-change-request.messages',
                                    component: Messages,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true,
                                        showUnreadAnnouncements: true
                                    }
                                }
                            ]
                        },
                        {
                            path: 'undertime-request/:requestId',
                            name: 'requests.undertime-request.view',
                            component: UndertimeRequest,
                            props: true,
                            children: [
                                {
                                    path: 'details',
                                    name: 'requests.undertime-request.details',
                                    component: UndertimeRequestDetails,
                                    props: true,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true,
                                        showUnreadAnnouncements: true
                                    }
                                },
                                {
                                    path: 'messages',
                                    name: 'requests.undertime-request.messages',
                                    component: Messages,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true,
                                        showUnreadAnnouncements: true
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    path: '/announcements',
                    component: Announcements,
                    props: true,
                    meta: {
                        requiresAuth: true,
                        requiresAccountToBeVerified: true
                    },
                    children: [
                        {
                            path: '',
                            name: 'announcements.index',
                            component: AnnouncementList,
                            redirect: '/announcements/my-announcements',
                            children: [
                                {
                                    path: 'my-announcements',
                                    name: 'announcements.my-announcements',
                                    component: MyAnnouncements,
                                    props: true,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true,
                                        showUnreadAnnouncements: true
                                    }
                                },
                                {
                                    path: 'other-announcements',
                                    name: 'announcements.other-announcements',
                                    component: OtherAnnouncements,
                                    props: true,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true,
                                        showUnreadAnnouncements: true
                                    }
                                }
                            ]
                        },
                        {
                            path: 'publish',
                            name: 'announcements.create-announcement',
                            component: CreateAnnouncement,
                            meta: {
                                showUnreadAnnouncements: true
                            }
                        },
                        {
                            path: 'my-announcements/:announcementId',
                            name: 'announcements.my-announcements.details',
                            props: true,
                            component: MyAnnouncementDetails,
                            meta: {
                                showUnreadAnnouncements: true
                            },
                            children: [
                                {
                                    path: 'views',
                                    name: 'announcements.my-announcements.views',
                                    component: MyViews,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true
                                    }
                                },
                                {
                                    path: 'responses',
                                    name: 'announcements.my-announcements.responses',
                                    component: MyResponses,
                                    meta: {
                                        requiresAuth: true,
                                        requiresAccountToBeVerified: true
                                    }
                                }
                            ]
                        },
                        {
                            path: 'my-announcements/:announcementId/reply/:replyId',
                            name: 'announcements.reply',
                            component: AnnouncementReply,
                            props: true,
                            meta: {
                                showUnreadAnnouncements: true
                            }
                        },
                        {
                            path: 'other-announcement/:announcementId',
                            name: 'announcements.other-announcements.details',
                            props: true,
                            component: OtherAnnouncementDetails,
                            meta: {
                                requiresAuth: true,
                                requiresAccountToBeVerified: true
                            }
                        }
                    ]
                },
                {
                    path: '/attendance',
                    name: 'attendance',
                    component: Attendance,
                    meta: {
                        requiresAuth: true,
                        requiresAccountToBeVerified: true,
                        showUnreadAnnouncements: true
                    }
                },
                {
                    path: '/payslips',
                    name: 'payslips',
                    component: Payslips,
                    meta: {
                        requiresAuth: true,
                        requiresAccountToBeVerified: true
                    }
                },
                {
                    path: '/teams',
                    component: Teams,
                    meta: {
                        requiresAuth: true,
                        requiresAccountToBeVerified: true,
                        requiresAccountToBeTeamLeader: true
                    },
                    beforeEnter: ( to, from, next ) => {
                        requiresAccountToBeTeamLeader( to )
                        .then( () => {
                            next();
                        })
                        .catch( ( redirect ) => {
                            next( redirect );
                        });
                    },
                    children: [
                        {
                            path: '',
                            name: 'team-list',
                            props: true,
                            component: TeamList,
                            meta: {
                                requiresAccountToBeTeamLeader: true
                            }
                        },
                        {
                            path: ':teamId',
                            name: 'team-view',
                            props: true,
                            component: TeamView,
                            meta: {
                                requiresAccountToBeTeamLeader: true
                            }
                        },
                        {
                            path: ':teamId/member/:memberId/selected_date=:selectedDate',
                            name: 'member-view',
                            props: true,
                            component: MemberView,
                            meta: {
                                requiresAccountToBeTeamLeader: true
                            }
                        },
                        {
                            path: ':teamId/member/:memberId/assign_shift/:selectedDate',
                            name: 'assign-view',
                            props: true,
                            component: AssignView,
                            meta: {
                                requiresAccountToBeTeamLeader: true,
                                requiresLeaderToHaveAssignOrEditPermission: true
                            },
                            beforeEnter: ( to, from, next ) => {
                                requiresLeaderToHaveAssignOrEditPermission( to )
                                .then( () => {
                                    next();
                                })
                                .catch( ( redirect ) => {
                                    next( redirect );
                                });
                            }
                        }]
                },
                {
                    path: '/calendar',
                    name: 'calendar',
                    component: Calendar,
                    meta: {
                        requiresAuth: true,
                        requiresAccountToBeVerified: true,
                        showUnreadAnnouncements: true
                    }
                }
            ]
        },
        {
            path: '*',
            component: PageNotFound
        }
    ]
});

if ( process.env.NODE_ENV === 'development' ) {
    router.addRoutes([
        {
            path: '/login',
            name: 'login',
            component: require( '../pages/Auth/Login.vue' ).default, // eslint-disable-line global-require,
            meta: {
                guestPage: true
            }
        },
        {
            path: '/styleguide',
            component: require( '../pages/Styleguide.vue' ).default // eslint-disable-line global-require
        }
    ]);
}

router.beforeEach( ( to, from, next ) => {
    clearModals();

    // Start "middleware" chain
    Promise.resolve( to )
        .then( guestPage )
        .then( requiresAuth )
        .then( requiresAccountToBeVerified )
        .then( checkPermissions )
        .then( () => {
            next();
        }).catch( ( redirect ) => {
            if ( redirect.name === 'login' && process.env.NODE_ENV !== 'development' ) {
                open( '/login' );
                return;
            }

            if ( redirect === '/dashboard' ) {
                open( redirect );
                return;
            }

            next( redirect );
        });
});

router.afterEach( () => {
    Vue.prototype.$flashMessage.showQueued();
});

export default router;
