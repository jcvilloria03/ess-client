import Approvals from './Approvals';
import FiledRequests from './FiledRequests';
import Calendar from './Calendar';

/**
 * "istanbul ignore next" to exlude this area from coverage. at least until "ignore file" or "ignore start/stop" is merged to a stable branch of istanbul.
 */
const DashboardComponents = {
    install( Vue ) {
        /* istanbul ignore next */
        Vue.component( FiledRequests.name, FiledRequests );
        /* istanbul ignore next */
        Vue.component( Approvals.name, Approvals );
        /* istanbul ignore next */
        Vue.component( Calendar.name, Calendar );
    }
};

export default DashboardComponents;
