import FlashMessageList from './FlashMessageList';

const body = document.querySelector( 'body' );

export const STORAGE_KEY = 'flash_message_queue';

export default {
    install( Vue ) {
        Vue.component( 'sal-flash-message-list-mobile', FlashMessageList );

        const FlashMessageComponentConstructor = Vue.extend({
            render( createElement ) {
                return createElement( 'sal-flash-message-list-mobile', {
                    props: {
                        messages: this.messages,
                        onClose: this.hide.bind( this ),
                        onDownload: this.download.bind( this )
                    }
                });
            },

            data() {
                return {
                    element: null,
                    messages: []
                };
            },

            methods: {
                /**
                 * Prepare flash message object.
                 * @param {String} message
                 * @param {String} type
                 * @param {Object} options
                 * @return {Object}
                 */
                prepareMessage( message, type, options ) {
                    return {
                        message,
                        type,
                        options: Object.assign({
                            timeout: 3000,
                            closable: false,
                            showIcon: false,
                            donwloadURI: null
                        }, options )
                    };
                },

                /**
                 * Flash our alert to the screen for the user to see
                 * and begin the process to hide it.
                 * @param {String} message
                 * @param {String} type
                 * @param {Object} options
                 */
                show( text, type, options = {}) {
                    this.showPrepared( this.prepareMessage( text, type, options ) );
                },

                /**
                 * Show prepared flash message.
                 * @param {Object} message
                 */
                showPrepared( message ) {
                    this.messages.push( message );

                    setTimeout( () => {
                        this.hide( message );
                    }, message.options.timeout );
                },

                /**
                 * Store message to show it later.
                 * @param {String} message
                 * @param {String} type
                 * @param {Object} options
                 */
                queue( text, type, options = {}) {
                    const message = this.prepareMessage( text, type, options );

                    localStorage.setItem( STORAGE_KEY, JSON.stringify( message ) );
                },

                /**
                 * Show queued flash message.
                 */
                showQueued() {
                    const message = JSON.parse( localStorage.getItem( STORAGE_KEY ) );

                    localStorage.removeItem( STORAGE_KEY );

                    if ( message ) {
                        this.showPrepared( message );
                    }
                },

                download( item ) {
                    const link = document.createElement( 'a' );

                    link.href = item;
                    link.setAttribute( 'download', '' );

                    document.body.appendChild( link );
                    link.click();

                    document.body.removeChild( link );
                },

                /**
                 * Hide Our Alert
                 * @param {Object} item
                 */
                hide( item ) {
                    const index = item ? this.messages.indexOf( item ) : 0;

                    if ( index >= 0 ) {
                        this.messages.splice( index, 1 );
                    }

                    if ( this.messages.length === 0 && body.contains( this.element ) ) {
                        body.removeChild( this.element );
                    }
                },

                closeAll() {
                    this.messages = [];

                    if ( body.contains( this.element ) ) {
                        body.removeChild( this.element );
                    }
                }
            }
        });

        const flashMessageComponent = new FlashMessageComponentConstructor({ el: document.createElement( 'div' ) });

        Vue.prototype.$flashMessage = {
            /**
             * Show flash message (now).
             * @param {String} message
             * @param {String} type
             * @param {Object} options
             */
            show( message, type, options ) {
                flashMessageComponent.element = body.appendChild( flashMessageComponent.$el );

                flashMessageComponent.show( message, type, options );
            },

            /**
             * Show queued flash message.
             */
            showQueued() {
                flashMessageComponent.element = body.appendChild( flashMessageComponent.$el );

                flashMessageComponent.showQueued();
            },

            /**
             * Queue flash message to show it later.
             * @param {String} message
             * @param {String} type
             * @param {Object} options
             */
            queue( message, type, options ) {
                flashMessageComponent.queue( message, type, options );
            },

            /**
             * Close all flash messages.
             */
            closeAll() {
                flashMessageComponent.closeAll();
            }
        };
    }
};
