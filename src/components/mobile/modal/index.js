import Vue from 'vue';
import DialogModalComponent from './DialogModal';
import collection from './Collection';

/**
 * { Opens Modal component }
 *
 * @param {Object} propsData  The properties data
 * @param {DialogModalComponent} componentConstructor  The component constructor
 * @return {ModalComponent}  Returns new ModalComponent
 */
function open( propsData, componentConstructor = DialogModalComponent ) {
    const ModalComponent = Vue.extend( componentConstructor );

    return collection.push( new ModalComponent({
        el: document.createElement( 'div' ),
        propsData
    }) );
}

export const DialogModal = {

    open( params, componentConstructor = DialogModalComponent ) {
        const defaultParam = { title: '', content: '', type: 'primary', showCancel: false };
        const propsData = Object.assign( defaultParam, params );
        return open( propsData, componentConstructor );
    },

    confirm( params ) {
        const defaultParam = { title: 'Confirm', content: '', type: 'primary' };
        const propsData = Object.assign( defaultParam, params );
        return open( propsData );
    },

    danger( params ) {
        const defaultParam = { title: 'Alert', content: '', type: 'danger' };
        const propsData = Object.assign( defaultParam, params );
        return open( propsData );
    }
};
