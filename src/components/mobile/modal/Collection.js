/**
 * Collection of modals
 */
function ModalCollection() {
    this.modals = [];
}

/**
 * Add modal to the collection.
 * @param {DialogModal} modal
 */
/* e */
ModalCollection.prototype.push = function ( modal ) { // eslint-disable-line func-names
    this.modals.push( modal );

    return modal;
};

/**
 * Clear all modals.
 */
ModalCollection.prototype.clear = function () { // eslint-disable-line func-names
    this.modals.forEach( ( modal ) => modal.$destroy() );

    this.modals = [];
};

export default new ModalCollection();
