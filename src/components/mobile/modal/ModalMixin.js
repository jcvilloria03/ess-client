const ModalMixin = {
    props: {
        isShow: {
            type: Boolean,
            default: false
        },
        title: {
            type: String
        },
        okText: {
            type: String,
            default: 'OK'
        },
        cancelText: {
            type: String,
            default: 'Cancel'
        },
        onOk: {
            type: Function,
            default() {}
        },
        onCancel: {
            type: Function,
            default() {}
        },
        width: {
            type: Number,
            default: 400
        },
        showOk: {
            type: Boolean,
            default: true
        },
        showCancel: {
            type: Boolean,
            default: true
        },
        showHeader: {
            type: Boolean,
            default: true
        },
        showFooter: {
            type: Boolean,
            default: true
        }
    },

    data() {
        return {
            isActive: false
        };
    },

    computed: {
        modalWidth() {
            if ( this.width !== 400 && this.width !== 0 ) {
                return { width: `${this.width}px` };
            }

            return null;
        }
    },

    watch: {
        isShow( val ) {
            this.isActive = val;
        }
    },

    mounted() {
        this.$nextTick( () => {
            document.body.appendChild( this.$el );
            if ( this.isShow ) {
                this.active();
            }
        });
    },

    beforeDestroy() {
        this.$el.remove();
    },

    methods: {
        active() {
            this.isActive = true;
        },

        handleOk() {
            this.onOk();
            this.handleClose();
        },

        handleCancel() {
            this.onCancel();
            this.handleClose();
        },

        handleClose() {
            setTimeout( () => {
                this.$destroy();
            }, 100 );
        },

        handleBackgroundClick( evt ) {
            evt.stopPropagation();
        }
    }
};

export default ModalMixin;
