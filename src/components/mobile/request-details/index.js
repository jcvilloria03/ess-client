import RequestTrail from './RequestTrail';

/**
 * "istanbul ignore next" to exlude this area from coverage. at least until "ignore file" or "ignore start/stop" is merged to a stable branch of istanbul.
 */
const RequestDetailsComponents = {
    install( Vue ) {
        /* istanbul ignore next */
        Vue.component( RequestTrail.name, RequestTrail );
    }
};

export default RequestDetailsComponents;
