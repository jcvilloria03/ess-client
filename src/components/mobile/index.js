import Multiselect from 'vue-multiselect';

import Input from './Input';
import Button from './Button';
import A from './A';
import Checkbox from './Checkbox';
import Radio from './Radio';
import Textarea from './Textarea';
import Select from './Select';

import Notification from './Notification';
import Loader from './Loader';
import Progress from './Progress';
import Menu from './Menu';
import Sidebar from './Sidebar';
import NotificationsList from './NotificationsList';
import Help from './Help';
import Employee from './Employee';
import CheckSalPay from './CheckSalPay';
import MessagesFeed from './request-messages/MessagesFeed';
import Message from './request-messages/Message';
import InitialMessagesFeed from './initial-request-message/InitialMessagesFeed';
import InitialMessage from './initial-request-message/InitialMessage';
import MessageInput from './request-messages/MessageInput';
import Back from './Back';
import Card from './Card';
import ApprovalCard from './approvals/ApprovalCard';
import ApprovalActions from './approvals/ApprovalActions';
import LeaveDetails from './requests/LeaveDetails';
import OvertimeDetails from './requests/OvertimeDetails';
import UndertimeDetails from './requests/UndertimeDetails';
import TimeDisputeDetails from './requests/TimeDisputeDetails';
import TimeDisputeDetailsCard from './requests/TimeDisputeDetailsCard';
import ShiftChangeDetails from './requests/ShiftChangeDetails';
import LeaveRequestCard from './requests/LeaveRequestCard';
import OvertimeRequestCard from './requests/OvertimeRequestCard';
import UndertimeRequestCard from './requests/UndertimeRequestCard';
import TimeDisputeRequestCard from './requests/TimeDisputeRequestCard';
import ShiftChangeRequestCard from './requests/ShiftChangeRequestCard';
import AddRequestDetailsCard from './requests/AddRequestDetailsCard';
import AddRequestDetails from './requests/AddRequestDetails';
import AddLeaveRequestDetails from './requests/AddLeaveRequestDetails';
import LeaveRequestApprovalCard from './approvals/LeaveRequestApprovalCard';
import OvertimeRequestApprovalCard from './approvals/OvertimeRequestApprovalCard';
import UndertimeRequestApprovalCard from './approvals/UndertimeRequestApprovalCard';
import OvertimeEditRequestCard from './requests/OvertimeEditRequestCard';
import UndertimeEditRequestCard from './requests/UndertimeEditRequestCard';
import TimeDisputeRequestApprovalCard from './approvals/TimeDisputeRequestApprovalCard';
import ShiftChangeRequestApprovalCard from './approvals/ShiftChangeRequestApprovalCard';
import TimeDisputeEditRequestCard from './requests/TimeDisputeEditRequestCard';
import AddHoursWorkForm from './requests/AddHoursWorkForm';
import AddTimeRecordForm from './requests/AddTimeRecordForm';
import QuickAdd from './requests/QuickAdd';
import GlobalLoader from './GlobalLoader';
import DateRangePicker from './DateRangePicker';
import RequestFilters from './requests/RequestFilters';
import ApprovalFilters from './approvals/ApprovalFilters';
import CalendarShiftCard from './calendar/CalendarShiftCard';
import CalendarLeaveCard from './calendar/CalendarLeaveCard';
import CalendarFilters from './calendar/CalendarFilters';
import FlashMessages from './flash-messages';
import FileUpload from './FileUpload';
import MyAnnouncementCard from './announcements/MyAnnouncementCard';
import OtherAnnouncementCard from './announcements/OtherAnnouncementCard';
import AnnouncementFilters from './announcements/AnnouncementFilters';
import AnnouncementFiltersContainer from './announcements/AnnouncementFiltersContainer';
import AnnouncementsOverlay from './announcements/AnnouncementsOverlay';
import Overlay from './Overlay';
import RequestDetailsAttachment from './RequestDetailsAttachment';
import RequestDetailsComponents from './request-details/index';
// import DashboardComponents from './dashboard/index';
import TimeClock from './timeclock/TimeClock';
import TimeClockTags from './timeclock/TimeClockTags';
import RequestTimeRecordTags from './timeclock/RequestTimeRecordTags';
import RequestTagsInput from './RequestTagsInput';
import DatePicker from './DatePicker';
import MultiselectDropdown from './MultiselectDropdown';
import ProfileSection from './profile/ProfileSection';
import TeamCard from './teams/TeamCard';
import TeamTimeClock from './teams/TeamTimeClock';
import TeamAssignShift from './teams/TeamAssignShift';
import TeamAssignRestDay from './teams/TeamAssignRestDay';
import TeamViewCalendar from './teams/TeamViewCalendar';
import Pill from './Pill';
import Tooltip from './Tooltip';
import ValidationMessage from './ValidationMessage';
import HorizontalCalendar from './HorizontalCalendar';
import ApprovalItem from './approvals/ApprovalItem';
import ApprovalSidePanelDetail from './approvals/ApprovalSidePanelDetail';
import SidePanelTabsLeave from './approvals/SidePanelTabsLeave';
import SidePanelTabsUndertime from './approvals/SidePanelTabsUndertime';
import SidePanelTabsOvertime from './approvals/SidePanelTabsOvertime';
import SidePanelTabsTimeDispute from './approvals/SidePanelTabsTimeDispute';
import SidePanelTabsShiftChange from './approvals/SidePanelTabsShiftChange';

/**
 * "istanbul ignore next" to exlude this area from coverage. at least until "ignore file" or "ignore start/stop" is merged to a stable branch of istanbul.
 */
const Components = {
    install( Vue ) {
        /* istanbul ignore next */
        Vue.component( 'sal-input-mobile', Input );
        /* istanbul ignore next */
        Vue.component( 'sal-button-mobile', Button );
        /* istanbul ignore next */
        Vue.component( 'sal-a-mobile', A );
        /* istanbul ignore next */
        Vue.component( 'sal-checkbox-mobile', Checkbox );
        /* istanbul ignore next */
        Vue.component( 'sal-radio-mobile', Radio );
        /* istanbul ignore next */
        Vue.component( 'sal-textarea-mobile', Textarea );
        /* istanbul ignore next */
        Vue.component( 'sal-select-mobile', Select );
        /* istanbul ignore next */
        Vue.component( 'sal-notify-mobile', Notification );
        /* istanbul ignore next */
        Vue.component( 'sal-mask', Loader );
        /* istanbul ignore next */
        Vue.component( 'sal-menu', Menu );
        /* istanbul ignore next */
        Vue.component( 'sal-sidebar-mobile', Sidebar );
        /* istanbul ignore next */
        Vue.component( 'sal-notifications-list', NotificationsList );
        /* istanbul ignore next */
        Vue.component( 'sal-help-mobile', Help );
        /* istanbul ignore next */
        Vue.component( 'sal-employee-mobile', Employee );
        /* istanbul ignore next */
        Vue.component( 'sal-check-sal-pay-mobile', CheckSalPay );
        /* istanbul ignore next */
        Vue.component( 'sal-messages-feed', MessagesFeed );
        /* istanbul ignore next */
        Vue.component( 'sal-message', Message );
        /* istanbul ignore next */
        Vue.component( 'sal-message-input', MessageInput );
        /* istanbul ignore next */
        Vue.component( 'sal-back', Back );
        /* istanbul ignore next */
        Vue.component( 'sal-card', Card );
        /* istanbul ignore next */
        Vue.component( 'sal-approval-card-mobile', ApprovalCard );
        /* istanbul ignore next */
        Vue.component( 'sal-leave-details', LeaveDetails );
        /* istanbul ignore next */
        Vue.component( 'sal-leave-request-card-mobile', LeaveRequestCard );
        /* istanbul ignore next */
        Vue.component( 'sal-leave-request-approval-card-mobile', LeaveRequestApprovalCard );
        /* istanbul ignore next */
        Vue.component( 'sal-quick-add', QuickAdd );
        /* istanbul ignore next */
        Vue.component( 'sal-date-range-picker', DateRangePicker );
        /* istanbul ignore next */
        Vue.component( 'sal-request-filters', RequestFilters );
        /* istanbul ignore next */
        Vue.component( 'sal-add-leave-request-details', AddLeaveRequestDetails );
        /* istanbul ignore next */
        Vue.component( 'sal-add-request-details', AddRequestDetails );
        /* istanbul ignore next */
        Vue.component( 'sal-approval-filters', ApprovalFilters );
        /* istanbul ignore next */
        Vue.component( 'sal-multiselect', Multiselect );
        /* istanbul ignore next */
        Vue.component( 'sal-calendar-shift-card-mobile', CalendarShiftCard );
        /* istanbul ignore next */
        Vue.component( 'sal-calendar-leave-card-mobile', CalendarLeaveCard );
        /* istanbul ignore next */
        Vue.component( 'sal-calendar-filters', CalendarFilters );
        /* istanbul ignore next */
        Vue.component( 'sal-file-upload', FileUpload );
        /* istanbul ignore next */
        Vue.component( 'sal-my-announcement-card', MyAnnouncementCard );
        /* istanbul ignore next */
        Vue.component( 'sal-other-announcement-card', OtherAnnouncementCard );
        /* istanbul ignore next */
        Vue.component( 'sal-announcement-filters', AnnouncementFilters );
        /* istanbul ignore next */
        Vue.component( 'sal-announcement-filters-container', AnnouncementFiltersContainer );
        /* istanbul ignore next */
        Vue.component( 'sal-announcements-overlay-mobile', AnnouncementsOverlay );
        /* istanbul ignore next */
        Vue.component( 'sal-overlay-mobile', Overlay );
        /* istanbul ignore next */
        Vue.component( 'sal-request-details-attachment', RequestDetailsAttachment );
        /* istanbul ignore next */
        Vue.component( 'sal-progress-mobile', Progress );
        /* istanbul ignore next */
        Vue.component( 'sal-time-clock', TimeClock );
        /* istanbul ignore next */
        Vue.component( 'sal-time-clock-tags', TimeClockTags );
        /* istanbul ignore next */
        Vue.component( 'sal-request-time-record-tags', RequestTimeRecordTags );
        /* istanbul ignore next */
        Vue.component( 'sal-datepicker', DatePicker );
        /* istanbul ignore next */
        Vue.component( 'sal-overtime-edit-request-card', OvertimeEditRequestCard );
        /* istanbul ignore next */
        Vue.component( 'sal-request-tags-input', RequestTagsInput );
        /* istanbul ignore next */
        Vue.component( 'sal-overtime-request-approval-card-mobile', OvertimeRequestApprovalCard );
        /* istanbul ignore next */
        Vue.component( 'sal-undertime-request-approval-card-mobile', UndertimeRequestApprovalCard );
        /* istanbul ignore next */
        Vue.component( 'sal-overtime-details', OvertimeDetails );
        /* istanbul ignore next */
        Vue.component( 'sal-approval-actions', ApprovalActions );
        /* istanbul ignore next */
        Vue.component( 'sal-overtime-request-card-mobile', OvertimeRequestCard );
        /* istanbul ignore next */
        Vue.component( 'sal-time-dispute-request-card-mobile', TimeDisputeRequestCard );
        /* istanbul ignore next */
        Vue.component( 'sal-time-dispute-details', TimeDisputeDetails );
        /* istanbul ignore next */
        Vue.component( 'sal-undertime-details', UndertimeDetails );
        /* istanbul ignore next */
        Vue.component( 'sal-time-dispute-request-approval-card-mobile', TimeDisputeRequestApprovalCard );
        /* istanbul ignore next */
        Vue.component( 'sal-shift-change-request-approval-card-mobile', ShiftChangeRequestApprovalCard );
        /* istanbul ignore next */
        Vue.component( 'sal-shift-change-request-card-mobile', ShiftChangeRequestCard );
        /* istanbul ignore next */
        Vue.component( 'sal-shift-change-details', ShiftChangeDetails );
        /* istanbul ignore next */
        Vue.component( 'sal-initial-messages-feed', InitialMessagesFeed );
        /* istanbul ignore next */
        Vue.component( 'sal-initial-message', InitialMessage );
        /* istanbul ignore next */
        Vue.component( 'sal-add-request-details-card', AddRequestDetailsCard );
        /* istanbul ignore next */
        Vue.component( 'sal-multiselect-dropdown', MultiselectDropdown );
        /* istanbul ignore next */
        Vue.component( 'sal-time-dispute-edit-request-card', TimeDisputeEditRequestCard );
        /* istanbul ignore next */
        Vue.component( 'sal-hours-work-form', AddHoursWorkForm );
        /* istanbul ignore next */
        Vue.component( 'sal-time-record-form', AddTimeRecordForm );
        /* istanbul ignore next */
        Vue.component( 'sal-time-dispute-details-card', TimeDisputeDetailsCard );
        /* istanbul ignore next */
        Vue.component( 'sal-undertime-request-card-mobile', UndertimeRequestCard );
        /* istanbul ignore next */
        Vue.component( 'sal-undertime-edit-request-card', UndertimeEditRequestCard );
        /* istanbul ignore next */
        Vue.component( 'sal-profile-section', ProfileSection );
        Vue.component( 'sal-team-card', TeamCard );
        Vue.component( 'sal-team-time-clock', TeamTimeClock );
        Vue.component( 'sal-team-assign-shift', TeamAssignShift );
        Vue.component( 'sal-team-rest-day', TeamAssignRestDay );
        Vue.component( 'sal-team-view-calendar', TeamViewCalendar );
        Vue.component( 'sal-pill', Pill );
        Vue.component( 'sal-tooltip', Tooltip );
        Vue.component( 'sal-validation-message', ValidationMessage );
        Vue.component( 'sal-horizontal-calendar', HorizontalCalendar );
        Vue.component( 'sal-approval-item', ApprovalItem );
        Vue.component( 'sal-approval-side-panel-detail', ApprovalSidePanelDetail );
        Vue.component( 'sal-side-panel-tabs-leave', SidePanelTabsLeave );
        Vue.component( 'sal-side-panel-tabs-undertime', SidePanelTabsUndertime );
        Vue.component( 'sal-side-panel-tabs-overtime', SidePanelTabsOvertime );
        Vue.component( 'sal-side-panel-tabs-time-dispute', SidePanelTabsTimeDispute );
        Vue.component( 'sal-side-panel-tabs-shift-change', SidePanelTabsShiftChange );

        Vue.use( GlobalLoader );
        Vue.use( FlashMessages );
        // Vue.use( DashboardComponents );
        Vue.use( RequestDetailsComponents );
    }
};

export default Components;
