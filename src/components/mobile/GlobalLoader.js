export default {
    install: ( Vue ) => {
        // Define component
        const GlobalLoaderConstructor = Vue.extend({
            name: 'sal-global-loader',

            render( h ) {
                if ( this.visible ) {
                    return h( 'sal-mask' );
                }
                return null;
            },

            data() {
                return {
                    visible: false
                };
            },

            methods: {
                show() {
                    this.visible = true;
                },

                hide() {
                    this.visible = false;
                }
            }
        });

        // Construct and mount
        const globalLoaderComponent = new GlobalLoaderConstructor({
            el: document.createElement( 'div' )
        });

        document.querySelector( 'body' ).appendChild( globalLoaderComponent.$el );

        // Enable interaction
        Vue.$loader = {
            show() {
                globalLoaderComponent.show();
            },

            hide() {
                globalLoaderComponent.hide();
            }
        };

        Vue.prototype.$loader = Vue.$loader;
    }
};
