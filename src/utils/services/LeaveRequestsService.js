import axios from 'axios';
import moment from 'moment';
import { PRODUCTS } from '@/constants/employee-request';
import { DEFAULT_SHIFT_DATETIME_FORMAT } from '@/constants/overtime-request';
import { filter, isEmpty, get } from 'lodash-es';
import { round } from 'js-big-decimal';
import { shiftsService } from '@/utils/services/ShiftsService';

/**
 * Requests service class
 */
export default class LeaveRequestsService {
    /**
     * Get all leave types in company for the employee
     * @return {Promise}
     */
    getLeaveTypes() {
        return axios.get( 'ess/employee/leave_types' )
            .then( ( response ) => response.data.data );
    }

    /**
     * Post new leave request
     * @return {Promise}
     */
    submitLeaveRequest( data ) {
        return axios.post( 'ess/leave_request', data )
            .then( ( response ) => response.data.data );
    }

    /**
     * Fetches request with messages
     * @param Number id Leave Request ID
     * @return {Promise}
     */
    fetchLeaveRequest( id ) {
        return axios.get( `ess/leave_request/${id}` )
            .then( ( response ) => response.data );
    }

    /**
     * Get employee's leave credits.
     * @returns {Promise}
     */
    getLeaveCredits() {
        return axios.get( 'ess/leave_credits' )
            .then( ( response ) => response.data );
    }

    /**
     * Checks is account subscribed to payroll only
     *
     * @param {Array} employeeProducts
     *
     * @returns {Boolean}
     */
    hasSubscriptionToPayrollOrTaPlusPayroll( employeeProducts ) {
        if ( !employeeProducts ) {
            return false;
        }

        return !!filter(
            employeeProducts, ( product ) =>
                product.name === PRODUCTS.taPlusPayroll ||
                product.name === PRODUCTS.payroll
        ).length;
    }

    /**
     * Gets hours per day.
     *
     * @param {Object} leave
     * @param {Float} employeeHoursPerDay
     *
     * @returns {Number}
     */
    getHoursPerDay( leave, employeeHoursPerDay ) {
        let scheduledHours = 0;

        if ( leave && !isEmpty( leave ) ) {
            scheduledHours = get( leave, 'schedule.total_hours' );
            scheduledHours = moment( scheduledHours, 'HH:mm' ).hours();
        }

        return employeeHoursPerDay || scheduledHours;
    }

    /**
     * Handles filedLeave total value calculation.
     *
     * @param {Object} payload {unit, filteredLeavesByDate, employee}
     *
     * @returns {Number}
     */
    handleFiledLeaveTotalValueCalculation( payload ) {
        const leaveTotalValue = payload.unit !== 'days'
            ? this.getCalculatedFiledLeaveTotalValueInHours( payload )
            : this.getCalculatedFiledLeaveTotalValueInDays( payload );

        return round( leaveTotalValue, 2 );
    }

    /**
     * Computes the hours per day on a default schedule.
     *
     * @param {Object} leave The default schedule assigned to an emplyoee.
     *
     * @returns {Number}
     */
    getHoursPerDayFromDefaultSchedule( leave ) {
        const defaultScheduleStart = moment( leave.schedule.start_time, 'HH:mm' );
        const defaultScheduleEnd = moment( leave.schedule.end_time, 'HH:mm' );
        return moment.duration( defaultScheduleEnd.diff( defaultScheduleStart ) ).hours();
    }

    /**
     * Gets calculated filed leave day total value in hours.
     *
     * @param {Object} payload
     *
     * @returns {Number}
     */
    getCalculatedFiledLeaveTotalValueInDays( payload ) {
        if ( isEmpty( payload.filteredLeavesByDate ) ) {
            return 0;
        }

        let defaultHoursPerDay = 0;
        let totalValueInDays = 0;
        const employeeHoursPerDay = get( payload, 'employee.hours_per_day' );

        payload.filteredLeavesByDate.forEach( ( leave ) => {
            if ( !leave ) {
                return;
            }

            let hoursPerDay = this.getHoursPerDay(
                leave,
                employeeHoursPerDay
             );

            let duration = this.getCalculatedLeaveDurationHours( leave );

            if ( hoursPerDay ) {
                // Deduct breaks from total leave hours
                duration = shiftsService.deductBreaksFromSchedule( leave, duration );

                // Deduct breaks from hours per day if using schedule hours
                if ( isEmpty( employeeHoursPerDay ) ) {
                    hoursPerDay = shiftsService.deductBreaksFromSchedule( leave, hoursPerDay );
                }

                // Only resort to the condition of limiting total hours (hours per day) if
                // the schedule type is flexi otherwise, use the calculated duration
                if ( leave.schedule.type === 'flexi' ) {
                    duration = Math.min( duration, hoursPerDay );
                }

                totalValueInDays += duration / hoursPerDay;
            } else {
                // This is probably considered as a default schedule
                defaultHoursPerDay = this.getHoursPerDayFromDefaultSchedule( leave );

                // Deduct breaks for both rendered hours and scheduled hours
                duration = shiftsService.deductBreaksFromSchedule( leave, duration );
                defaultHoursPerDay = shiftsService.deductBreaksFromSchedule( leave, defaultHoursPerDay );

                totalValueInDays += duration / defaultHoursPerDay;
            }
        });

        return totalValueInDays;
    }

    /**
     * Gets calculated filed leave day total value in hours.
     *
     * @param {Object} payload filed leave information
     *
     * @returns {Number}
     */
    getCalculatedFiledLeaveTotalValueInHours( payload ) {
        const { filteredLeavesByDate: filteredSelectedLeaves } = payload;

        if ( !filteredSelectedLeaves || !filteredSelectedLeaves.length ) {
            return 0;
        }

        let totalValueInHours = 0;
        const employeeHoursPerDay = get( payload, 'employee.hours_per_day' );

        filteredSelectedLeaves.forEach( ( leave ) => {
            if ( !leave || !leave.schedule ) {
                return;
            }

            let hoursPerDay = this.getHoursPerDay(
                leave,
                employeeHoursPerDay
            );

            // Deduct breaks from hours per day if using schedule's total hours
            if ( isEmpty( employeeHoursPerDay ) ) {
                hoursPerDay = shiftsService.deductBreaksFromSchedule( leave, hoursPerDay );
            }

            // Only resort to the condition of limiting total hours (hours per day) if
            // the schedule type is flexi otherwise, use the calculated duration
            let duration = this.getCalculatedLeaveDurationHours( leave );

            if ( leave.schedule.type === 'flexi' ) {
                totalValueInHours += Math.min( duration, hoursPerDay );
            } else {
                // Deduct breaks for both rendered hours and scheduled hours
                duration = shiftsService.deductBreaksFromSchedule( leave, duration );
                totalValueInHours += duration;
            }
        });

        return totalValueInHours;
    }

    /**
     * Gets time duration in hours.
     *
     * @param {String} time
     *
     * @returns {Number}
     */
    getTimeDurationInHours( time ) {
        return ( moment.duration( time ).asMinutes() ) / 60;
    }

    /**
     * Gets datetime moment object.
     *
     * @param {String} date
     * @param {String} time
     *
     * @returns {Object} moment object
     */
    getDateTimeMomentObject( date, time ) {
        return moment( `${date} ${time}`, DEFAULT_SHIFT_DATETIME_FORMAT );
    }

    /**
     * Gets calculated leave duration hours.
     *
     * @param {Object}
     *
     * @returns {Number}
     */
    getCalculatedLeaveDurationHours( leave ) {
        const startDateTime = this.getDateTimeMomentObject( leave.date, leave.start );
        const endDateTime = this.getDateTimeMomentObject( leave.date, leave.end );

        if ( endDateTime.isBefore( startDateTime ) ) {
            endDateTime.add( 1, 'days' );
        }

        return this.getTimeDurationInHours( endDateTime.diff( startDateTime ) );
    }

    /**
     * Checks is leave start or end time between schedule time range.
     *
     * @param {Object} shift
     *
     * @returns {Boolean}
     */
    isLeaveTimeBetweenScheduleTimeRange( shift ) {
        const shiftScheduleStartDateTime = moment( shift.schedule.start_time, 'HH:mm' );
        const shiftScheduleEndDateTime = moment( shift.schedule.end_time, 'HH:mm' );

        const leaveStartDateTime = moment( shift.start_time, 'HH:mm' );
        const leaveEndDateTime = moment( shift.end_time, 'HH:mm' );

        if ( this.isStartAfterEndDateTime( shiftScheduleStartDateTime, shiftScheduleEndDateTime ) ) {
            const isStartValid = this.isLeaveDateTimeValid( leaveStartDateTime, shiftScheduleStartDateTime, shiftScheduleEndDateTime );
            const isEndValid = this.isLeaveDateTimeValid( leaveEndDateTime, shiftScheduleStartDateTime, shiftScheduleEndDateTime );

            return isStartValid && isEndValid;
        }

        return this.isShiftStartAndEndDateTimeBetweenSchedulesDateTimeRange(
            leaveStartDateTime,
            leaveEndDateTime,
            shiftScheduleStartDateTime,
            shiftScheduleEndDateTime
        );
    }

    /**
     * Checks is leave datetime valid.
     *
     * @param {Object} dateTime
     * @param {Object} scheduleStartDateTime
     * @param {Object} scheduleEndDateTime
     *
     * @returns {Boolean}
     *
     */
    isLeaveDateTimeValid( dateTime, scheduleStartDateTime, scheduleEndDateTime ) {
        return dateTime.isSameOrAfter( scheduleStartDateTime ) ||
            dateTime.isSameOrBefore( scheduleEndDateTime );
    }

    /**
     * Check is shift start datetime and end datetime between schedules datetime range.
     *
     * @param {Object} shiftStartDateTime
     * @param {Object} shiftEndDateTime
     * @param {Object} shiftScheduleStartDateTime
     * @param {Object} shiftScheduleEndDateTime
     *
     * @returns {Boolean}
     */
    isShiftStartAndEndDateTimeBetweenSchedulesDateTimeRange(
        shiftStartDateTime,
        shiftEndDateTime,
        shiftScheduleStartDateTime,
        shiftScheduleEndDateTime
    ) {
        const leaveStartDateTime = shiftStartDateTime.format( DEFAULT_SHIFT_DATETIME_FORMAT );
        const leaveEndDateTime = shiftEndDateTime.format( DEFAULT_SHIFT_DATETIME_FORMAT );
        const scheduleStartDateTime = shiftScheduleStartDateTime.format( DEFAULT_SHIFT_DATETIME_FORMAT );
        const scheduleEndDateTime = shiftScheduleEndDateTime.format( DEFAULT_SHIFT_DATETIME_FORMAT );

        return moment( leaveStartDateTime ).isBetween( scheduleStartDateTime, scheduleEndDateTime, null, []) &&
            moment( leaveEndDateTime ).isBetween( scheduleStartDateTime, scheduleEndDateTime, null, []);
    }

    /**
     * Checks is start DateTime after end DateTime.
     *
     * @param {String} startDateTime
     * @param {String} endDateTime
     *
     * @returns {Boolean}
     */
    isStartAfterEndDateTime( startDateTime, endDateTime ) {
        return moment( startDateTime ).isAfter( endDateTime );
    }
}

export const leaveRequestsService = new LeaveRequestsService();
