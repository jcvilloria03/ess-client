import { Fetch } from '@/utils/request';
import moment from 'moment';
import { groupBy, orderBy, keys, assign } from 'lodash-es';
import axios from 'axios';

/**
 * Employee Requests service class.
 */
export default class EmployeeRequestService {
    /**
     * Get all requests for employee.
     * @param Number page page number
     * @param filters optional param
     * @returns Promise
     */
    getEmployeeRequests( page, filters = null, isRootModule ) {
        const params = filters ? this.parseFilters( filters ) : {};
        params.page = page;

        return Fetch( 'ess/employee/requests', { isRootModule, params });
    }

    /**
     * Formats to coma separated and deletes keys if there is no value
     * @param Object filters
     * @returns Object parsed
     */
    parseFilters( filters ) {
        const parsed = Object.assign({
            statuses: [],
            types: [],
            start_date: null,
            end_date: null
        }, filters );

        parsed.statuses = parsed.statuses.join( ',' );
        parsed.types = parsed.types.join( ',' );

        keys( parsed ).forEach( ( key ) => !parsed[ key ] && delete parsed[ key ]);

        return parsed;
    }

    /**
     * Groups requests by date in descending order
     * @param Array requests array
     * @return Array returns array of grouped requests by date
     */
    groupRequestsByDate( requests ) {
        return groupBy( this.orderNotificationsByDateDesc( requests ),
            ( request ) => moment( request.date ).format( 'MMMM D, YYYY' )
        );
    }

    /**
     * Order requests in descending order by date
     * @param Array requests
     * @return Array returns ordered requests by date in descending order
     */
    orderNotificationsByDateDesc( requests ) {
        return orderBy( requests, 'created_at', 'desc' );
    }

    /**
     * Gets the employee approvals.
     */
    getEmployeeApprovals( page, filters = {}, isRootModule, perPage ) {
        return axios
            .post( 'ess/employee/approvals', assign({ page, my_level: true, per_page: perPage }, filters ), { isRootModule })
            .then( ( response ) => response.data );
    }
    /**
     * Declines parsed approvals
     * @param Array approvalsIds
     * @return Promise
     */
    bulkDeclineRequests( approvalsIds ) {
        return axios.post( 'ess/request/bulk_decline', {
            employee_requests_ids: approvalsIds
        });
    }

    /**
     * @param Array approvalsIds
     * @return Promise
     */
    bulkApproveRequests( approvalsIds ) {
        return axios.post( 'ess/request/bulk_approve', {
            employee_requests_ids: approvalsIds
        });
    }

    /**
     * @param Array approvals
     * @return Promise
     */
    bulkCancelRequests( approvals ) {
        return axios.post( 'ess/request/bulk_cancel', {
            employee_requests: approvals
        });
    }

    /**
     * Cancel request
     *
     * @param int  id
     * @return Promise
     */
    cancelRequest( request ) {
        return axios.post( `ess/request/${request.id}/cancel`, request )
        .then( ( response ) => response.data );
    }

    /**
     * Delete attachment.
     * @param {Number} requestId
     * @param {Number} attachmentId
     * @returns {Object}
     */
    deleteAttachment( requestId, attachmentId ) {
        return axios.delete( `ess/employee_request/${requestId}/attachments/${attachmentId}` )
            .then( ( response ) => response.data );
    }

    /**
     * Get generated csv file.
     * @param {Number|String} requestId
     * @returns {Promise}
     */
    downloadAll( requestId ) {
        return axios.get( `ess/employee_request/${requestId}/download_attachments`, {
            responseType: 'blob'
        }).then( ( response ) => response.data );
    }

    /**
     * Get employee rest days.
     * @returns {Promise}
     */
    getEmployeeRestDays() {
        return axios.get( 'ess/employee/rest_days' )
            .then( ( response ) => response.data.data );
    }

    /**
     * @param filters
     * sample payload
     * {
            "company_id": 1192,
            "statuses": [
                "pending"
            ],
            "types": [
                "leaves",
                "loans",
                "overtime",
                "shift_change",
                "time_dispute",
                "undertime"
            ],
            "start_date": null,
            "end_date": null,
            "name": "",
            "affected_employees": [
                {
                    "id": 45363,
                    "name": "Alvini C Constantine - 1150-ACC",
                    "type": "employee",
                    "uid": "employee45363"
                }
            ]
        }
     *
     * sample single payload
     *  {
            "company_id": 1192,
            "employee_requests_ids": [
                761404
            ]
        }
     * @return Promise
     */
    newBulkApproveRequests( payload ) {
        return axios.post( 'ess/request/bulk_approve', payload );
    }

    /**
     * @param filters
     * sample payload
     * {
            "company_id": 1192,
            "statuses": [
                "pending"
            ],
            "types": [
                "leaves",
                "loans",
                "overtime",
                "shift_change",
                "time_dispute",
                "undertime"
            ],
            "start_date": null,
            "end_date": null,
            "name": "",
            "affected_employees": [
                {
                    "id": 45363,
                    "name": "Alvini C Constantine - 1150-ACC",
                    "type": "employee",
                    "uid": "employee45363"
                }
            ]
        }
     *
     * sample single payload
     *  {
            "company_id": 1192,
            "employee_requests_ids": [
                761404
            ]
        }
     * @return Promise
     */
    newBulkDeclineRequests( payload ) {
        return axios.post( 'ess/request/bulk_decline', payload );
    }

    /**
     * @param filters
     * sample payload for multiple
     * {
            "company_id": 1192,
            "statuses": [
                "pending"
            ],
            "types": [
                "leaves",
                "loans",
                "overtime",
                "shift_change",
                "time_dispute",
                "undertime"
            ],
            "start_date": null,
            "end_date": null,
            "name": "",
            "affected_employees": [
                {
                    "id": 45363,
                    "name": "Alvini C Constantine - 1150-ACC",
                    "type": "employee",
                    "uid": "employee45363"
                }
            ]
        }
     *
     * sample single payload
     *  {
            "company_id": 1192,
            "employee_requests_ids": [
                761404
            ]
        }
     * @return Promise
     */
    newBulkCancelRequests( payload ) {
        return axios.post( 'ess/request/bulk_cancel', payload );
    }
}

export const employeeRequestService = new EmployeeRequestService();
