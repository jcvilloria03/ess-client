/* eslint-disable require-jsdoc */
import axios from 'axios';

/**
 * Calendar service class
 */
export default class CalendarService {
    /**
     * Get all calendar data for the employee
     * @return {Promise}
     */
    getCalendarData() {
        return axios.get( 'ess/employee/calendar_data' )
            .then( ( response ) => response.data );
    }

    getCompanyHolidays() {
        try {
            const companyId = JSON.parse( localStorage.getItem( 'user' ) ).company_id;
            return axios.get( `company/${companyId}/holidays`, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem( 'access_token' )}`,
                    'X-Authz-Entities': 'main_page.dashboard'
                }
            })
                .then( ( response ) => response.data );
        } catch ( error ) {
            return Promise.reject( error );
        }
    }

}

export const calendarService = new CalendarService();
