import axios from 'axios';
import { map, findIndex } from 'lodash-es';

/**
 * Time correction request service class
 */
export default class TimeDisputeRequestService {

    /**
     * Post new time correction request.
     * @param {Object} data request data
     * @return {Promise}
     */
    submitTimeDisputeRequest( data ) {
        return axios.post( 'ess/time_dispute_request', data )
            .then( ( response ) => response.data );
    }

    /**
     * Fetches request with messages.
     * @param {Number} id Time Correction Request ID
     * @return {Promise}
     */
    fetchTimeDisputeRequest( id ) {
        return axios.get( `ess/time_dispute_request/${id}` )
            .then( ( response ) => response.data );
    }

    /**
     * Gets the time types.
     *
     * @return {Promise}
     */
    getTimeTypes() {
        return axios.get( 'ess/time_types' )
            .then( ( response ) => response.data.data );
    }

    /**
     * Fetch formatted time data for indication
     * @param {Object} data Time records collection
     * @return {Array}
     */
    getTimeDataForIndication( data, isTimesheet ) {
        let indicationItems = [];

        const oldDataItems = [...data.old_state];
        let newDataItems = [...data.new_state];

        oldDataItems.forEach( ( oldItem ) => {
            const searchByType = { type: oldItem.type };

            // search by time or timestamp value and by type
            const searchByValue = {
                ...searchByType,
                ...( isTimesheet ? { timestamp: oldItem.timestamp } : { time: oldItem.time })
            };

            let index = findIndex( newDataItems, searchByValue );
            let item = newDataItems[ index ];

            if ( item ) {
                indicationItems.push( item );
            } else {
                // not found by time or timestamp value
                oldItem.action = 'old';
                indicationItems.push( oldItem );

                // search by type
                index = findIndex( newDataItems, searchByType );
                item = newDataItems[ index ];

                if ( item ) {
                    item.action = 'new';
                } else {
                    item = {
                        ...oldItem,
                        action: 'new',
                        ...( isTimesheet ? { timestamp: 0 } : { time: '00:00' })
                    };
                }

                indicationItems.push( item );
            }

            if ( index !== -1 ) {
                newDataItems.splice( index, 1 );
            }
        });

        newDataItems = map( newDataItems, ( item ) => {
            item.action = 'added';
            return item;
        });

        // merge remaining added time records
        indicationItems = [ ...indicationItems, ...newDataItems ];

        return indicationItems;
    }

    /**
     * Get number of indication changes
     *
     * @param {Array} indicationData
     * @param {Number}
     */
    getNumberOfChanges( indicationData ) {
        return indicationData.filter( ( record ) =>
            ( record.action && record.action !== 'old' )
        ).length;
    }
}

export const timeDisputeRequestService = new TimeDisputeRequestService();
