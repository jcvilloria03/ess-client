import { Fetch } from '@/utils/request';
import axios from 'axios';

/**
 * Payslips service class
 */
export default class PayslipsService {
    /**
     * Fetches unread payslips
     * @return {Promise}
     */
    getUnreadPayslips() {
        return axios.get( '/ess/payslips/unread', { isRootModule: true })
            .then( ( response ) => response.data );
    }

    /**
     * Fetches payslips
     * @param {Integer} year - Year
     * @return {Promise}
     */
    getPayslips( year = new Date().getFullYear() ) {
        return Fetch( `/ess/payslips?year=${year}`, { method: 'GET' });
    }

    /**
     * Fetches payslip details
     * @param {Integer} id - Payslip ID
     * @returns {Promise}
     */
    getPayslip( id ) {
        return Fetch( `/ess/payslip/${id}`, { method: 'GET' });
    }
}

export const payslipsService = new PayslipsService();
