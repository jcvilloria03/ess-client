import axios from 'axios';
import moment from 'moment';
import { groupBy, orderBy } from 'lodash-es';

/**
 * Notifications service class
 */
export default class NotificationsService {

    /**
     * Fetches User notifications
     * @param Number pagenumber
     * @return {Promise}
     */
    fetchNotificationsData( pageNumber = 1 ) {
        return axios.get( `ess/notifications?page=${pageNumber}`, { isRootModule: true })
            .then( ( response ) => response.data.data );
    }

    /**
     * Groups notifications by date in descending order
     * @param Array notifications array
     * @return Array returns array of grouped notifications by date
     */
    groupeNotifications( notifications ) {
        const today = moment();

        return groupBy( this.orderNotificationsByDateDesc( notifications ), ( notification ) => {
            const date = moment( notification.created_at );

            return this.isNotificationsDateSameAsCurrent( date, today );
        });
    }

    /**
     * Checks is current date same as date when notification is created
     * @param {*} date date from notification created_at
     * @param {*} today todays date
     * @return String return formated date string
     */
    isNotificationsDateSameAsCurrent( date, today ) {
        return date.isSame( today, 'day' )
            ? `Today, ${date.format( 'MMMM D, YYYY' )}`
            : date.format( 'MMMM D, YYYY' );
    }

    /**
     * Order notification in descending order by date
     * @param Array notifications
     * @return Array returns ordered notifications by date in descending order
     */
    orderNotificationsByDateDesc( notifications ) {
        return orderBy( notifications, 'created_at', 'desc' );
    }

    /**
     * Gets notifications for user
     * @return Promise
     */
    getUserNotificationsStatus() {
        return axios.get( 'ess/notifications_status', { isRootModule: true })
            .then( ( response ) => response.data );
    }

    /**
     *  Update notifications status
     * @return Promise
     */
    updateNotificationStatus() {
        return axios.put( 'ess/notifications_status', {}, { isRootModule: true })
            .then( ( response ) => response.data );
    }

    /**
     * Updates notifications status after notifiaction is clicked
     * @param Number notificationId
     * @return Promise
     */
    updateNotificationClickedStatus( notificationId ) {
        return axios.put( `ess/notifications/${notificationId}/clicked`, false, { isRootModule: true })
            .then( ( response ) => response.data );
    }
}

export const notificationsService = new NotificationsService();
