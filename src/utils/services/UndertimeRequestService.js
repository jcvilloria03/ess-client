import axios from 'axios';
import moment from 'moment';
import { cloneDeep } from 'lodash-es';
import { INVALID_NUMBER_OF_HOURS } from '@/constants/employee-request';
import { DEFAULT_SHIFT_DATETIME_FORMAT } from '@/constants/overtime-request';
/**
 * Undertime requests service class
 */
export default class UndertimeRequestService {

    /**
     * Post new undertime request.
     * @param {Object} data request data
     * @return {Promise}
     */
    submitRequest( data ) {
        return axios.post( 'ess/undertime_request', data )
            .then( ( response ) => response.data );
    }

    /**
     * Fetches request with messages.
     * @param {Number} id Undertime Request ID
     * @return {Promise}
     */
    fetchRequest( id ) {
        return axios.get( `ess/undertime_request/${id}` )
            .then( ( response ) => response.data );
    }

    /**
     * Check is undertime request between shift start and shift end dateTime.
     *
     * @param {Object} shiftDateTimeRange shift start and end dateTime - moment objects
     * @param {Object} undertimeTimeRange undertime start, end and number of hours
     * @param {String} selectedDate request date
     * @returns {Boolean}
     */
    isUndertimeRequestValid( shiftDateTimeRange, undertimeTimeRange, selectedDate ) {
        const undertimeStartDateTime = `${selectedDate} ${undertimeTimeRange.start}`;
        const undertimeEndDateTime = `${selectedDate} ${undertimeTimeRange.end}`;

        if ( !this.isNumberOfHoursValid( undertimeTimeRange.numberOfHours, shiftDateTimeRange, selectedDate ) ) {
            return false;
        }

        if ( this.isNightShift( shiftDateTimeRange ) ) {
            return this.isUndertimeRequestOnNightShiftValid(
                shiftDateTimeRange,
                undertimeStartDateTime,
                undertimeEndDateTime
            );
        }

        return this.isUndertimeRequestInShiftTimeRange(
            undertimeStartDateTime,
            shiftDateTimeRange.start,
            shiftDateTimeRange.end
        ) &&
        this.isUndertimeRequestInShiftTimeRange(
            undertimeEndDateTime,
            shiftDateTimeRange.start,
            shiftDateTimeRange.end
        );
    }

    /**
     * Validate undertime request on night shift.
     * @param {Object} shiftDateTimeRange
     * @param {Object} undertimeStartDateTime
     * @param {Object} undertimeEndDateTime
     *
     * @returns {Boolean}
     */
    isUndertimeRequestOnNightShiftValid( shiftDateTimeRange, undertimeStartDateTime, undertimeEndDateTime ) {
        const shiftStart = this.formatDateTimeToShiftsDefaultFormat( shiftDateTimeRange.start );
        const shiftEnd = this.formatDateTimeToShiftsDefaultFormat( shiftDateTimeRange.end );
        let endDateTime = undertimeEndDateTime;
        let startDateTime = undertimeStartDateTime;

        if ( !this.isUndertimeRequestInShiftTimeRange( startDateTime, shiftStart, shiftEnd ) ) {
            startDateTime = this.addDayToDatetime( undertimeStartDateTime );

            return this.isUndertimeRequestInShiftTimeRange( startDateTime, shiftStart, shiftDateTimeRange.end );
        }

        if (
            this.isShiftTimeSameOrBeforeUndertimeStartTime( shiftStart, undertimeStartDateTime ) &&
            !this.isShiftTimeSameOrBeforeUndertimeStartTime( shiftStart, undertimeEndDateTime )
        ) {
            endDateTime = this.addDayToDatetime( undertimeEndDateTime );

            return this.isUndertimeRequestInShiftTimeRange( endDateTime, shiftStart, shiftDateTimeRange.end );
        }

        return true;
    }

    /**
     * Check is shift time same or before undertime start time.
     * @param {String} shiftStart
     * @param {String} undertimeStart
     *
     * @returns {Boolean}
     */
    isShiftTimeSameOrBeforeUndertimeStartTime( shiftStart, undertimeStart ) {
        return moment( shiftStart ).isSameOrBefore( undertimeStart );
    }

    /**
     * Formats datetime to DEFAULT_SHIFT_DATETIME_FORMAT
     * @param {Object} dateTime momentObject
     *
     * @returns {String}
     */
    formatDateTimeToShiftsDefaultFormat( dateTime ) {
        return moment( dateTime ).format( DEFAULT_SHIFT_DATETIME_FORMAT );
    }

    /**
     * Check is night shift start dateTime and end dateTime on same day.
     * @param {Object} shiftDateTimeRange
     *
     * @returns {Boolean}
     */
    isNightShift( shiftDateTimeRange ) {
        return !moment( shiftDateTimeRange.end ).isSame( shiftDateTimeRange.start, 'day' );
    }

    /**
     * Validate is undertimeRequest between shift start and end time.
     * @param {String} undertimeTime
     * @param {Object} shiftStart
     * @param {Object} shiftEnd
     *
     * @returns {Boolean}
     */
    isUndertimeRequestInShiftTimeRange( undertimeTime, shiftStart, shiftEnd ) {
        return moment( undertimeTime ).isBetween( shiftStart, shiftEnd, null, []);
    }

    /**
     * Validate undertime request numberOfHours
     * @param {String} numberOfHours
     * @param {Object} shiftDateTimeRange
     * @param {String} selectedDate
     *
     * @returns {Boolean}
     */
    isNumberOfHoursValid( numberOfHours, shiftDateTimeRange, selectedDate ) {
        const shiftDateTimeRangeCloned = cloneDeep( shiftDateTimeRange );

        const shiftsDurationDateTime = this.getShiftDurationTime( shiftDateTimeRangeCloned );
        const undertimeDurationDateTime = `${selectedDate} ${numberOfHours}`;

        return numberOfHours !== INVALID_NUMBER_OF_HOURS &&
            this.isUndertimeRequestDurationTimeValid( shiftsDurationDateTime, undertimeDurationDateTime );
    }

    /**
     * Adds day to datetime.
     * @param {String} datetime
     *
     * @returns {String}
     */
    addDayToDatetime( datetime ) {
        return moment( datetime ).add( 1, 'day' ).format( DEFAULT_SHIFT_DATETIME_FORMAT );
    }

    /**
     * Gets shifts duration time.
     * @param {Object} shiftTimeRange
     *
     * @returns {String}
     */
    getShiftDurationTime( shiftTimeRange ) {
        return shiftTimeRange.end.subtract(
            shiftTimeRange.start.hour(), 'hour'
        ).format( DEFAULT_SHIFT_DATETIME_FORMAT );
    }

    /**
     * Checks is undertime request duration valid
     * @param {String} shiftsDurationDateTime
     * @param {String} undertimeDurationDateTime
     *
     * @returns {Boolean}
     */
    isUndertimeRequestDurationTimeValid( shiftsDurationDateTime, undertimeDurationDateTime ) {
        return moment( shiftsDurationDateTime ).isSameOrAfter( undertimeDurationDateTime );
    }
}

export const undertimeRequestService = new UndertimeRequestService();
