import axios from 'axios';

/**
 * Requests service class
 */
export default class OvertimeRequestService {

    /**
     * Post new overtime request.
     * @param {Object} data request data
     * @return {Promise}
     */
    submitOvertimeRequest( data ) {
        return axios.post( 'ess/overtime_request', data )
            .then( ( response ) => response.data );
    }

    /**
    * Fetches request with messages.
    * @param {Number} id Overtime Request ID
    * @return {Promise}
    */
    fetchOvertimeRequest( id ) {
        return axios.get( `ess/overtime_request/${id}` )
            .then( ( response ) => response.data );
    }
}

export const overtimeRequestService = new OvertimeRequestService();
