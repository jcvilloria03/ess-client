import axios from 'axios';

/**
 * Attendance Records service class
 */
export default class AttendanceRecordService {
    /**
     * Get attendance record of employee for a given date
     * @param {Number} id - Employee ID
     * @param {String} date - Date of attendance record in yyyy-mm-dd format
     * @return {Promise}
     */
    getAttendanceRecordForDate( id, date ) {
        return axios.get( `/ess/attendance/${id}/${date}` )
            .then( ( response ) => response.data );
    }

}

export const attendanceRecordService = new AttendanceRecordService();
