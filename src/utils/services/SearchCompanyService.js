import axios from 'axios';
/**
 * Search company class.
 */
export default class SearchCompanyService {
    /**
     * Search location, department, position, employee name.
     * @param {String} term
     * @param {Number} companyId
     * @param {Number} limit
     * @returns {Array} response.data
     */
    fetchEntitledEmployees( term, limit, includeAdmins = false ) {
        return axios
        .get( '/ess/affected_employees/search', {
            params: {
                term,
                limit,
                include_admins: includeAdmins
            }
        })
        .then( ( response ) => response.data );
    }
}

export const searchCompanyService = new SearchCompanyService();
