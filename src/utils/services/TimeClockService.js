import axios from 'axios';
import moment from 'moment';
import Vue from 'vue';
import { Fetch } from '@/utils/request';
import { sortBy, findLast, filter, last, isEmpty, get } from 'lodash-es';
import { auth } from '@/utils/Authentication';
import { DEFAULT_SHIFT_DATETIME_FORMAT, TIMECLOCK_ORIGIN } from '@/constants/overtime-request';
import { shiftsService } from './ShiftsService';

/**
 * TimeClock service class
 */
export default class TimeClockService {
    timesheet = null;

    /**
     * Gets the time clock state.
     *
     * @return {Promise}
     */
    getTimeClockState() {
        return axios.get( 'ess/clock_state' )
            .then( ( response ) => response.data );
    }

    /**
     * Logs a state with origin.
     *
     * @return {Promise}
     */
    logState( state, tags ) {
        const maxClockRule = JSON.parse( localStorage.getItem( 'user' ) ).max_clock_rule;

        return axios.post( 'ess/clock_state/log', { state, tags, origin: TIMECLOCK_ORIGIN, max_clockout_rule: maxClockRule })
            .then( ( response ) => response.data.item );
    }

    /**
     * Get the entire time clock timesheet for the user.
     * @return {Promise}
     */
    getTimeClockTimesheet( startTimestamp = null, endTimestamp = null ) {
        let url = 'ess/clock_state/timesheet';

        if ( startTimestamp ) {
            url = `${url}?start_timestamp=${startTimestamp}`;
        }

        if ( startTimestamp && endTimestamp ) {
            url = `${url}&end_timestamp=${endTimestamp}`;
        }

        return axios.get( url )
            .then( ( response ) => {
                this.timesheet = response.data;

                return response.data;
            });
    }

    /**
     * Get the last time clock timesheet for the user.
     * @return {Promise}
     */
    getTimeClockTimesheetLastEntry() {
        return axios.get( 'ess/clock_state/timesheet/last_entry' )
            .then( ( response ) => {
                this.timesheet = [];
                this.timesheet.push( response.data.data );

                return response.data;
            });
    }

    /**
     * Update timesheet chacke with new timeclock entry.
     * @param  {Object} timeclock
     */
    updateTimesheetCache( state, tags ) {
        if ( !this.timesheet ) {
            this.timesheet = [];
        }

        const user = auth.getUser();

        this.timesheet.push({
            employee_uid: parseInt( user.employee_id, 10 ),
            company_uid: user.company_id,
            status: state,
            tags,
            timestamp: moment().format( 'X' )
        });
    }

    /**
     * Get array of objects with clock-in and clock-out pairs.
     * @param  {Array} data timesheet
     * @return {Array}
     */
    getAttendancePairs( data ) {
        let previousIn = false;
        let workingPair = null;

        const timePairs = [];
        sortBy( data, 'timestamp' ).forEach( ( attendance ) => {
            if ( attendance.state ) {
                workingPair = { clockIn: attendance };
            } else if ( !attendance.state && previousIn ) {
                workingPair = Object.assign({}, workingPair, { clockOut: attendance });
                timePairs.push( workingPair );
            }

            previousIn = attendance.state;
        });

        return timePairs;
    }

    /**
     * Checks is parsed timeclock pair overlaping shift range.
     *
     * @param  {Object} timeclock pair of clockin and clockout
     * @param  {Object} shift
     * @returns {*}
     */
    isTimeclockOverlapingWithShift( timeclock, shift ) {
        if ( !( timeclock && timeclock.clockIn && timeclock.clockOut ) ) {
            return false;
        }

        const clockIn = moment( timeclock.clockIn.timestamp, 'X' );
        const clockOut = moment( timeclock.clockOut.timestamp, 'X' );
        const shiftStart = moment( shift.start, 'HH:mm' );
        const shiftEnd = moment( shift.end, 'HH:mm' );

        if ( !clockIn.isValid() || !clockOut.isValid() ) return false;

        if (
            this.isClockInAroundShiftStart( clockIn, clockOut, shiftStart, shiftEnd ) ||
            this.isShiftRangeInsideTimeclockInterval( clockIn, clockOut, shiftStart, shiftEnd ) ||
            this.isTimeclockInsideShiftInterval( clockIn, clockOut, shiftStart, shiftEnd ) ||
            this.isClockInAroundShiftEnd( clockIn, clockOut, shiftStart, shiftEnd )
        ) {
            return timeclock;
        }
        return false;
    }

    /**
     * Checks is clock in before shift start and clock out between shift start and end.
     * @param {Object} clockIn
     * @param {Object} clockOut
     * @param {Object} shiftStart
     * @param {Object} shiftEnd
     * @returns {Boolean}
     */
    isClockInAroundShiftStart( clockIn, clockOut, shiftStart, shiftEnd ) {
        return clockIn.isSameOrBefore( shiftStart ) && clockOut.isBetween( shiftStart, shiftEnd );
    }
    /**
     * Checks is shift inside of clock in and out interval.
     * @param {Object} clockIn
     * @param {Object} clockOut
     * @param {Object} shiftStart
     * @param {Object} shiftEnd
     * @returns {Boolean}
     */
    isShiftRangeInsideTimeclockInterval( clockIn, clockOut, shiftStart, shiftEnd ) {
        return clockIn.isSameOrBefore( shiftStart ) && clockOut.isSameOrAfter( shiftEnd );
    }

    /**
     * Checks is clock in and clock out inside of shift interval.
     * @param {Object} clockIn
     * @param {Object} clockOut
     * @param {Object} shiftStart
     * @param {Object} shiftEnd
     * @returns {Boolean}
     */
    isTimeclockInsideShiftInterval( clockIn, clockOut, shiftStart, shiftEnd ) {
        return clockIn.isBetween( shiftStart, shiftEnd ) && clockOut.isBetween( shiftStart, shiftEnd );
    }

    /**
     * Checks is clock in and clock out around shift end.
     * @param {Object} clockIn
     * @param {Object} clockOut
     * @param {Object} shiftStart
     * @param {Object} shiftEnd
     * @returns {Boolean}
     */
    isClockInAroundShiftEnd( clockIn, clockOut, shiftStart, shiftEnd ) {
        return clockIn.isBetween( shiftStart, shiftEnd ) && clockOut.isSameOrAfter( shiftEnd );
    }

    /**
     * Get time clock pairs form shift timesheet
     *
     * @param {Array} timesheet
     * @returns {Array} timePairs
     */
    getClockPairsFromShiftTimesheet( timesheet ) {
        let previousIn = false;
        let workingPair = null;
        const timePairs = [];

        sortBy( timesheet, 'timestamp' ).forEach( ( timeclock ) => {
            if ( timeclock.type === 'clock_in' && !previousIn ) {
                workingPair = { clockIn: timeclock };
                previousIn = true;
            } else if ( timeclock.type === 'clock_out' && previousIn ) {
                workingPair.clockOut = timeclock;
                timePairs.push( workingPair );
                previousIn = false;
            }
        });

        return timePairs;
    }

    /**
     * Handle pairing of clock-in and clock-out.
     * even if clock-in/out isn't available yet
     * @param {object} employee attendance
     *
     */
    handleAttendancePairing( data ) {
        let previousIn = false;
        let previousOut = false;
        let workingPair = null;
        const timePairs = [];

        sortBy( data, 'timestamp' ).forEach( ( attendance ) => {
            const timestamp = this.timestampFormatting( attendance.timestamp );
            const attendanceData = Object.assign({}, attendance, { timestamp });

            if ( attendance.state ) {
                if ( previousIn ) {
                    timePairs.push( workingPair );
                }

                workingPair = { clockIn: attendanceData };
            } else {
                if ( previousIn ) {
                    workingPair = Object.assign({}, workingPair, { clockOut: attendanceData });
                } else if ( previousOut || workingPair === null ) {
                    workingPair = { clockOut: attendanceData };
                }

                timePairs.push( workingPair );
                workingPair = null;
            }

            previousIn = attendance.state;
            previousOut = !attendance.state;
        });

        return workingPair !== null ? timePairs.concat( workingPair ) : timePairs;
    }

    /**
     * format timestamp to have a fixed +08:00 timezone
     * @param {integer} timestamp
     * @returns {String} timestamp
     */
    timestampFormatting( timestamp ) {
        return moment( moment( timestamp, 'X' ).utcOffset( '+0800' ).format( 'YYYY-MM-DD HH:mm:ss' ) ).format( 'X' );
    }

    /**
     * Get the UTC offset between local time and server time in minutes
     *
     * @return {Number}
     */
    getUtcOffsetFromServerTime() {
        const localUtcOffset = moment().utcOffset();
        const serverUtcOffset = 8 * 60; // Asia/Manila +08:00 time in minutes

        return localUtcOffset - serverUtcOffset;
    }

    /**
     * Returns number of working minutes
     *
     * @param {Array} clockPairs
     * @param {Object} shift
     * @returns {Number}
     */
    getMinutesWorked( clockPairs, shift ) {
        let minutesWorked = 0;
        const shiftRange = shiftsService.getShiftRange( shift );
        const expectedMinutes = this.timeToMinutes( shift.schedule.expected_hours, 'HH:mm' );

        if ( isEmpty( clockPairs ) ) {
            return minutesWorked;
        }

        clockPairs.forEach( ( clockPair ) => {
            let clockIn = moment( clockPair.clockIn.datetime, DEFAULT_SHIFT_DATETIME_FORMAT );
            let clockOut = moment( clockPair.clockOut.datetime, DEFAULT_SHIFT_DATETIME_FORMAT );

            if ( clockPair.clockIn.offset ) {
                clockIn = clockIn.utcOffset( clockPair.clockIn.offset );
            }

            if ( clockPair.clockOut.offset ) {
                clockOut = clockOut.utcOffset( clockPair.clockOut.offset );
            }

            if ( this.isClockInAroundShiftStart( clockIn, clockOut, shiftRange.start, shiftRange.end ) ) {
                minutesWorked += clockOut.diff( shiftRange.start, 'minutes' );
            } else if ( this.isShiftRangeInsideTimeclockInterval( clockIn, clockOut, shiftRange.start, shiftRange.end ) ) {
                minutesWorked += shiftRange.end.diff( shiftRange.start, 'minutes' );
            } else if ( this.isTimeclockInsideShiftInterval( clockIn, clockOut, shiftRange.start, shiftRange.end ) ) {
                minutesWorked += clockOut.diff( clockIn, 'minutes' );
            } else if ( this.isClockInAroundShiftEnd( clockIn, clockOut, shiftRange.start, shiftRange.end ) ) {
                minutesWorked += shiftRange.end.diff( clockIn, 'minutes' );
            }
        });

        // If a shift is a flexi type of shift and the rendered hours (minutes) is greater than the expected
        // hours, the default time should be the expected hours otherwise, the rendered hours.
        if ( !isEmpty( shift.schedule.type ) && shift.schedule.type === 'flexi' && minutesWorked > expectedMinutes ) {
            return expectedMinutes;
        } else if ( minutesWorked ) {
            minutesWorked = shiftsService.deductBreaksFromClockpairsDuration( shift, clockPairs, minutesWorked, 'asMinutes' );
        }

        return minutesWorked;
    }

    /**
     * Get timesheet for shift
     *
     * @param {Array} timesheet
     * @param {Object} shift
     * @returns {Array} timesheet for shift
     */
    getTimesheetForShift( timesheet, shift ) {
        let timeclocksBeforeShift = [];
        const timeclocksWithinShift = [];
        const timeclocksAfterShift = [];
        const shiftRange = shiftsService.getShiftRange( shift );

        // divide timeclocks
        sortBy( timesheet, 'timestamp' ).forEach( ( timeclock ) => {
            if ( this.isTimeClockBeforeShiftStart( timeclock, shiftRange.start ) ) {
                timeclocksBeforeShift = timeclock.state ? [timeclock] : [];
            } else if ( this.isTimeClockInShiftRange( timeclock, shiftRange ) ) {
                timeclocksWithinShift.push( timeclock );
            } else if ( this.isTimeClockAfterShiftEnd( timeclock, shiftRange.end ) ) {
                timeclocksAfterShift.push( timeclock );
            }
        });

        // filter timeclocks after shifts
        const filteredTimeclocksAfterShift = [];
        if ( this.isClockInTheLastTimeRecordWithinTimesheets( timeclocksBeforeShift, timeclocksWithinShift ) ) {
            for ( let i = 0; i < timeclocksAfterShift.length; i += 1 ) {
                filteredTimeclocksAfterShift.push( timeclocksAfterShift[ i ]);
                if ( !timeclocksAfterShift[ i ].state ) {
                    break;
                }
            }
        }

        return [ ...timeclocksBeforeShift, ...timeclocksWithinShift, ...filteredTimeclocksAfterShift ];
    }

    /**
     * Get timesheet specifically for time dispute
     *
     * @param {Array} timesheet
     * @param {Object} shift
     * @param {Object} nextShift
     * @returns {Array} timesheet for shift
     */
    getTimesheetForShiftForTimeCorrection( timesheet, shift, nextShift, previousShift ) {
        const previousShiftStartDateTimeStr = `${previousShift.date} ${previousShift.start}`;
        let previousShiftEndDateTimeStr = `${previousShift.date} ${previousShift.end}`;

        // Check if previous shift's end time crosses the next day
        if ( moment( previousShiftEndDateTimeStr ).isBefore( moment( previousShiftStartDateTimeStr ) ) ) {
            previousShiftEndDateTimeStr = `${moment( previousShift.date ).add( 1, 'day' ).format( 'YYYY-MM-DD' )} ${previousShift.end}`;
        }

        const shiftStartDateTimeStr = `${shift.date} ${shift.start}`;
        let shiftEndDateTimeStr = `${shift.date} ${shift.end}`;

        // Check if shift's end time crosses the next day
        if ( moment( shiftEndDateTimeStr ).isBefore( moment( shiftStartDateTimeStr ) ) ) {
            shiftEndDateTimeStr = `${moment( shift.date ).add( 1, 'day' ).format( 'YYYY-MM-DD' )} ${shift.end}`;
        }

        let startBuffer = moment( shiftStartDateTimeStr )
            .diff( moment( previousShiftEndDateTimeStr ), 'hour' );
        startBuffer = startBuffer > 4 ? 4 : startBuffer;
        const startDateTimeBuffer = moment( shiftStartDateTimeStr ).subtract( startBuffer, 'hour' );

        const nextShiftDateTimeStr = `${nextShift.date} ${nextShift.start}`;
        let endBuffer = moment( nextShiftDateTimeStr )
            .diff( moment( shiftEndDateTimeStr ), 'hour' );
        endBuffer = endBuffer >= 4 ? endBuffer - 4 : 0;
        const endDateTimeBuffer = moment( shiftEndDateTimeStr ).add( endBuffer, 'hour' );

        const sortedTimesheet = sortBy( timesheet, 'timestamp' );

        const filteredTimeRecords = sortedTimesheet
            .filter( ( timeRecord ) => moment.unix( timeRecord.timestamp )
                .isBetween( startDateTimeBuffer, endDateTimeBuffer, 'second', '[)' )
            );

        // Check if first time record state is a clock-out and remove
        if ( filteredTimeRecords.length > 0 && get( filteredTimeRecords, '0.state' ) === false ) {
            filteredTimeRecords.shift();
        }

        // Check if last time record state is a clock-in and add the next clock out
        if ( filteredTimeRecords.length > 0 ) {
            const lastTimeRecord = filteredTimeRecords[ filteredTimeRecords.length - 1 ];

            if ( lastTimeRecord.state ) {
                const lastTimeRecordIndex = sortedTimesheet
                    .findIndex( ( timeRecord ) => timeRecord.state && timeRecord.timestamp === lastTimeRecord.timestamp );

                if ( lastTimeRecordIndex > -1 ) {
                    const timeRecordToPush = sortedTimesheet[ lastTimeRecordIndex + 1 ];
                    if ( timeRecordToPush && !timeRecordToPush.state ) {
                        filteredTimeRecords.push( timeRecordToPush );
                    }
                }
            }
        }

        return filteredTimeRecords;
    }

    /**
     * Is clock in the last time record within timesheets
     *
     * @param {Array} timeclocksBeforeShift
     * @param {Array} timeclocksWithinShift
     * @returns {Boolean}
     */
    isClockInTheLastTimeRecordWithinTimesheets( timeclocksBeforeShift, timeclocksWithinShift ) {
        return this.isClockInTheLastTimeRecord( timeclocksWithinShift ) ||
            ( this.isClockInTheLastTimeRecord( timeclocksBeforeShift ) &&
                ( this.checkTimesheetContainsOnlyClockIns( timeclocksWithinShift ) || !timeclocksWithinShift.length )
            );
    }

    /**
     * Is clock in the last time record within timesheet
     *
     * @param {Array} timesheet
     * @returns {Boolean}
     */
    isClockInTheLastTimeRecord( timesheet ) {
        return timesheet.length > 0 && last( timesheet ).state;
    }

    /**
     * Check timesheet contains only clock ins
     *
     * @param {Array} timesheet
     * @returns {Boolean}
     */
    checkTimesheetContainsOnlyClockIns( timesheet ) {
        return timesheet.length > 0 && !filter( timesheet, ( timeclock ) =>
                ( !timeclock.state ).length > 0 );
    }

    /**
     * Checks is parsed timeclock pair overlaping shift range.
     *
     * @param {Object} timeclock
     * @param {Object} shiftRange
     *
     * @returns {Boolean}
     */
    isTimeClockInShiftRange( timeclock, shiftRange ) {
        const time = moment( timeclock.timestamp, 'X' );
        return time.isBetween( shiftRange.start, shiftRange.end, null, '[]' );
    }

    /**
     * Checks is parsed timeclock is before shift start.
     *
     * @param {Object} timeclock
     * @param {Object} shiftStart
     *
     * @returns {Boolean}
     */
    isTimeClockBeforeShiftStart( timeclock, shiftStart ) {
        const time = moment( timeclock.timestamp, 'X' );
        const startOfDay = moment( shiftStart.format( 'YYYY-MM-DD' ) );

        return time.isBetween( startOfDay, shiftStart, null, '[]' );
    }

    /**
     * Checks is parsed timeclock is after shift end.
     *
     * @param {Object} timeclock
     * @param {Object} shiftStart
     *
     * @returns {Boolean}
     */
    isTimeClockAfterShiftEnd( timeclock, shiftEnd ) {
        const time = moment( timeclock.timestamp, 'X' );
        const endOfDay = moment( shiftEnd.format( 'YYYY-MM-DD' ) ).add( 1, 'days' );

        return time.isBetween( shiftEnd, endOfDay, null, '[]' );
    }

    /**
     * Gets last clock out for shift.
     * It finds the last clock out before the next shift's start
     * if Found returns clock out, if not false.
     * @param {Array} timeclockPairs
     * @param {Object} shiftRange
     * @param {Object} nextShift
     * @returns {*} returns {Object} if found else {Boolean}
     */
    getLastShiftsClockOut( timeclockPairs, shiftRange, nextShift ) {
        let nextShiftStart;
        if ( isEmpty( nextShift ) ) {
            const lastClockIn = findLast( timeclockPairs, ( timeclockPair ) =>
                moment( timeclockPair.clockIn.timestamp, 'X' ).isBetween( shiftRange.start, shiftRange.end, 'minute', '[]' )
            );
            nextShiftStart = moment( lastClockIn.clockIn.timestamp, 'X' ).add( 15, 'hour' );
        } else {
            nextShiftStart = moment( `${nextShift.date} ${nextShift.start}`, DEFAULT_SHIFT_DATETIME_FORMAT );
        }

        const clockPair = findLast( timeclockPairs, ( timeclockPair ) =>
            moment( timeclockPair.clockOut.timestamp, 'X' ).isBetween( shiftRange.start, nextShiftStart, 'minute', '[]' )
        );

        return clockPair.clockOut || false;
    }

    /**
     * Get Unix Epoch time for time record
     *
     * @param {String} datetime date in format YYYY-MM-DD HH:mm
     * @returns {String} Unix epoch time value
     */
    getUnixEpochTime( datetime ) {
        return moment( datetime, DEFAULT_SHIFT_DATETIME_FORMAT ).format( 'X' );
    }

    /**
     * Format number of minutes into time string.
     * @param  {Number} minutes
     * @return {String} HH:mm
     */
    formatMinutesToTime( minutes ) {
        let h = parseInt( Math.floor( minutes / 60 ), 10 );
        let m = parseInt( minutes % 60, 10 );

        h = h < 10 ? `0${h}` : h;
        m = m < 10 ? `0${m}` : m;

        return `${h}:${m}`;
    }

    /**
     * Formats time clock values for display
     *
     * @param  {Object} timeclock
     * @return {Object} Formated time clock
     */
    formatTimeClockForDisplay( timeclock ) {
        return {
            timestamp: timeclock.timestamp,
            datetime: moment( timeclock.timestamp, 'X' ).utcOffset( '+0800' ).format( DEFAULT_SHIFT_DATETIME_FORMAT ),
            tags: timeclock.tags,
            max_clock_out: timeclock.max_clock_out,
            type: timeclock.state ? 'clock_in' : 'clock_out',
            offset: '+0800'
        };
    }

    /**
     * This method converts a human readable time into minutes (integer)
     *
     * @param {String} time
     *
     * @returns {Integer}
     */
    timeToMinutes( time, format ) {
        const momentObj = moment( time, format );
        const hours = momentObj.hour() * 60;
        const minutes = momentObj.minute();
        return hours + minutes;
    }

    /**
     * Get current server time
     *
     * @param {Function} accept
     * @param {Function} reject
     *
     * @returns {Void}
     */
    getServerTime( accept, reject ) {
        Fetch( '/ess/time', { method: 'GET' })
            .then( ( response ) => {
                accept( response.data );
            })
            .catch( () => {
                Vue.prototype.$flashMessage.show(
                    'Failed to sync the web bundy with the server time. '
                    + 'Please reload your employee self service page and try again.', 'error'
                );

                reject();
            });
    }

    /**
     * This method fetches the last timeclock entry
     *
     * @returns {Object} Last timeclock entry
     */
    getLastTimeClock() {
        return axios.get( 'ess/clock_state/timesheet/last_entry' ).then( ( response ) => response.data );
    }

    /**
     * Set the last timeclock entry
     *
     * @param {Object} lastEntry
     *
     * @returns {Void}
     */
    setLastTimeClock( item ) {
        if ( 'state' in item ) {
            const user = JSON.parse( localStorage.getItem( 'user' ) );

            const entry = {
                employee_uid: parseInt( user.employee_id, 10 ),
                company_uid: user.company_id,
                state: item.state,
                tags: item.tags,
                timestamp: parseInt( item.timestamp, 10 ),
                max_clock_out: item.max_clock_out
            };

            const updatedUser = Object.assign({}, JSON.parse( localStorage.getItem( 'user' ) ), {
                timeclock_last_entry: entry
            });

            localStorage.setItem( 'user', JSON.stringify( updatedUser ) );
        }
    }
}

export const timeClockService = new TimeClockService();
