import axios from 'axios';

/**
 * Teams service class
 */
export default class TeamRequestService {

    /**
     * Fetches list of teams
     * @return {promise}
     */
    getTeamsList( isRootModule ) {
        return axios.get( '/ess/teams', { isRootModule }
            ).then( ({ data }) => data.data );
    }

    /**
     * Fetches team details
     * @param {number} team id
     * @return {promise}
     */
    getTeamDetails( teamId ) {
        return axios.get( `/ess/teams/${teamId}` ).then( ({ data }) => data.data );
    }

    /**
     * Fetches team details
     * @param {number} team id
     * @return {promise}
     */
    getTeamCalendarData( teamId, startDate, endDate ) {
        return axios.get( `/ess/teams/${teamId}/calendar?start_date=${startDate}&end_date=${endDate}` ).then( ({ data }) => data );
    }

    /**
     * regenerate team attendance
     * @param {Number} teamId
     * @param {Object} payload
     * @return {promise}
     */
    regenerateAttendance( teamId, payload ) {
        return axios.post( `/ess/teams/${teamId}/attendance/regenerate`, payload );
    }

    /**
     * get regeneration status
     * @param {number} team id
     * @param {String} job id
     * @return {promise}
     */
    regenerationStatus( teamId, jobId ) {
        return axios.get( `/ess/teams/${teamId}/attendance/regenerate/${jobId}` ).then( ({ data }) => data );
    }

    /**
     * get regeneration errors
     * @param {number} team id
     * @param {String} job id
     * @return {promise}
     */
    regenerationErrors( teamId, jobId ) {
        return axios.get( `/ess/teams/${teamId}/attendance/regenerate/${jobId}/errors/download` ).then( ({ data }) => data );
    }

    /**
     *  Fetches single team and it's members
     * @param {number} team id
     * @param {number} page number
     * @return {promise}
     */
    getTeamMembers( teamId, page, perPage = 20 ) {
        return axios.get( `/ess/teams/${teamId}/members?page=${page}&per_page=${perPage}` ).then( ({ data }) => data );
    }

    /**
     * Fetches member info
     * @param {Number} teamId
     * @param {Number} memberId
     * @return {promise}
     */
    getMemberInfo( teamId, memberId ) {
        return axios.get( `/ess/teams/${teamId}/members/${memberId}` ).then( ( response ) => response.data );
    }

    /**
     * Fetches member scheduled shifts
     * @param {Number} teamId
     * @param {Number} memberId
     * @param {String} startDate
     * @param {String} endDate
     * @return {promise}
     */
    getMemberCalendarData( teamId, memberId, startDate, endDate ) {
        return axios.get( `/ess/teams/${teamId}/members/${memberId}/calendar?start_date=${startDate}&end_date=${endDate}` ).then( ( response ) => response.data );
    }

    /**
     * Get a list entitled schedules of employee
     * @param {Number} teamId
     * @param {Number} memberId
     * @return {promise}
     */
    getEmployeeSchedules( teamId, memberId ) {
        return axios.get( `/ess/teams/${teamId}/members/${memberId}/schedules` ).then( ( response ) => response.data );
    }

    /**
     * Get a list of assigned shifts of employee
     * @param {Number} teamId
     * @param {Number} memberId
     * @param {String} selectedDate
     * @return {promise}
     */
    getEmployeeAssignedShift( teamId, memberId, selectedDate ) {
        return axios.get( `/ess/teams/${teamId}/members/${memberId}/shifts?date=${selectedDate}` ).then( ( response ) => response.data );
    }

    /**
     * This overwrites existing employee shift.
     * @param {Number} teamId
     * @param {Number} memberId
     * @param {Object} payload
     * @return {promise}
     */
    assignEmployeeShift( teamId, memberId, payload ) {
        return axios.post( `/ess/teams/${teamId}/members/${memberId}/shifts`, payload );
    }

    /**
     * Get a list of assigned rest day of employee
     * @param {Number} teamId
     * @param {Number} memberId
     * @param {String} selectedDate
     * @return {promise}
     */
    getEmployeeAssignedRestDay( teamId, memberId, selectedDate ) {
        return axios.get( `/ess/teams/${teamId}/members/${memberId}/rest_day?date=${selectedDate}` ).then( ( response ) => response.data );
    }

    /**
     * assign rest day on employee.
     * @param {Number} teamId
     * @param {Number} memberId
     * @param {Object} payload
     * @return {promise}
     */
    assignEmployeeRestDay( teamId, memberId, payload ) {
        return axios.post( `/ess/teams/${teamId}/members/${memberId}/rest_day`, payload );
    }
}

export const teamService = new TeamRequestService();
