import axios from 'axios';
/**
 * Announcement service class
 */
export default class AnnouncementService {

    myAnnouncements = [];
    otherAnnouncements = [];
    /**
     * Submit Announcement
     * @return {Promise}
     */
    submitAnnouncement( announcement ) {
        return axios.post( 'ess/announcement', announcement )
            .then( ( response ) => response.data );
    }

    /**
     * Gets Announcement by id
     * @return {Promise}
     */
    getAnnouncementById( announcementId ) {
        return axios.get( `ess/announcement/${announcementId}` )
            .then( ( response ) => response.data );
    }

    /**
     * Gets the announcements.
     *
     * @return {Promise} The announcements.
     */
    getAnnouncements( body, page ) {
        return axios.post( `ess/employee/announcements?page=${page}`, body )
            .then( ( response ) => response.data );
    }

    /**
     * Parses the announcements
     *
     * @param {Array} announcements The announcements
     * @param {Object} user The user
     */
    parseAnnouncements( announcements, user ) {
        this.myAnnouncements = [];
        this.otherAnnouncements = [];

        announcements.forEach( ( announcement ) => {
            if ( user.user_id === announcement.sender_id ) {
                this.myAnnouncements.push( announcement );
            } else {
                this.otherAnnouncements.push( announcement );
            }
        });
    }
    /**
     * Get count of filtered company T&A employees using department, location, position or employee id.
     * @param {*} employeeId
     * @returns {Promise}
     */
    getViewCount( affectedEmployees ) {
        return axios.post( 'ess/employee/count', { affected_employees: affectedEmployees })
            .then( ( response ) => response.data.count );
    }

    /**
     * Gets my announcements.
     *
     * @return {Array} My announcements.
     */
    getMyAnnouncements() {
        return this.myAnnouncements;
    }

    /**
     * Gets the other announcements.
     *
     * @return {Array} The other announcements.
     */
    getOtherAnnouncements() {
        return this.otherAnnouncements;
    }

    /**
     * Submits received announcement reply.
     * @param {Number} announcementId
     * @param {FormData} message
     * @returns {Promise}
     */
    submitReply( announcementId, message ) {
        return axios.post( `ess/announcement/${announcementId}/reply`, { message })
            .then( ( response ) => response.data );
    }

    /**
     * Gets the reply.
     *
     * @param {Number} replyId  The reply identifier
     * @return {Promise} The reply.
     */
    getReply( replyId ) {
        return axios.get( `ess/announcement/reply/${replyId}` )
            .then( ( response ) => response.data );
    }

    /**
     * Updates reply to read.
     * @param {NUmber} replyId
     * @returns {Promise} replyId
     */
    markReplyAsRead( replyId ) {
        return axios.put( `ess/announcement/reply/${replyId}/seen` )
            .then( ( response ) => response.data );
    }

    /**
     * Mark announcement as seen.
     *
     * @param  {Number} announcementId [description]
     * @return {Promise}
     */
    markAnnouncementAsSeen( announcementId ) {
        return axios.put( `ess/announcement/${announcementId}/recipient_seen` );
    }

    /**
     * Get users unread announcements.
     * @return {Promise}
     */
    getUnreadAnnouncements() {
        return axios.get( 'ess/employee/announcements/unread', { isRootModule: true })
            .then( ( response ) => response.data.data );
    }
}

export const announcementService = new AnnouncementService();
