import axios from 'axios';
import { filter } from 'lodash-es';
import { timeClockService } from './TimeClockService';

/**
 * HoursWorked service class
 */
export default class HoursWorkedService {

    /**
     * Get the entire hours worked in date range for the user.
     *
     * @return {Promise}
     */
    getEmployeeHoursWorked( startDate = '', endDate = '' ) {
        return axios.get( `ess/hours_worked?start_date=${startDate}&end_date=${endDate}` )
            .then( ( response ) => response.data );
    }

    /**
     * Get hours worked for shift
     *
     * @param {Array} hoursWorked
     * @param {Object} shift
     * @returns {Array} hours worked for shift
     */
    getHoursWorkedForShift( hoursWorked, shift ) {
        return filter( hoursWorked, { date: shift.date, shift_id: shift.shift_id });
    }

    /**
     * Convert time string to minutes
     *
     * @param  {String} time
     * @return {Number}
     */
    convertTimeStringToMinsInt( time ) {
        return time.split( ':' ).reduce( ( sum, current, index ) =>
            sum + current / ( 60 ** index )
        , 0 ) * 60;
    }

    /**
     * Returns number of working minutes
     *
     * @param {Array} hoursWorked
     * @returns {Number}
     */
    getMinutesWorked( hoursWorked ) {
        return hoursWorked.reduce( ( acc, el ) => acc + timeClockService.timeToMinutes( el.time, 'HH:mm' ), 0 );
    }
}

export const hoursWorkedService = new HoursWorkedService();
