import axios from 'axios';

/**
 * Request messages service class
 */
export default class RequestMessagesService {
    /**
     * Stores request message
     * @param Number requestId request id
     * @param String messageContent message content
     * @return Promise
     */
    storeRequestMessage( requestId, messageContent ) {
        const data = {
            request_id: requestId,
            content: messageContent
        };

        return axios.post( 'ess/request/send_message', data )
            .then( ( response ) => response.data );
    }

}
export const requestMessagesService = new RequestMessagesService();
