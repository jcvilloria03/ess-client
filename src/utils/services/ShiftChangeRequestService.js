import axios from 'axios';
import moment from 'moment';
import { findLast, find } from 'lodash-es';
import { shiftsService } from './ShiftsService';

/**
 * Shift change request service class
 */
export default class ShiftChangeRequestService {

    /**
     * Post new shift change request.
     * @param {Object} data request data
     * @return {Promise}
     */
    submitShiftChangeRequest( data ) {
        return axios.post( 'ess/shift_change_request', data )
            .then( ( response ) => response.data );
    }

    /**
     * Fetches request with messages.
     * @param {Number} id Shift Change Request ID
     * @return {Promise}
     */
    fetchShiftChangeRequest( id ) {
        return axios.get( `ess/shift_change_request/${id}` )
            .then( ( response ) => response.data );
    }

    /**
     * Iterates through day shifts and schedules.
     * Checks is allowed to add rest day on that schedule.
     * @param {Object} eventType to be added
     * @param {Array} assignedDayShifts
     * @returns {Boolean}
     */
    daySchedulesDoesNotAllowRestDay( eventType, assignedDayShifts ) {
        if ( !eventType.rest_day_id ) {
            return false;
        }

        const dayShiftOnRestDay = find( assignedDayShifts, ( dayShift ) =>
            dayShift && dayShift.schedule && !dayShift.schedule.on_rest_day
        );

        return !!dayShiftOnRestDay;
    }

    /**
     * Gets last assigned day event.
     * @param {Array} dayEvents
     * @returns {Object}
     */
    getLastAssingedDayEvents( dayEvents ) {
        return findLast( dayEvents, ( dayEvent ) =>
            dayEvent && dayEvent.isAssigned
        );
    }

    /**
     * Checks is eventType overlaping with last event from previouse day.
     * @param {Object} eventType
     * @param {Array} previouseDayShifts
     * @returns {Boolean}
     */
    isEventOverlapingWithPreviouseDayEvents( eventType, previouseDayShifts ) {
        const previouseDayNightShift = this.getLastAssingedDayEvents( previouseDayShifts );

        if ( !previouseDayNightShift ) {
            return false;
        }

        const nightShiftEnd = this.getEventTime( previouseDayNightShift.end );
        const nightShiftStart = this.getEventTime( previouseDayNightShift.start );
        const eventStart = this.getEventTime( eventType.start );

        if ( this.isNightShiftEndSameOrAfterEventStart( nightShiftEnd, nightShiftStart ) ) {
            return false;
        }

        if ( this.isRestDayOverlapingPreviouseDayNightShift( eventType, previouseDayNightShift ) ) {
            return true;
        }

        return this.isNightShiftEndSameOrAfterEventStart( nightShiftEnd, eventStart );
    }

    /**
     * Checks is rest day overlaping previouse day night shift.
     * @param {Object} eventType
     * @param {Object} previouseDayNightShift
     * @returns {Boolean}
     */
    isRestDayOverlapingPreviouseDayNightShift( eventType, previouseDayNightShift ) {
        if ( this.isRestDay( eventType ) && this.isRestDay( previouseDayNightShift ) ) {
            return false;
        }

        return this.isRestDay( eventType ) && !this.assignedEventCanBeOnRestDay( previouseDayNightShift );
    }
    /**
     * Checks can assigned shift be on rest day.
     * @param {Object} assignedEvent
     * @return {Boolean}
     */
    assignedEventCanBeOnRestDay( assignedEvent ) {
        return !!( assignedEvent.isAssigned && assignedEvent.schedule && assignedEvent.schedule.on_rest_day );
    }

    /**
     * Checks is eventType rest day.
     * @param {Object} eventType
     * @returns {Boolean}
     */
    isRestDay( eventType ) {
        return !!( eventType && eventType.rest_day_id );
    }

    /**
     * Checks is nightShiftEnd same or after eventStart.
     * @param {String} nightShiftEnd
     * @param {String} eventStart
     * @returns {Boolean}
     */
    isNightShiftEndSameOrAfterEventStart( nightShiftEnd, eventStart ) {
        if ( shiftsService.areDatesValidMomentObjects( nightShiftEnd, eventStart ) ) {
            return false;
        }

        return moment( nightShiftEnd, 'HH:mm' ).isSameOrAfter( moment( eventStart, 'HH:mm' ), 'hour' );
    }

    /**
     * Gets event time.
     * @param {Object} eventTime
     * @returns {String}
     */
    getEventTime( eventTime ) {
        return moment( eventTime, 'HH:mm' ).format( 'HH:mm' );
    }

    /**
     * Checks is night shift.
     * @param {String} shiftStart
     * @param {String} shiftEnd
     * @returns {Boolean}
     */
    isNightShift( shiftStart, shiftEnd ) {
        return moment( shiftStart ).isAfter( shiftEnd );
    }

    /**
     * Check is night shift allowed on rest day.
     * @param {Object} eventType
     * @param {Object} nextDayShift
     * @returns {Boolean}
     */
    isNightShiftAllowedOnRestDay( eventType, nextDayShift ) {
        if ( this.assignedEventCanBeOnRestDay( eventType ) && this.isRestDay( nextDayShift ) ) return false;

        return !this.isEventRestDayOrNextDayHasEvents( eventType, nextDayShift ) ||
            this.isNightShiftOnNextDayRestDayAllowed( eventType, nextDayShift );
    }

    /**
     * Checks is event to be added rest day or is any event assigned to next day.
     * @param {Object} eventType
     * @param {Object} nextDayShift
     * @returns {Boolean}
     */
    isEventRestDayOrNextDayHasEvents( eventType, nextDayShift ) {
        return this.isRestDay( eventType ) || !nextDayShift || !nextDayShift.isAssigned;
    }

    /**
     * Checks is event to be added night shift and is it allowed on rest day.
     * @param {Object} eventType
     * @param {Object} nextDayShift
     * @returns {Boolean}
     */
    isNightShiftOnNextDayRestDayAllowed( eventType, nextDayShift ) {
        return this.isRestDay( nextDayShift ) && eventType.schedule && eventType.schedule.on_rest_day;
    }

    /**
     * Checks is allowed to add a new day event on day with rest day assigned.
     * @param {Object} eventType
     * @param {Array} assignedDayShifts
     * @returns {Boolean}
     */
    isNewEventAllowedOnRestDay( eventType, assignedDayShifts ) {
        const assignedRestDay = find( assignedDayShifts, ( dayShift ) =>
            this.isRestDay( dayShift )
        );

        return !( !assignedRestDay || eventType.schedule.on_rest_day );
    }

    /**
     * Check is shift to be added overlaping with existing day or next day shifts.
     * @param {Object} eventType
     * @param {String} shiftStart
     * @param {String} shiftEnd
     * @param {String} assignedDayShifts
     * @returns {Boolean}
     */
    isShiftOverlapingWithDayShifts( eventType, shiftStart, shiftEnd, assignedDayShifts ) {
        if ( eventType.rest_day_id ) {
            return false;
        }

        const isOverlapingDayShifts = find( assignedDayShifts, ( dayShift ) => {
            if ( !dayShift || this.isRestDay( dayShift ) ) {
                return false;
            }

            const dayShiftStart = shiftsService.getShiftDateTime( dayShift.date, dayShift.start );
            const dayShiftEnd = shiftsService.getShiftDateTime( dayShift.date, dayShift.end );

            return this.isTimeBetweenTimeRange( shiftStart, shiftEnd, dayShiftStart, dayShiftEnd );
        });

        return !!isOverlapingDayShifts;
    }

    /**
     * Checks is shift start or end datetime between day shift datetime range.
     * @param {String} shiftStart shift start datetime
     * @param {String} shiftEnd shift end datetime
     * @param {Object} dayShiftStart start of day shift
     * @param {Object} dayShiftEnd end of day shift
     * @returns {Boolean}
     */
    isTimeBetweenTimeRange( shiftStart, shiftEnd, dayShiftStart, dayShiftEnd ) {
        return moment( shiftStart ).isBetween( dayShiftStart, dayShiftEnd, null, []) ||
            moment( shiftEnd ).isBetween( dayShiftStart, dayShiftEnd, null, []);
    }
}

export const shiftChangeRequestService = new ShiftChangeRequestService();
