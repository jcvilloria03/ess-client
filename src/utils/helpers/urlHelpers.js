/**
 * Open URL
 */
export function open( url ) {
    window.location.href = url;
}
