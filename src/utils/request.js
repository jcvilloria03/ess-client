import axios from 'axios';

axios.defaults.baseURL = process.env.API_URL;
axios.defaults.headers.common[ 'X-Requested-With' ] = 'XMLHttpRequest';
axios.defaults.headers.post[ 'Content-Type' ] = 'application/json';
axios.defaults.headers.common[ 'Access-Control-Allow-Origin' ] = window.location.origin || `${window.location.protocol}//${window.location.host}`;

/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
function parseDATA( response ) {
    return response.data;
}

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
function checkStatus( response ) {
    if ( response.status >= 200 && response.status < 300 ) {
        return response;
    }
    const error = new Error( response.statusText );
    error.response = response;
    throw error;
}

/**
 * Requests a URL, returning a promise
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "axios"
 *
 * @return {object}           The response data
 */
export function Fetch( url, options ) {
    if ( localStorage.getItem( 'access_token' ) !== null ) {
        axios.defaults.headers.common[ 'Authorization' ] = `Bearer ${localStorage.getItem( 'access_token' )}`; // eslint-disable-line dot-notation
    }
    return axios( url, options )
    .then( checkStatus )
    .then( parseDATA );
}
