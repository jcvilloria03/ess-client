import { get } from 'lodash-es';
import { EMPLOYEE_REQUEST_NAMES } from '@/constants/employee-request';

/**
 * Get route for request details.
 * @param {Object} request
 * @return {Object} route
 */
export function getRequestDetailsRoute( request ) {
    return {
        name: `requests.${request.request_type.replace( /_/g, '-' )}.details`,
        params: {
            requestId: request.request.id
        }
    };
}

/**
 * Get route for approval details.
 * @param {Object} request
 * @return {Object} route
 */
export function getApprovalDetailsRoute( request ) {
    return {
        name: `approvals.${request.request_type.replace( /_/g, '-' )}.details`,
        params: {
            requestId: request.request.id
        }
    };
}

/**
 * Get request name.
 * @param {Object} request
 * @return {String}
 */
export function getRequestName( request ) {
    const name = get( EMPLOYEE_REQUEST_NAMES, request.request_type, 'Request' );

    if ( typeof name === 'function' ) {
        return name( request );
    }

    return name;
}
