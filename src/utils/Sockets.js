import Vue from 'vue';
import VueEcho from 'vue-echo-laravel';

window.io = require( 'socket.io-client' );

const parser = document.createElement( 'a' );
parser.href = process.env.SOCKETS_URL;
const HOST = `${parser.protocol}//${parser.host}`;
const PATH = `${parser.pathname}socket.io`;

/**
 * Sockets class for websocket.
 */
export default class Sockets {
    /**
     * Add VueEcho plugin. Setup broadcaster, socket host and auth.
     */
    init() {
        Vue.use( VueEcho, {
            broadcaster: 'socket.io',
            host: HOST,
            path: PATH,
            auth: {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem( 'access_token' )}`
                }
            }
        });
    }
}

export const sockets = new Sockets();
