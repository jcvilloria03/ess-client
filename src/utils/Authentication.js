import axios from 'axios';
import router from '@/router';
import { isTokenExpired } from '@/utils/helpers/jwtHelpers';
import { open } from '@/utils/helpers/urlHelpers';
import { teamService } from '@/utils/services/TeamRequestService';
import { LOGIN_PAGE_SNACKBAR_MESSAGE } from '@/constants';

/**
 * Authentication class for Auth0
 */
export default class AuthService {
    /**
     * Class constructor
     */
    constructor() {
        this.login = this.login.bind( this );
        this.logout = this.logout.bind( this );
        this.isAuthenticated = this.isAuthenticated.bind( this );
        this.isEmailVerified = this.isEmailVerified.bind( this );
    }

    /**
     * Login function
     */
    login({ username, password }) {
        const data = {
            data: {
                user: {
                    email: username,
                    password
                }
            }
        };

        return axios.post( '/auth/user/login', data )
            .then( ( response ) => response.data );
    }

    /**
     * Clear local storage except log-in message
     */
    clearLocalStorageExceptMessage() {
        const message = localStorage.getItem( LOGIN_PAGE_SNACKBAR_MESSAGE );
        localStorage.clear();

        if ( message ) {
            localStorage.setItem( LOGIN_PAGE_SNACKBAR_MESSAGE, message );
        }
    }

    /**
     * Handles logout flow
     */
    logout() {
        axios.post( `${process.env.AUTHN_API_BASE_URI}/auth/logout`, {}, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem( 'access_token' )}`
            }
        })
        .then( () => {
            this.clearLocalStorageExceptMessage();
            // navigate to the login route
            if ( process.env.NODE_ENV === 'development' ) {
                router.replace({
                    name: 'login'
                });
            } else {
                open( '/login' );
            }
        });
    }

    /**
     * Checks email verification status of auth0 account
     * @return {Boolean}
     */
    isEmailVerified() {
        return this.getUser().email_verified;
    }

    /**
     * Checkes user authentication status
     * @return {Boolean}
     */
    isAuthenticated() {
        const token = localStorage.getItem( 'access_token' );

        return token && !isTokenExpired( token );
    }

    /**
     * Check if logged in user is an employee.
     * @return {Boolean}
     */
    isEmployee() {
        const user = this.getUser();

        return !!( user && user.employee );
    }

    /**
     * Check if user has permissions.
     * @return {Boolean}
     */
    hasPermissions() {
        const authZPermissions = this.getPermission();

        return !!( authZPermissions && authZPermissions.length );
    }

    /**
     * get user permissions in localStorage.
     * @return {Boolean}
     */
    getPermission() {
        return JSON.parse( localStorage.getItem( 'authz_permissions' ) );
    }

    /**
     * Fetch permissions
     * @return {Promise}
     */
    fetchPermissions() {
        return axios.get( '/users/permissions', {
            headers: {
                'X-Authz-Entities': 'root.admin'
            }
        }).then( ( response ) => response.data.data );
    }

    /**
     * Set permissions to local storage.
     * @param {Object} user
     */
    setPermissions( permissions ) {
        const authzPermissions = Object.keys( permissions );

        localStorage.setItem( 'authz_permissions', JSON.stringify( authzPermissions ) );
    }

    /**
     * Get user's token.
     * @return {String}
     */
    getToken() {
        return localStorage.getItem( 'access_token' );
    }

    /**
     * Sets authentication token
     */
    setAuthToken() {
        axios.defaults.headers.common.Authorization = `Bearer ${this.getToken()}`;
    }

    /**
     * Fetch logged-in user data
     * @return {Promise}
     */
    fetchUser() {
        const user = this.getUser();
        if ( user && user.employee ) {
            return user;
        }

        this.setAuthToken();

        const userId = this.getUserId();

        return axios.get( `/user/${userId}/initial-data`, {
            headers: {
                'X-Authz-Entities': 'root.ess'
            }
        }).then( ( response ) => response.data );
    }

    /**
     * Set user data to local storage.
     * @param {Object} user
     */
    setUserData( user ) {
        localStorage.setItem( 'user', JSON.stringify( user ) );
    }

    /**
     * Get user data
     * @return {Object}
     */
    getUser() {
        return JSON.parse( localStorage.getItem( 'user' ) );
    }

    /**
     * Gets User ID from local storage
     * @return {Number}
     */
    getUserId() {
        return parseInt( this.getUser().user_id, 10 );
    }

    /**
     * Gets Employee ID from local storage
     * @return {Number}
     */
    getEmployeeId() {
        return parseInt( this.getUser().employee_id, 10 );
    }

    /**
     * Check if user should change password.
     */
    userShouldChangePassword() {
        const user = this.getUser();

        return user && user.for_change_password;
    }

    /**
     * Sends request to api-gateway to update user's password.
     *
     * @param {Object} changePasswordPayload request options.
     * @returns {Promise}
     */
    changePassword( changePasswordPayload ) {
        return axios.post( `${process.env.AUTHN_API_BASE_URI}/auth/change-password`, changePasswordPayload, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem( 'access_token' )}`
            }
        })
        .then( ( response ) => response );
    }

    /**
     * Checks whether user is admin
     * @returns {Boolean}
     */
    isAdmin() {
        const user = this.getUser();
        return user && user.user_type && user.user_type !== 'employee';
    }

    /**
     * Sets user status
     * @param {string} status
     */
    setUserStatus( status ) {
        const user = Object.assign({}, this.getUser(), { status });
        this.setUserData( user );
    }

    /**
     * check if user is team leader
     */
    async isTeamLeader() {
        const response = await teamService.getTeamsList({ isRootModule: true });
        return response.length > 0;
    }

    /**
     * check if leader has permission to assign or edit shift
     */
    async hasAssignOrEditPermission( teamId ) {
        const response = await teamService.getTeamDetails( teamId );
        return response.edit_team_schedule;
    }
}

export const auth = new AuthService();
