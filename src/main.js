// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import axios from 'axios';
import Vuelidate from 'vuelidate';
import VueCarousel from 'vue-carousel';
import VTooltip from 'v-tooltip';
import IdleVue from 'idle-vue';
import flatPickr from 'vue-flatpickr-component';
import 'flatpickr/dist/flatpickr.css';
import get from 'lodash-es/get';
import set from 'lodash-es/set';
import each from 'lodash-es/each';
import isEmpty from 'lodash-es/isEmpty';
// import MediaQueries from 'vue-media-queries/dist/vue-media-queries';
import VueTagsInput from '@johmun/vue-tags-input';
import { auth } from '@/utils/Authentication';
import { open } from '@/utils/helpers/urlHelpers';
import { USER_STATUS, LOGIN_PAGE_SNACKBAR_MESSAGE, SESSION_IDLE_TIME_MS } from '@/constants';
import App from './App';
import router from './router';
import Components from './components/mobile/index';
import { DialogModal } from './components/mobile/modal';
import modulesMap from './modules_map';

// const mediaQueries = new MediaQueries();

Vue.config.productionTip = false;
axios.defaults.baseURL = process.env.API_URL;

axios.interceptors.request.use( ( config ) => {
    const [matchedRoute] = router.currentRoute.matched.slice( -1 );

    if ( matchedRoute !== undefined ) {
        const companyId = JSON.parse( localStorage.getItem( 'user' ) ).company_id;
        const permissions = auth.getPermission();
        const { path } = matchedRoute;
        const modules = [];

        each( modulesMap, ( item, index ) => {
            if ( path.includes( index ) ) {
                modules.push( item );
            }
        });

        const filteredModule = modules.filter( ( permission ) => permissions.includes( permission ) );

        if ( !isEmpty( filteredModule ) ) {
            if ( config.headers ) {
                if ( !config.headers[ 'X-Authz-Entities' ]) {
                    config.headers[ 'X-Authz-Entities' ] = config.isRootModule
                    ? 'root.ess'
                    : filteredModule.pop();
                }
                config.headers[ 'X-Authz-Company-Id' ] = companyId;
            } else {
                config.headers = {
                    'X-Authz-Entities': filteredModule.pop(),
                    'X-Authz-Company-Id': companyId
                };
            }
        }
    }

    return config;
});

// Set axios interceptor
axios.interceptors.response.use( ( response ) => response, ( error ) => {
    const { status, data = {}} = error.response;

    if ( status === 418 ) {
        auth.isAdmin()
            ? open( '/control-panel/subscriptions' )
            : auth.logout();
    } else if ( status === 309 ) {
        const payslipsRegex = new RegExp( 'payslips', 'i' );
        const isInPayslipsPage = payslipsRegex.test( get( router, 'history.current.path', '' ) );

        /**
         * Action codes:
         * 1 - Redirect
         * 2 - Logout
         */
        if ( data.action_code === 1 ) {
            auth.setUserStatus( USER_STATUS.SEMI_ACTIVE );
            !isInPayslipsPage && open( '/ess/payslips' );
        } else if ( data.action_code === 2 ) {
            const message = {
                title: data.title || 'Error',
                message: data.message || '',
                type: 'error'
            };

            localStorage.setItem( LOGIN_PAGE_SNACKBAR_MESSAGE, JSON.stringify( message ) );
            auth.logout();
        }
    } else if ( status >= 500 ) {
        const errorClone = Object.assign({}, error );

        set(
            errorClone,
            'response.data.message',
            'The system encountered a problem while processing your request. Please try again after 5 minutes. If the issue still persists, please contact support@salarium.com.'
        );

        return Promise.reject( errorClone );
    } else if ( status === 401 && data && data.message.includes( 'Unauthenticated.' ) ) {
        const message = {
            title: 'Unauthenticated.',
            message: 'Your session has been expired or revoked.',
            type: 'error'
        };

        localStorage.setItem( LOGIN_PAGE_SNACKBAR_MESSAGE, JSON.stringify( message ) );
        auth.clearLocalStorageExceptMessage();
    }

    return Promise.reject( error );
});

const eventsHub = new Vue();
Vue.use( IdleVue, {
    eventEmitter: eventsHub,
    idleTime: SESSION_IDLE_TIME_MS
});
Vue.use( flatPickr );
Vue.use( Components );
// Vue.use( mediaQueries );
Vue.use( Vuelidate );
Vue.use( VueCarousel );
Vue.use( VTooltip, {
    defaultClass: 'sl-c-tooltip'
});
Vue.component( 'vue-tags-input', VueTagsInput );

Vue.prototype.$modal = DialogModal;

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components: { App }
    // mediaQueries
});
