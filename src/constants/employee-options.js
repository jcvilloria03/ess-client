const SELECT_ALL_EMPLOYEES_OPTIONS = [
    {
        id: null,
        type: 'department',
        name: 'All departments'
    },
    {
        id: null,
        type: 'position',
        name: 'All positions'
    },
    {
        id: null,
        type: 'location',
        name: 'All locations'
    },
    {
        id: null,
        type: 'employee',
        name: 'All employee'
    }
];

export {
  SELECT_ALL_EMPLOYEES_OPTIONS
};
