export const TIME_TYPES_MAP = {
    regular: 'Regular',
    overtime: 'Overtime',
    undertime: 'Undertime',
    night_shift: 'Night',
    overtime_night_shift: 'Night, Overtime'
};
