import { template } from '@/utils/helpers/taggedStringTemplate';
import { get, keys } from 'lodash-es';

export const REQUEST_TYPE_PLACEHOLDERS = {
    leave_request: 'Leave',
    loan_request: 'Loan',
    overtime_request: 'Overtime',
    shift_change_request: 'Shift Change',
    time_dispute_request: 'Time Correction',
    undertime_request: 'Undertime'
};

/**
 * Get request name
 * @param {Object} activity
 * @return {String}
 */
function getRequestName( activity ) {
    const requestType = get( activity, 'params.request_type' );

    if ( requestType === 'leave_request' ) {
        return get( activity, 'params.leave_type.name' );
    }

    return REQUEST_TYPE_PLACEHOLDERS[ requestType ];
}

/**
 * Dinamicaly parses routes objects.
 * @param {Boolean} isApproval
 */
function parseDefaultRoutes( isApproval = false ) {
    const routes = {};

    keys( REQUEST_TYPE_PLACEHOLDERS ).forEach( ( type ) => {
        routes[ type ] = {
            name: `${isApproval ? 'approvals' : 'requests'}.${type.replace( /_/g, '-' )}.details`,
            params: {
                requestId: 'params.request_id'
            }
        };
    });

    return routes;
}

/**
 * Dinamicaly parses announcements routes objects.
 * @param {Boolean} isMyAnnouncement
 */
function parseAnnouncementsRoute( isMyAnnouncement = false ) {
    return {
        default: {
            name: `announcements.${isMyAnnouncement ? 'my' : 'other'}-announcements.details`,
            params: {
                announcementId: 'params.announcement_id'
            }
        }
    };
}
/**
 * Programatically get route for announcement reply or replies page.
 * This is done in order to preserve compatibility with old announcement replies,
 * and allow navigating to a specific reply page using previously missing reply id.
 * @param {Object} activity
 * @return {Object}
 */
function getAnnouncementReplyRoute( activity ) {
    const replyId = get( activity, 'params.reply_id' );

    if ( replyId ) {
        return {
            name: 'announcements.reply',
            params: {
                announcementId: get( activity, 'params.announcement_id' ),
                replyId
            }
        };
    }

    return {
        name: 'announcements.my-announcements.responses',
        params: {
            announcementId: get( activity, 'params.announcement_id' )
        }
    };
}

export default {
    request_pending_approval: {
        own: {
            messageTemplate: template`You filed a ${0} request.`,
            templateTags: [getRequestName],
            defaultValues: ['request'],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        },
        other: {
            messageTemplate: template`${0} filed a ${1} request.`,
            templateTags: [ 'params.employee_name', getRequestName ],
            defaultValues: [ 'Employee', 'request' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes( true )
        }
    },

    message_sent: {
        own: {
            messageTemplate: template`${0} sent a message concerning your ${1} request.`,
            templateTags: [ 'params.employee_name', getRequestName ],
            defaultValues: [ 'Approver', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        },
        other: {
            messageTemplate: template`${0} sent a message concerning ${1}s ${2} request.`,
            templateTags: [ 'params.employee_name', 'params.owner_name', getRequestName ],
            defaultValues: [ 'Approver', 'employee', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes( true )
        }
    },

    request_approval_rejected: {
        own: {
            messageTemplate: template`${0} declined your ${1} request.`,
            templateTags: [ 'params.employee_name', getRequestName ],
            defaultValues: [ 'Approver', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        },
        other: {
            messageTemplate: template`${0} decilined ${1}s ${2} request.`,
            templateTags: [ 'params.employee_name', 'params.owner_name', getRequestName ],
            defaultValues: [ 'Approver', 'employee', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes( true )
        }
    },

    request_approval_approved: {
        own: {
            messageTemplate: template`${0} approved your ${1} request.`,
            templateTags: [ 'params.employee_name', getRequestName ],
            defaultValues: [ 'Approver', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        },
        other: {
            messageTemplate: template`${0} approved ${1}s ${2} request.`,
            templateTags: [ 'params.employee_name', 'params.owner_name', getRequestName ],
            defaultValues: [ 'Approver', 'employee', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes( true )
        }
    },

    request_approved: {
        own: {
            messageTemplate: template`Your ${0} request has been approved.`,
            templateTags: [getRequestName],
            defaultValues: ['filed'],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        },
        other: {
            messageTemplate: template`${0}s ${1} request has been approved.`,
            templateTags: [ 'params.owner_name', getRequestName ],
            defaultValues: [ 'Employee', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes( true )
        }
    },

    request_rejected: {
        own: {
            messageTemplate: template`Your ${0} request has been declined.`,
            templateTags: [getRequestName],
            defaultValues: ['filed'],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        },
        other: {
            messageTemplate: template`${0}s ${1} request has been declined.`,
            templateTags: [ 'params.owner_name', getRequestName ],
            defaultValues: [ 'Employee', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes( true )
        }
    },

    request_auto_rejected: {
        own: {
            messageTemplate: template`Your ${0} has been auto-rejected due to shift change.`,
            templateTags: [getRequestName],
            defaultValues: ['request'],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        },
        other: {
            messageTemplate: template`${0}s ${1} request has been auto-rejected due to shift change.`,
            templateTags: [ 'params.owner_name', getRequestName ],
            defaultValues: [ 'Employee', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes( true )
        }
    },

    request_cancelled: {
        own: {
            messageTemplate: template`Your ${0} request has been cancelled`,
            templateTags: [getRequestName],
            defaultValues: ['filed'],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        },
        other: {
            messageTemplate: template`${0}s ${1} request has been cancelled`,
            templateTags: [ 'params.employee_name', getRequestName ],
            defaultValues: [ 'Employee', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes( true )
        }
    },
    request_created_and_approved: {
        own: {
            messageTemplate: template`${0} has filed a ${1} on your behalf. No further action is required.
                If you did not intend to take this leave please contact ${0} directly.`,
            templateTags: [ 'params.employee_name', getRequestName ],
            defaultValues: [ 'Employee', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        },
        other: {
            messageTemplate: template`${0} has filed a ${1} on your behalf. No further action is required.
                If you did not intend to take this leave please contact ${0} directly.`,
            templateTags: [ 'params.employee_name', getRequestName ],
            defaultValues: [ 'Employee', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes( true )
        }
    },
    request_updated: {
        own: {
            messageTemplate: template`${0} updated your ${1}`,
            templateTags: [ 'params.employee_name', getRequestName ],
            defaultValues: [ 'Employee', 'updated' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        },
        other: {
            messageTemplate: template`You updated ${1}`,
            templateTags: [ 'params.employee_name', getRequestName ],
            defaultValues: [ 'Employee', 'updated' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes( true )
        }
    },

    announcement_reply_sent: {
        own: {
            messageTemplate: template`${0} replied to your announcement`,
            templateTags: ['params.reply_sender.full_name'],
            defaultValues: ['Employee'],
            link: true,
            multipleRoutes: false,
            routes: {
                default: getAnnouncementReplyRoute
            }
        },
        other: {
            messageTemplate: template`${0} replied to ${1}'s announcement`,
            templateTags: [ 'params.reply_sender.name', 'params.owner.name' ],
            defaultValues: [ 'Employee', 'someone' ],
            link: true,
            multipleRoutes: false,
            routes: parseAnnouncementsRoute()
        }
    },

    announcement_published: {
        own: {
            messageTemplate: template`Your announcement has been published`,
            templateTags: [],
            defaultValues: [],
            link: true,
            multipleRoutes: false,
            routes: parseAnnouncementsRoute( true )
        },
        other: {
            messageTemplate: template`${0} has published an announcement`,
            templateTags: ['params.employee_name'],
            defaultValues: ['Someone'],
            link: true,
            multipleRoutes: false,
            routes: parseAnnouncementsRoute()
        }
    },

    'payslips.new': {
        own: {
            messageTemplate: template`Your payslip for ${0} payroll is ready for viewing`,
            templateTags: ['params'],
            defaultValues: ['the previous'],
            link: true,
            multipleRoutes: false,
            isPayslip: true,
            routes: {
                default: {
                    name: 'payslips'
                }
            }
        },
        other: {
            messageTemplate: template`Your payslip for ${0} payroll is ready for viewing`,
            templateTags: ['params'],
            defaultValues: ['the previous'],
            link: true,
            multipleRoutes: false,
            isPayslip: true,
            routes: {
                default: {
                    name: 'payslips'
                }
            }
        }
    }
};
