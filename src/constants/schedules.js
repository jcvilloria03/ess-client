export const SCHEDULES = {
    FLEXI: 'flexi',
    FIXED: 'fixed',
    REST: 'Rest Day'
};

export const SCHEDULE_TYPES = {
    flexi: 'Flexi Schedule',
    fixed: 'Fixed Schedule'
};

export const TOTAL_WEEK_COUNT = 30;

export const REPEAT_DAYS = [
    {
        label: 'Sun',
        value: 'sunday',
        selected: true
    },
    {
        label: 'Mon',
        value: 'monday',
        selected: false
    },
    {
        label: 'Tue',
        value: 'tuesday',
        selected: false
    },
    {
        label: 'Wed',
        value: 'wednesday',
        selected: false
    },
    {
        label: 'Thu',
        value: 'thursday',
        selected: false
    },
    {
        label: 'Fri',
        value: 'friday',
        selected: false
    },
    {
        label: 'Sat',
        value: 'saturday',
        selected: false
    }
];

export const DEFAULT_DATE_FORMAT = 'YYYY-MM-DD';
export const DEFAULT_DATETIME_FORMAT = 'YYYY-MM-DD HH:mm';
export const DEFAULT_TIME_FORMAT = 'HH:mm';
export const TIME_FORMAT = 'HH:mm:ss';
