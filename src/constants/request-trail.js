import { template } from '../utils/helpers/taggedStringTemplate';

export const COMPLEX_ACTIVITIES = [
    'request_created_by_admin'
];

export const REQUEST_TRAIL_DATA = {
    request_created: {
        message_template: template`${0} submitted ${1} request`,
        icon: 'fa fa-flag sl-u-text--success',
        employee: 'owner',
        timestamp: true,
        default_inputs: ['You']
    },
    request_created_by_admin: {
        message_template: template`${0} created a leave request on ${1} behalf.`,
        icon: 'fa fa-flag sl-u-text--success',
        employee: 'owner',
        timestamp: true,
        default_inputs: [ 'You', 'your' ]
    },
    request_updated: {
        message_template: template`${0} updated a leave request.`,
        icon: 'fa fa-flag sl-u-text--success',
        employee: 'admin',
        timestamp: true,
        default_inputs: ['You']
    },
    request_pending_approval: {
        message_template: template`Pending approval from ${0}`,
        icon: 'fa fa-circle sl-u-text--warning',
        employee: 'approver',
        default_inputs: ['you']
    },
    message_sent: {
        message_template: template`${0} send a message`,
        icon: 'fa fa-reply sl-u-text--disabled',
        employee: 'sender',
        timestamp: true,
        default_inputs: ['You']
    },
    request_approval_approved: {
        message_template: template`${0} approved the request`,
        icon: 'fa fa-check sl-u-text--success',
        employee: 'approver',
        timestamp: true,
        default_inputs: ['You']
    },
    request_approval_rejected: {
        message_template: template`${0} rejected the request`,
        icon: 'fa fa-times sl-u-text--danger',
        employee: 'approver',
        timestamp: true,
        default_inputs: ['You']
    },
    request_approved: {
        message_template: template`${0} has been approved!`,
        icon: 'fa fa-flag sl-u-text--success',
        employee: 'owner',
        timestamp: true,
        default_inputs: [ 'Your request', 'Request' ]
    },
    request_rejected: {
        message_template: template`${0} has been rejected!`,
        icon: 'fa fa-flag sl-u-text--danger',
        employee: 'owner',
        timestamp: true,
        default_inputs: [ 'Your request', 'Request' ]
    },
    request_auto_rejected: {
        message_template: template`Auto-rejected due to shift change.`,
        icon: 'fa fa-flag sl-u-text--danger',
        employee: 'owner',
        timestamp: true,
        default_inputs: []
    },
    request_cancelled: {
        message_template: template`${0} has been cancelled!`,
        icon: 'fa fa-ban sl-u-text--cancel',
        employee: 'owner',
        timestamp: true,
        default_inputs: [ 'Your request', 'Request' ]
    },
    request_reset: {
        message_template: template`${0} has been reset!`,
        icon: 'fa fa-flag sl-u-text--success',
        employee: 'owner',
        timestamp: true,
        default_inputs: [ 'Your request', 'Request' ]
    },
    request_approval_no_action: {
        message_template: template`No action is required from ${0}`,
        icon: 'fa fa-circle sl-u-text--warning',
        employee: 'approver',
        default_inputs: ['you']
    }
};

export const TIMESTAMP_FORMAT = 'HH:mm MMMM D, YYYY';
