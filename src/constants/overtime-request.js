export const DEFAULT_NUMBER_OF_HOURS = '00:00';
export const DEFAULT_SHIFT_DATETIME_FORMAT = 'YYYY-MM-DD HH:mm:ss';
export const DATE_FORMAT = 'MMMM DD, YYYY';
export const HOURS_MINUTES_FORMAT = 'HH:mm:ss';
export const NOT_AVAILABLE = 'N/A';
export const REST_DAY = 'Rest Day';
export const REST_DAY_TYPE = 'rest_day';
export const TIMECLOCK_ORIGIN = 'Web Bundy';
