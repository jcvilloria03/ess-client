export const TYPES = [ 'leaves', 'loans', 'overtime', 'shift_change', 'time_dispute', 'undertime' ];
export const DEFAULT_DATE_FORMAT = 'YYYY-MM-DD';
export const NEW_DATE_FORMAT = 'ddd D MMM YYYY';
export const MESSAGE_DATETIME_FORMAT = 'HH:mm MMMM D, YYYY';
export const STATUS = [ 'pending', 'approved', 'declined', 'cancelled', 'rejected' ];
export const DATE_FORMAT = 'MMMM DD, YYYY';
export const TIME_FORMAT = 'HH:mm:ss';
export const UNDERTIME_TIME_FIELDS_NAMES = [ 'startTime', 'endTime', 'hours' ];
export const UNDERTIME_FIELDS = {
    start: 'startTime',
    end: 'endTime',
    numberOfHours: 'hours'
};
export const INVALID_NUMBER_OF_HOURS = '00:00';

export const REQUEST_TYPES = {
    leaves: 'Leaves',
    loans: 'Loans',
    overtime: 'Overtime',
    shift_change: 'Shift Change',
    time_dispute: 'Time correction',
    undertime: 'Undertime'
};

export const REQUEST_STATUSES = {
    pending: 'Pending',
    approved: 'Approved',
    declined: 'Declined',
    cancelled: 'Cancelled',
    rejected: 'Rejected',
    processing: 'Approving / Declining / Canceling'
};

export const REQUEST_STATUS_DATA = {
    Pending: {
        icon: 'fa fa-check',
        color: 'sl-u-text--disabled'
    },
    Approved: {
        icon: 'fa fa-check',
        color: 'sl-u-text--success'
    },
    Declined: {
        icon: 'fa fa-times',
        color: 'sl-u-text--danger'
    },
    Cancelled: {
        icon: 'fa fa-times',
        color: 'sl-u-text--disabled'
    },
    Rejected: {
        icon: 'fa fa-times',
        color: 'sl-u-text--danger'
    }
};

export const FILE_TYPES = [
    'application/pdf',
    'text/plain',
    'image/jpg',
    'image/jpeg',
    'image/gif',
    'application/msword',
    'doc',
    'image/png'
];
export const FILE_UPLOAD_VALIDATION_MESSAGE = {
    invalidType: 'Invalid file type',
    fileSize: 'File size should not be more than 10 MB'
};
export const SIZE_OF_10_MB_IN_BYTES = 10485760;
export const SIZE_OF_KB = 1024;

export const SIZE_UNITS = [ 'Bytes', 'KB', 'MB' ];
export const FILE_ACTIONS = {
    upload: 'ess/attachments',
    replace: 'ess/attachments/replace'
};

/**
 * Employee request name based on request type.
 * @type {Object}
 */
export const EMPLOYEE_REQUEST_NAMES = {
    leave_request: ( request ) => request.request.leave_type.name,
    time_dispute_request: 'Time Correction',
    overtime_request: 'Overtime',
    shift_change_request: 'Shift Change',
    undertime_request: 'Undertime'
    // TODO: Add other request types.
};
export const EMPLOYEE_REQUEST_TYPES = {
    leave: 'leave_request'
};
export const EMPLOYEE_REQUEST_DATES = {
    startDate: {
        leave_request: ( request ) => request.request.start_date,
        overtime_request: ( request ) => request.request.date,
        time_dispute_request: ( request ) => request.request.start_date,
        shift_change_request: ( request ) => request.request.start_date,
        undertime_request: ( request ) => request.request.date
    },
    endDate: {
        leave_request: ( request ) => request.request.end_date,
        overtime_request: ( request ) => request.request.date,
        time_dispute_request: ( request ) => request.request.end_date,
        shift_change_request: ( request ) => request.request.end_date,
        undertime_request: ( request ) => request.request.date
    }
};

export const ATTACHMENT_HEADERS = {
    'Content-Type': 'multipart/form-data'
};

export const PRODUCTS = {
    ta: 'time and attendance',
    payroll: 'payroll',
    taPlusPayroll: 'time and attendance plus payroll'
};

// created new const for request type name to prevent
// any side effects from the previously used const above
export const REQUEST_TYPE_NAME = {
    leave_request: 'Leave',
    time_dispute_request: 'Time Correction',
    overtime_request: 'Overtime',
    shift_change_request: 'Shift Change',
    undertime_request: 'Undertime'
};

export const APPROVAL_REQUEST_TYPE = {
    LEAVE: 'leave_request',
    TIME_DISPUTE: 'time_dispute_request',
    OVERTIME: 'overtime_request',
    SHIFT_CHANGE: 'shift_change_request',
    UNDERTIME: 'undertime_request'
};
