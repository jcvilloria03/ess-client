export const CALENDAR_CONFIGURATION = {
    nowIndicator: true,
    defaultView: 'month',
    allDaySlot: true,
    slotLabelFormat: 'H:mm',
    fixedWeekCount: false,
    slotEventOverlap: false,
    eventOverlap: false,
    dayNamesShort: [ 'S', 'M', 'T', 'W', 'T', 'F', 'S' ],
    buttonText: {
        agendaDay: 'Day',
        listWeek: 'Week',
        month: 'Month'
    },
    views: {
        month: {
            titleFormat: 'MMMM YYYY',
            eventLimit: 5
        },
        day: {
            titleFormat: 'MMMM YYYY',
            columnHeaderFormat: 'DD dddd'
        }
    },
    header: {
        left: 'prev,next',
        center: 'agendaDay,listWeek,month',
        right: 'title filter'
    },

    customButtons: {
        filter: {
            icon: 'fa fa fa-filter',
            click: () => {
            }
        }
    },
    timeFormat: 'HH:mm'
};

export const TEAM_CALENDAR_CONFIGURATION = {
    nowIndicator: true,
    defaultView: 'listWeek',
    allDaySlot: true,
    slotLabelFormat: 'H:mm',
    fixedWeekCount: false,
    buttonText: {
        agendaDay: 'Day',
        listWeek: 'Week',
        month: 'Month'
    },
    header: {
        left: 'prev',
        center: 'agendaDay,listWeek,month',
        right: 'title next'
    },
    views: {
        week: {
            titleFormat: 'MMMM DD, YYYY',
            columnHeaderFormat: 'DD dddd'
        }
    }
};

export const CALENDAR_COLORS = {
    MY: {
        ATTENDANCE: {
            border: '#79AC78',
            background: '#85D054'
        },
        SHIFTS: {
            border: '#1078A3',
            background: '#81BBD1'
        },
        LEAVES: {
            border: '#F29748',
            background: '#F8CBA1'
        },
        PAYROLL_DISBURSEMENTS: {
            border: '#A8CBD7',
            background: '#D3E4EB'
        },
        HOLIDAYS: {
            border: '#F29748',
            background: '#F8CBA1'
        }
    },
    TEAM: {
        ATTENDANCE: {
            border: '#8F2CFA',
            background: '#8F2CFA'
        },
        SHIFTS: {
            border: '#598CFC',
            background: '#ABC5FD'
        },
        LEAVES: {
            border: '#E97677',
            background: '#F3B9BA'
        }
    },
    TEAM_VIEW: {
        infractions: {
            border: '#E02020',
            background: '#E02020'
        },
        noInfractions: {
            border: '#83D24B',
            background: '#83D24B'
        },
        pending: {
            border: '#00678F',
            background: '#00678F'
        },
        default: {
            border: '#cccccc',
            background: '#cccccc'
        }
    }
};

export const ATTENDANCE_SUMMARY = {
    INFRACTIONS: 'infractions',
    NO_INFRACTIONS: 'no_infractions',
    PENDING: 'pending'
};

export const TYPES = [
    'attendance', 'shifts', 'leaves', 'teamAttendance', 'teamShifts',
    'teamLeaves', 'payroll_disbursements'
];

export const CALENDAR_EMPLOYEE_TYPES = {
    attendance: 'Attendance',
    shifts: 'Shifts',
    leaves: 'Leaves',
    payroll_disbursements: 'Payroll Disbursments'
};

export const CALENDAR_TEAM_TYPES = {
    teamAttendance: 'Attendance',
    teamShifts: 'Shifts',
    teamLeaves: 'Leaves'
};
