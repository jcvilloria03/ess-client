import moment from 'moment';
import { DATE_FORMAT, TIME_FORMAT } from '@/constants/employee-request';

export default {
    filters: {
        /**
         * Formats date.
         * @param {String} date
         * @returns {String}
         */
        date( date ) {
            return moment( date ).format( DATE_FORMAT );
        },

        /**
         * Formats time.
         * @param {String} time
         * @returns {String}
         */
        time( time ) {
            return moment( time, TIME_FORMAT ).format( 'HH:mm' );
        },

        /**
         * Formats unix time.
         * @param {Number} time
         * @returns {String}
         */
        timestampToTime( time ) {
            return moment( time, 'X' ).format( 'HH:mm:ss' );
        }
    }
};
