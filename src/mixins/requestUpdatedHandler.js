import { employeeRequestService } from '@/utils/services/EmployeeRequestService';

export const handleUpdatedRequest = {
    created() {
        this.getUserDefinedRestDays();
    },

    /**
     * When component is destroyed leave click event listener.
     */
    destroyed() {
        this.leaveEssRequestChannel();
    },

    data() {
        return {
            userDefinedRestDays: []
        };
    },

    methods: {
        /**
         * Listening for request updated event broadcasts
         */
        listenEssRequestChannel( requestId = null ) {
            const employeeRequestId = requestId || this.request.employee_request_id;

            this.listenPrivateChannel(
                `ess-request.${employeeRequestId}`,
                'EssRequestUpdated',
                ( response ) => {
                    if ( !response ) {
                        return;
                    }
                    this.request = response.request.request;
                }
            );
        },

        /**
         * Leaving channel for request updated events
         */
        leaveEssRequestChannel( requestId = null ) {
            const employeeRequestId = requestId || ( this.request && this.request.employee_request_id );

            employeeRequestId && this.leaveChannel( `ess-request.${employeeRequestId}` );
        },

        /**
         * Listen private channel
         *
         * @param channel channel name
         * @param event event name
         * @param handler callback function
         */
        listenPrivateChannel( channel, event, handler ) {
            this.$echo.private( channel ).listen( event, handler );
        },

        /**
         * Leave private channel
         *
         * @param channel channel name
         */
        leaveChannel( channel ) {
            this.$echo.leave( channel );
        },

        /**
         * Fetch user defined rest days
         */
        getUserDefinedRestDays() {
            employeeRequestService.getEmployeeRestDays()
                .then( ( userDefinedRestDays ) => {
                    this.userDefinedRestDays = userDefinedRestDays;
                });
        }
    }
};
