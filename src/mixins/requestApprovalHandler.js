export default {
    methods: {
        /**
         * Handles request approval.
         * Sets approved request.
         * Redirects to requests list page if it's last level approver.
         * @param {Object} approval
         * @param {Boolean} isLastLevelApprover
         */
        handleRequestApproval( approval, isLastLevelApprover ) {
            this.approval = approval;
            if ( isLastLevelApprover ) {
                this.$router.push({ name: 'approvals.index' });
            }
        }
    }
};
