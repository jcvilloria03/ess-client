import { filter, first } from 'lodash-es';
import { shiftsService } from '@/utils/services/ShiftsService';
import { shiftChangeRequestService } from '@/utils/services/ShiftChangeRequestService';
import moment from 'moment';
import { DEFAULT_SHIFT_DATETIME_FORMAT } from '@/constants/overtime-request';

export default {
    computed: {
        /**
         * Computes assigned day shifts.
         * @returns {Array}
         */
        assignedDayShifts() {
            return filter( this.dayShifts, ( dayShift ) => dayShift.isAssigned );
        }
    },
    methods: {
        /**
         * Checks is add event action valid.
         * Handles add event validation messages.
         * @param {Object} eventType
         */
        isAddEventActionValid( eventType ) {
            const shiftStart = shiftsService.getShiftDateTime( eventType.date, eventType.start );
            let shiftEnd = shiftsService.getShiftDateTime( eventType.date, eventType.end );
            const nextDayShift = first( this.nextDayShifts );

            // Validates is schedule on rest day allowed
            if ( shiftChangeRequestService.daySchedulesDoesNotAllowRestDay( eventType, this.assignedDayShifts ) ) {
                this.$flashMessage.show( 'Day schedule doesn\'t allow rest day on same date', 'error' );

                return false;
            }

            // Validates is eventType to be added overlaping with previouse day nightShift.
            if (
                shiftChangeRequestService.isEventOverlapingWithPreviouseDayEvents( eventType, this.previouseDayShifts )
            ) {
                this.$flashMessage.show( 'Shift is overlaping with previouse day\'s  night shift', 'error' );

                return false;
            }

            // if it's night shift adds a day to shift end
            if ( shiftChangeRequestService.isNightShift( shiftStart, shiftEnd ) ) {
                shiftEnd = moment( shiftEnd ).add( 1, 'day' ).format( DEFAULT_SHIFT_DATETIME_FORMAT );

                // validates is night shift allowed on rest day if rest day is assigned to next day
                if ( shiftChangeRequestService.isNightShiftAllowedOnRestDay( eventType, nextDayShift ) ) {
                    this.$flashMessage.show( 'Night shift is overlaping next day shift or rest day', 'error' );

                    return false;
                }
            }

            // validates is event to be assigned allowed on rest day if day has assigned rest day
            if ( shiftChangeRequestService.isNewEventAllowedOnRestDay( eventType, this.assignedDayShifts ) ) {
                this.$flashMessage.show( 'Shift doesn\'t allow rest day on same day', 'error' );

                return false;
            }

            // validates is shift overlaping day shifts
            if (
                shiftChangeRequestService.isShiftOverlapingWithDayShifts( eventType, shiftStart, shiftEnd, this.assignedDayShifts )
            ) {
                this.$flashMessage.show( 'Shift is overlaping with day\'s shift', 'error' );

                return false;
            }

            // validates is shift overlaping first next day shift
            if ( !shiftsService.isShiftBeforeNextDayShift( shiftEnd, nextDayShift ) ) {
                this.$flashMessage.show( 'Shift is overlaping with next day shift', 'error' );

                return false;
            }

            return true;
        }
    }
}
;
