import { employeeRequestService } from '@/utils/services/EmployeeRequestService';

export default {
    data() {
        return {
            isDetailsView: true,
            requestMessages: []
        };
    },

    created() {
        this.getPendingRequests();
    },

    methods: {
        /**
         * Triggers confirmation modal for request cancellation.
         * Redirects to request list if action is confirmed.
         */
        cancelRequest() {
            this.$modal.confirm({
                type: 'danger',
                title: 'Discard changes',
                cancelText: 'Stay on this page',
                okText: 'Discard',
                content: 'You are about to leave page with unsaved changes. Do you wish to proceed?',
                onOk: () => {
                    this.$router.push({ name: 'requests.index' });
                }
            });
        },
        /**
         * Gets the pending requests.
         * @param {String} requestType requests type.
         */
        getPendingRequests( requestType, onSucess ) {
            const data = {
                statuses: ['pending'],
                types: [requestType]
            };
            this.$loader.show();
            employeeRequestService.getEmployeeRequests( 1, data ).then( ( requests ) => {
                if ( onSucess ) {
                    onSucess( requests );
                }
                this.$loader.hide();
            })
            .catch( () => this.$loader.hide() );
        },

        /**
         * Shows details component.
         */
        showDetails() {
            this.isDetailsView = true;
        },

        /**
         * Shows messages component.
         */
        showMessages() {
            this.isDetailsView = false;
        },
        /**
         * Listens for send-message child event and pushes message to array.
         */
        sendMessage( message ) {
            this.requestMessages.push( message );
        }
    }
};
