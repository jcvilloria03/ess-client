export default {
    data() {
        return {
            announcements: [],
            pagination: null,
            loading: false,
            page: 1
        };
    },

    computed: {
        /**
         * Calculate start offset number for current page.
         */
        from() {
            return this.pagination.current_page * this.pagination.per_page - this.pagination.per_page + 1;
        },

        /**
         * Calculate end offset number for current page.
         */
        to() {
            return this.from + this.pagination.count - 1;
        },

        /**
         * Return total number of approvals.
         */
        total() {
            return this.pagination.total;
        },

        /**
         * Disable "previous" button if there is no previous page.
         */
        buttonPreviousDisabled() {
            return this.loading || this.pagination.current_page < 2;
        },

        /**
         * Disable "next" button if there is no next page.
         */
        buttonNextDisabled() {
            return this.loading || this.pagination.current_page >= this.pagination.total_pages;
        }
    },

    watch: {
        announcementFilter() {
            this.getAnnouncements();
        }
    },

    created() {
        this.getAnnouncements();
    },

    methods: {
        /**
         * Show previous page.
         */
        previous() {
            this.page -= 1;
            this.getAnnouncements();
        },

        /**
         * Show next page.
         */
        next() {
            this.page += 1;
            this.getAnnouncements();
        }
    }
};
