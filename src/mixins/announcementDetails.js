import moment from 'moment';
import { announcementService } from '@/utils/services/AnnouncementService';
import { ANNOUNCEMENT_DEFAULT_DATETIME_FORMAT } from '@/constants/announcement-date';

export default {
    props: {
        announcementId: {
            type: [ String, Number ],
            required: true
        }
    },
    data() {
        return {
            announcement: null,
            error: null
        };
    },

    computed: {
        /**
         * Computes responses.
         * @return {Number}
         */
        responses() {
            return this.announcement.replies && this.announcement.replies
            ? this.announcement.replies.length
            : null;
        },

        /**
         * Formats announcement created_at date.
         * @returns {Date} formated date to 'HH:MM MMM D, YYYY'
         */
        formatedDate() {
            return moment( this.announcement.created_at ).format( ANNOUNCEMENT_DEFAULT_DATETIME_FORMAT );
        },

        /**
         * Splits announcement message using regexp.
         * @returns {Array} splited announcement messages
         */
        announcementMessageParagraphs() {
            return this.announcement.message.split( /(\r\n|\n|\r)/gm );
        }
    },

    watch: {
        announcementId() {
            this.getAnnouncementById();
        }
    },

    methods: {
        /**
         * Gets announcement by parsed Id
         */
        getAnnouncementById() {
            this.$loader.show();

            return announcementService.getAnnouncementById( this.announcementId )
                .then( ( announcement ) => {
                    this.announcement = announcement;

                    this.error = null;
                    this.$loader.hide();
                })
                .catch( ( error ) => {
                    this.error = error.message;
                    this.$loader.hide();
                });
        }
    }
};
