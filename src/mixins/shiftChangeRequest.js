export default {
    methods: {
        /**
         * Gets eventName by parsed even type.
         * @param {Object} eventType
         * @returns {String}
         */
        getEventNameByType( eventType ) {
            if ( eventType.shift_id && eventType.schedule.name ) {
                return `${eventType.schedule.name} (${eventType.schedule.start_time} to ${eventType.schedule.end_time})`;
            }

            if ( eventType.schedule_id && eventType.name ) {
                return `${eventType.name} (${eventType.start} to ${eventType.end})`;
            }

            return 'Rest Day';
        }
    }
};
