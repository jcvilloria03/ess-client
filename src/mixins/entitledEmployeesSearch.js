import { debounce } from 'lodash-es';
import { SELECT_ALL_EMPLOYEES_OPTIONS } from '@/constants/employee-options';
import { searchCompanyService } from '@/utils/services/SearchCompanyService';

export default {
    data() {
        return {
            entitledEmployeesOptions: [],
            isSearchingForEntitledEmployees: false,
            noEntitledEmployeesOptions: false
        };
    },

    methods: {
        /**
         * Get options from entitled employees.
         */
        getEntitledEmployeesOptions: debounce( function ( search, includeAdmins = false ) { // eslint-disable-line func-names
            this.isSearchingForEntitledEmployees = true;
            this.noEntitledEmployeesOptions = false;
            searchCompanyService
                .fetchEntitledEmployees( search, 10, includeAdmins )
                .then( ( options ) => {
                    this.entitledEmployeesOptions = [
                        ...this.makeOptionsTrackable( SELECT_ALL_EMPLOYEES_OPTIONS ),
                        ...this.makeOptionsTrackable( options )
                    ];
                    this.noEntitledEmployeesOptions = !options.length;
                    this.isSearchingForEntitledEmployees = false;
                });
        }, 500 ),

        /**
         * Make affected/entitled employees trackable with uid.
         * @param {Array} options
         * @return {Array}
         */
        makeOptionsTrackable( options ) {
            return options.map( ( item ) => {
                item.uid = item.type + item.id;
                return item;
            });
        },

        /**
         * Gets logged employee's company id.
         * @returns {Number} company_id
         */
        getCompanyId() {
            return JSON.parse( localStorage.getItem( 'user' ) ).company_id;
        }
    }
};
