/**
 * For a component to work with v-model, it must:
 * - accept a "value" prop
 * - emit an input event with the new value
 *
 * Bind 'input' to v-model.
 *
 * Custom input needs:
 * - id
 * - label text
 * - input name
 * - disabled state
 *
 * It handles input event:
 * - @on-blur
 * - @on-focus
 * - @on-change
 */
export const customInput = {
    props: {
        /**
         * Sets input id.
         */
        id: {
            type: String,
            required: true
        },

        /**
         * Sets input label.
         */
        label: String,

        /**
         * Sets input name.
         */
        name: String,

        /**
         * Disables the input.
         */
        disabled: Boolean,

        /**
         * Current input value.
         */
        value: [ Array, String, Number, Boolean ],

        /**
         * Sets input error state.
         */
        error: Boolean
    },
    computed: {
        input: {
            get() {
                return this.value || '';
            },
            set( newValue ) {
                this.$emit( 'input', newValue );
            }
        }
    },
    methods: {
        handleOnBlur( evt ) {
            this.$emit( 'onBlur', evt );
        },
        handleOnFocus( evt ) {
            this.$emit( 'onFocus', evt );
        },
        handleOnChange( evt ) {
            this.$emit( 'onChange', evt );
        }
    }
};
