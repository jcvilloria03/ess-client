import moment from 'moment';
import { REQUEST_STATUSES,
    EMPLOYEE_REQUEST_NAMES,
    EMPLOYEE_REQUEST_DATES,
    EMPLOYEE_REQUEST_TYPES
} from '@/constants/employee-request';
import { DATE_FORMAT } from '@/constants/overtime-request';

export default {
    computed: {
        /**
         * Computes request type name according to type.
         * @returns {String}
         */
        requestType() { // eslint-disable-line consistent-return
            return this.request.request_type === EMPLOYEE_REQUEST_TYPES.leave
                ? EMPLOYEE_REQUEST_NAMES[ this.request.request_type ]( this.request )
                : EMPLOYEE_REQUEST_NAMES[ this.request.request_type ];
        },
        /**
         * Parses route according to request type.
         * @returns {String} route name
         */
        routeName() {
            return `requests.${this.request.request_type.replace( /_/g, '-' )}.details`;
        },
        /**
         * Format request start date.
         */
        from() {
            return moment( EMPLOYEE_REQUEST_DATES.startDate[ this.request.request_type ]( this.request ) );
        },

        /**
         * Format request end date.
         */
        to() {
            return moment( EMPLOYEE_REQUEST_DATES.endDate[ this.request.request_type ]( this.request ) );
        },

        /**
         * Gets request status.
         * @returns {String} request's status
         */
        status() {
            return this.request.status;
        },

        /**
         * Check if request status is pending
         *
         * @return boolean
         */
        isStatusPending() {
            return this.status === REQUEST_STATUSES.pending;
        },

        /**
         * Check if request status is pending or approved
         *
         * @return boolean
         */
        isRequestCancelable() {
            return [ REQUEST_STATUSES.pending, REQUEST_STATUSES.approved ].includes( this.status );
        },

        /**
         * Sets icon for status field label
         * @returns String css class name
         */
        statusIcon() {
            return {
                'fa-times sl-u-text': this.status === REQUEST_STATUSES.declined || this.status === REQUEST_STATUSES.cancelled,
                'fa-check sl-u-text': this.status === REQUEST_STATUSES.approved
            };
        },

        /**
         * Sets class for status field
         * @returns String css class name
         */
        statusClass() {
            return {
                'sl-c-label--danger': this.status === REQUEST_STATUSES.declined || this.status === REQUEST_STATUSES.cancelled,
                'sl-c-label--success': this.status === REQUEST_STATUSES.approved
            };
        }
    },

    filters: {
        /**
         * Formats date to 'MMMM DD, YYYY' date format.
         * @param {*} date
         * @returns {Object}
         */
        formatDate( date ) {
            return moment( date ).format( DATE_FORMAT );
        }
    }
};
