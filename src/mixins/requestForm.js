export default {
    data() {
        return {
            firstStepFormIsVisible: true
        };
    },

    computed: {
        /**
         * Changes arrow icon's direction if section is visible or not.
         * @param Boolean sectionIsVisible
         * @returns String css class name for icon
         */
        firstStepArrowClass() {
            return {
                'fa-chevron-down': !this.firstStepFormIsVisible,
                'fa-chevron-up': this.firstStepFormIsVisible
            };
        }
    },

    methods: {
        /**
         * Toggles first step section.
         */
        toggleFirstStepForm() {
            this.firstStepFormIsVisible = !this.firstStepFormIsVisible;
        }
    }
};
