import moment from 'moment';
import { DATE_FORMAT } from '@/constants/overtime-request';

export default {
    filters: {
        /**
         * Formats date to 'MMMM DD, YYYY' date format.
         * @param {String} date
         * @returns {Object}
         */
        formatDateToMonthDayYear( date ) {
            if ( !date ) {
                return '';
            }

            return moment( date ).format( DATE_FORMAT );
        }
    }
};
