import { REQUEST_STATUSES } from '@/constants/employee-request';

export default {
    computed: {
        /**
         * Request details specific to the type of request.
         *
         * @return {Object}
         */
        request() {
            return this.employeeRequest.request;
        },

        /**
         * Sets class for show details icon.
         *
         * @returns {String} css class name
         */
        arrowClass() {
            return {
                'fa-arrow-circle-down': !this.requestDetailsVisible,
                'fa-arrow-circle-up': this.requestDetailsVisible
            };
        },

        /**
         * Sets icon for status field label.
         *
         * @returns {String} css class name
         */
        statusIcon() {
            return {
                'fa-times sl-u-text': this.status === REQUEST_STATUSES.declined || this.status === REQUEST_STATUSES.cancelled,
                'fa-check sl-u-text': this.status === REQUEST_STATUSES.approved
            };
        },

        /**
         * Sets class for status field.
         *
         * @returns {String} css class name
         */
        statusClass() {
            return {
                'sl-c-label--danger': this.status === REQUEST_STATUSES.declined || this.status === REQUEST_STATUSES.cancelled,
                'sl-c-label--success': this.status === REQUEST_STATUSES.approved
            };
        },

        /**
         * Convert request status code to string.
         *
         * @returns {String}
         */
        status() {
            return this.employeeRequest.status;
        },

        /**
         * Check if request status is pending.
         *
         * @return {Boolean}
         */
        isStatusPending() {
            return this.status === REQUEST_STATUSES.pending;
        },

        /**
         * Check if request status is pending or approved
         *
         * @return boolean
         */
        isRequestCancelable() {
            return [ REQUEST_STATUSES.pending, REQUEST_STATUSES.approved ].includes( this.status );
        }
    }
};
