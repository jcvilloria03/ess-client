export default {
    filters: {
        /**
         * Rounds number to two decimals.
         *
         * @param {Number}
         *
         * @returns {Number}
         */
        roundValueToTwoDecimals( value ) {
            return Number( value ).toFixed( 2 );
        }
    }
};
