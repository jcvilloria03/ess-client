export default {
    '/dashboard': 'ess.dashboard',
    '/approvals': 'ess.approvals',
    '/approvals/leave-request/:requestId/details': 'ess.requests.leaves',
    '/requests': 'ess.requests',
    '/requests/leave-request': 'ess.requests.leaves',
    '/requests/undertime-request': 'ess.requests.undertime',
    '/requests/overtime-request': 'ess.requests.overtime',
    '/requests/shift-change-request': 'ess.requests.shift_change',
    '/requests/time-correction-request': 'ess.requests.time_dispute',
    '/announcements': 'ess.announcements',
    '/payslips': 'ess.payslips',
    '/calendar': 'ess.calendar',
    '/teams': 'ess.teams',
    '/profile': 'ess.profile_information',
    '/change-password': 'ess.profile_information'
};
