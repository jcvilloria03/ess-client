const utils = require('./utils');
const config = require('../config');

module.exports = {
    loaders: utils.cssLoaders({
        sourceMap: process.env.NODE_ENV === 'production' ? config.prod.productionSourceMap : config.dev.cssSourceMap,
        extract: process.env.NODE_ENV === 'production'
    }),
    transformToRequire: {
        video: 'src',
        source: 'src',
        img: 'src',
        image: 'xlink:href'
    }
};
