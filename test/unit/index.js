import Vue from 'vue';
import Components from '@/components/mobile/index';
import 'es6-promise/auto';
import axios from 'axios';

Vue.use( Components );

axios.defaults.baseURL = process.env.API_URL;

Vue.config.productionTip = false;

// require all test files (files that ends with .spec.js)
const testsContext = require.context( './specs', true, /\.spec$/ );
testsContext.keys().forEach( testsContext );

// require all src files except main.js for coverage.
// you can also change this to match only the subset of files that
// you want coverage for.
const srcContext = require.context( '../../src', true, /^\.\/(?!main(\.js)?$|components\/mobile\/index(\.js)?$|utils\/Authentication(\.js)?$)/ );
srcContext.keys().forEach( srcContext );
