import axios from 'axios';
import moment from 'moment';
import MockAdapter from 'axios-mock-adapter';
import { DEFAULT_SHIFT_DATETIME_FORMAT } from '@/constants/overtime-request';
import TimeClockService from '@/utils/services/TimeClockService';

let timeClockService = null;
let axiosMock = null;

describe( 'TimeClockService.js', () => {
    beforeEach( () => {
        timeClockService = new TimeClockService();
        axiosMock = new MockAdapter( axios );
    });

    afterEach( () => {
        timeClockService = null;
        axiosMock.restore();
    });

    it( 'can get entire time clock timesheet', ( done ) => {
        const timesheet = getTimesheet(); // eslint-disable-line no-use-before-define

        axiosMock.onGet( 'ess/clock_state/timesheet' ).reply( 200, timesheet );

        timeClockService.getTimeClockTimesheet().then( ( result ) => {
            expect( result ).to.eql( timesheet );

            done();
        });
    });

    /**
     * Get input timesheet for getTimesheetForShift conversion
     *
     * @param {Array} times
     * @param {String} shiftDate
     * @param {Array} states
     * @returns {Array} timesheets
     */
    function getInputTimesheet( times, shiftDate, states ) {
        const timesheet = [];

        times.forEach( ( time ) => {
            timesheet.push({
                timestamp: moment(
                    `${shiftDate} ${time}`, DEFAULT_SHIFT_DATETIME_FORMAT
                ).format( 'X' )
            });
        });

        return timesheet.map( ( timeclock, index ) => {
            timeclock.state = states[ index ];
            return timeclock;
        });
    }

    /**
     *  Get expected timesheet after getTimesheetForShift conversion
     *
     * @param {Array} timesheet
     * @param {Array} expectedDataIndices
     * @returns {Array} expectedTimesheet
     */
    function getExpectedTimesheet( timesheet, expectedDataIndices ) {
        return expectedDataIndices.map( ( index ) => timesheet[ index ]);
    }

    it( 'get timesheet for shift - case 1', ( done ) => {
        const shift = {
            date: '2018-10-10',
            schedule: {
                start_time: '08:00',
                end_time: '16:00'
            }
        };

        const times = [ '04:00', '07:00', '09:15', '15:30', '17:00', '20:00', '22:00' ];
        const states = [ true, true, true, true, true, false, true ];
        const input = getInputTimesheet( times, shift.date, states );
        const expected = getExpectedTimesheet( input, [ 1, 2, 3, 4, 5 ]);
        const actual = timeClockService.getTimesheetForShift( input, shift );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'get timesheet for shift - case 2', ( done ) => {
        const shift = {
            date: '2018-10-10',
            schedule: {
                start_time: '08:00',
                end_time: '16:00'
            }
        };

        const times = [ '04:00', '07:00', '09:15', '15:30', '17:00', '20:00', '22:00' ];
        const states = [ false, true, true, true, false, true, false ];
        const input = getInputTimesheet( times, shift.date, states );
        const expected = getExpectedTimesheet( input, [ 1, 2, 3, 4 ]);
        const actual = timeClockService.getTimesheetForShift( input, shift );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'get timesheet for shift - case 3', ( done ) => {
        const shift = {
            date: '2018-10-10',
            schedule: {
                start_time: '08:00',
                end_time: '16:00'
            }
        };

        const times = [ '04:00', '07:00', '09:15', '15:30', '17:00', '20:00', '22:00' ];
        const states = [ true, true, true, false, true, false, true ];
        const input = getInputTimesheet( times, shift.date, states );
        const expected = getExpectedTimesheet( input, [ 1, 2, 3 ]);
        const actual = timeClockService.getTimesheetForShift( input, shift );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'get timesheet for shift - case 4', ( done ) => {
        const shift = {
            date: '2018-10-10',
            schedule: {
                start_time: '08:00',
                end_time: '16:00'
            }
        };

        const times = [ '04:00', '07:00', '09:15', '15:30', '17:00', '20:00', '22:00' ];
        const states = [ true, true, true, false, false, true, false ];
        const input = getInputTimesheet( times, shift.date, states );
        const expected = getExpectedTimesheet( input, [ 1, 2, 3 ]);
        const actual = timeClockService.getTimesheetForShift( input, shift );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'get timesheet for shift - case 5', ( done ) => {
        const shift = {
            date: '2018-10-10',
            schedule: {
                start_time: '08:00',
                end_time: '16:00'
            }
        };

        const times = [ '04:00', '07:00', '09:15', '15:30', '17:00', '20:00', '22:00' ];
        const states = [ false, true, false, true, true, false, true ];
        const input = getInputTimesheet( times, shift.date, states );
        const expected = getExpectedTimesheet( input, [ 1, 2, 3, 4, 5 ]);
        const actual = timeClockService.getTimesheetForShift( input, shift );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'get timesheet for shift - case 6', ( done ) => {
        const shift = {
            date: '2018-10-10',
            schedule: {
                start_time: '08:00',
                end_time: '16:00'
            }
        };

        const times = [ '04:00', '07:00', '09:15', '15:30', '17:00', '20:00', '22:00' ];
        const states = [ false, true, false, true, false, true, false ];
        const input = getInputTimesheet( times, shift.date, states );
        const expected = getExpectedTimesheet( input, [ 1, 2, 3, 4 ]);
        const actual = timeClockService.getTimesheetForShift( input, shift );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'get timesheet for shift - case 7', ( done ) => {
        const shift = {
            date: '2018-10-10',
            schedule: {
                start_time: '08:00',
                end_time: '16:00'
            }
        };

        const times = [ '04:00', '07:00', '09:15', '15:30', '17:00', '20:00', '22:00' ];
        const states = [ false, true, false, false, true, false, true ];
        const input = getInputTimesheet( times, shift.date, states );
        const expected = getExpectedTimesheet( input, [ 1, 2, 3 ]);
        const actual = timeClockService.getTimesheetForShift( input, shift );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'get timesheet for shift - case 8', ( done ) => {
        const shift = {
            date: '2018-10-10',
            schedule: {
                start_time: '08:00',
                end_time: '16:00'
            }
        };

        const times = [ '04:00', '07:00', '09:15', '15:30', '17:00', '20:00', '22:00' ];
        const states = [ true, true, false, false, false, true, false ];
        const input = getInputTimesheet( times, shift.date, states );
        const expected = getExpectedTimesheet( input, [ 1, 2, 3 ]);
        const actual = timeClockService.getTimesheetForShift( input, shift );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'get timesheet for shift - case 9', ( done ) => {
        const shift = {
            date: '2018-10-10',
            schedule: {
                start_time: '08:00',
                end_time: '16:00'
            }
        };

        const times = [ '04:00', '07:00', '09:15', '15:30', '17:00', '20:00', '22:00' ];
        const states = [ true, false, true, true, true, false, true ];
        const input = getInputTimesheet( times, shift.date, states );
        const expected = getExpectedTimesheet( input, [ 2, 3, 4, 5 ]);
        const actual = timeClockService.getTimesheetForShift( input, shift );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'get timesheet for shift - case 10', ( done ) => {
        const shift = {
            date: '2018-10-10',
            schedule: {
                start_time: '08:00',
                end_time: '16:00'
            }
        };

        const times = [ '04:00', '07:00', '09:15', '15:30', '17:00', '20:00', '22:00' ];
        const states = [ true, false, true, true, false, true, false ];
        const input = getInputTimesheet( times, shift.date, states );
        const expected = getExpectedTimesheet( input, [ 2, 3, 4 ]);
        const actual = timeClockService.getTimesheetForShift( input, shift );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'get timesheet for shift - case 11', ( done ) => {
        const shift = {
            date: '2018-10-10',
            schedule: {
                start_time: '08:00',
                end_time: '16:00'
            }
        };

        const times = [ '04:00', '07:00', '09:15', '15:30', '17:00', '20:00', '22:00' ];
        const states = [ false, false, true, false, true, false, true ];
        const input = getInputTimesheet( times, shift.date, states );
        const expected = getExpectedTimesheet( input, [ 2, 3 ]);
        const actual = timeClockService.getTimesheetForShift( input, shift );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'get timesheet for shift - case 12', ( done ) => {
        const shift = {
            date: '2018-10-10',
            schedule: {
                start_time: '08:00',
                end_time: '16:00'
            }
        };

        const times = [ '04:00', '07:00', '09:15', '15:30', '17:00', '20:00', '22:00' ];
        const states = [ false, false, true, false, false, true, false ];
        const input = getInputTimesheet( times, shift.date, states );
        const expected = getExpectedTimesheet( input, [ 2, 3 ]);
        const actual = timeClockService.getTimesheetForShift( input, shift );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'get timesheet for shift - case 13', ( done ) => {
        const shift = {
            date: '2018-10-10',
            schedule: {
                start_time: '08:00',
                end_time: '16:00'
            }
        };

        const times = [ '04:00', '07:00', '09:15', '15:30', '17:00', '20:00', '22:00' ];
        const states = [ false, false, false, true, true, false, true ];
        const input = getInputTimesheet( times, shift.date, states );
        const expected = getExpectedTimesheet( input, [ 2, 3, 4, 5 ]);
        const actual = timeClockService.getTimesheetForShift( input, shift );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'get timesheet for shift - case 14', ( done ) => {
        const shift = {
            date: '2018-10-10',
            schedule: {
                start_time: '08:00',
                end_time: '16:00'
            }
        };

        const times = [ '04:00', '07:00', '09:15', '15:30', '17:00', '20:00', '22:00' ];
        const states = [ false, false, false, true, false, false, true ];
        const input = getInputTimesheet( times, shift.date, states );
        const expected = getExpectedTimesheet( input, [ 2, 3, 4 ]);
        const actual = timeClockService.getTimesheetForShift( input, shift );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'get timesheet for shift - case 15', ( done ) => {
        const shift = {
            date: '2018-10-10',
            schedule: {
                start_time: '08:00',
                end_time: '16:00'
            }
        };

        const times = [ '04:00', '07:00', '09:15', '15:30', '17:00', '20:00', '22:00' ];
        const states = [ true, false, false, false, true, false, true ];
        const input = getInputTimesheet( times, shift.date, states );
        const expected = getExpectedTimesheet( input, [ 2, 3 ]);
        const actual = timeClockService.getTimesheetForShift( input, shift );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'get timesheet for shift - case 16', ( done ) => {
        const shift = {
            date: '2018-10-10',
            schedule: {
                start_time: '08:00',
                end_time: '16:00'
            }
        };

        const times = [ '04:00', '07:00', '09:15', '15:30', '17:00', '20:00', '22:00' ];
        const states = [ true, false, false, false, false, true, false ];
        const input = getInputTimesheet( times, shift.date, states );
        const expected = getExpectedTimesheet( input, [ 2, 3 ]);
        const actual = timeClockService.getTimesheetForShift( input, shift );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'get timesheet for shift - case 17', ( done ) => {
        const shift = {
            date: '2018-10-10',
            schedule: {
                start_time: '08:00',
                end_time: '16:00'
            }
        };

        const times = [ '07:00', '17:00' ];
        const states = [ true, false ];
        const input = getInputTimesheet( times, shift.date, states );
        const expected = getExpectedTimesheet( input, [ 0, 1 ]);
        const actual = timeClockService.getTimesheetForShift( input, shift );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'format minutes to time', ( done ) => {
        const actual = timeClockService.formatMinutesToTime( 393 );

        expect( actual ).to.eql( '06:33' );

        done();
    });
});

/**
 * Get test timesheet.
 * @return {Array}
 */
function getTimesheet() {
    return [
        {
            employee_uid: 1,
            company_uid: 1,
            tags: [
                'test_tag1',
                'test_tag2'
            ],
            state: true,
            timestamp: 1521563334
        },
        {
            employee_uid: 1,
            company_uid: 1,
            tags: [],
            state: false,
            timestamp: 1521799109
        },
        {
            employee_uid: 1,
            company_uid: 1,
            tags: [
                'testtag',
                'another'
            ],
            state: true,
            timestamp: 1521799169
        }
    ];
}
