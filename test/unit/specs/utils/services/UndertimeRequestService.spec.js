import UndertimeRequestService from '@/utils/services/UndertimeRequestService';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import 'es6-promise/auto';
import moment from 'moment';
const API_URL = process.env.API_URL;

const data = {
    date: '2018-04-04',
    messages: [
        'message_1',
        'message_2',
        'message_3'
    ],
    shifts: [
        {
            shift_id: 0,
            hours: '02:00',
            start_time: '12:00',
            end_time: '14:00'
        }
    ]
};
const undertimeRequest = {
    id: 1,
    date: '2017-01-01',
    params: {
        shifts: {
            hours: '03:00:00',
            shift_id: 1,
            start_time: '12:00:00',
            end_time: '15:00:00'
        },
        activities: {},
        workflow_id: 1,
        employee_id: 1,
        employee_request_id: 1,
        status: 'Pending'
    },
    messages: [
        {
            id: 1,
            request_id: 1,
            sender_id: 1,
            content: 'bla bla bla',
            created_at: '2017-05-05 00:00:00',
            updated_at: '2017-05-05 00:00:00'
        }
    ]
};
/**
 * Gets undertime timerange
 * @param {String} startTime
 * @param {String} end
 * @param {String} numberOfHours
 *
 * @returns {Object}
 */
function getUndertimeTimeRange( start = '07:00', end = '18:00', numberOfHours = '11:00' ) {
    return { start, end, numberOfHours };
}

/**
 * Gets shift datetime range
 * @param {String} startTime
 * @param {String} end
 * @param {String} numberOfHours
 *
 * @returns {Object}
 */
function getShiftTimeRange( start = '2018-08-24 07:00', end = '2018-08-24 18:00' ) {
    return { start: moment( start ), end: moment( end ) };
}

let undertimeRequestService;
let mock;

describe( 'UndertimeRequestService.js', () => {
    beforeEach( () => {
        undertimeRequestService = new UndertimeRequestService();
        mock = new MockAdapter( axios );
    });

    afterEach( () => {
        undertimeRequestService = null;
        mock.restore();
    });

    it( 'can fetch request with messages', ( done ) => {
        mock.onGet( 'ess/undertime_request/1' ).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );

            return [ 200, [
                undertimeRequest
            ]];
        });

        undertimeRequestService.fetchRequest( 1 ).then( ( undertime ) => {
            expect( undertime ).to.be.an( 'array' ).to.have.lengthOf( 1 );
            expect( undertime[ 0 ]).to.eql( undertimeRequest );
            done();
        });
    });

    it( 'can submit undertime request with messages', ( done ) => {
        mock.onPost( 'ess/undertime_request' ).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );

            return [ 200, data ];
        });

        undertimeRequestService.submitRequest( data ).then( ( undertime ) => {
            expect( undertime ).to.be.an( 'object' );
            expect( undertime ).to.eql( data );

            done();
        });
    });

    it( 'can validate undertime request - valid case', () => {
        const response = undertimeRequestService.isUndertimeRequestValid(
            getShiftTimeRange(),
            getUndertimeTimeRange(),
            '2018-08-24'
        );
        expect( response ).to.be.true;
    });

    it( 'can validate undertime request - invalid undertime start time before shift start', () => {
        const response = undertimeRequestService.isUndertimeRequestValid(
            getShiftTimeRange(),
            getUndertimeTimeRange( '06:00' ),
            '2018-08-24'
        );
        expect( response ).to.be.false;
    });

    it( 'can validate undertime request - invalid undertime start time after shift end', () => {
        const response = undertimeRequestService.isUndertimeRequestValid(
            getShiftTimeRange(),
            getUndertimeTimeRange( '07:00', '18:01' ),
            '2018-08-24'
        );
        expect( response ).to.be.false;
    });

    it( 'can validate undertime request - invalid undertime numberOfHours', () => {
        const response = undertimeRequestService.isUndertimeRequestValid(
            getShiftTimeRange(),
            getUndertimeTimeRange( '07:00', '18:00', '12:00' ),
            '2018-08-24'
        );
        expect( response ).to.be.false;
    });

    it( 'can validate undertime request - invalid undertime start and end time before shift start', () => {
        const response = undertimeRequestService.isUndertimeRequestValid(
            getShiftTimeRange(),
            getUndertimeTimeRange( '06:59', '18:01' ),
            '2018-08-24'
        );
        expect( response ).to.be.false;
    });

    it( 'can validate undertime request - invalid undertime start and end time after shift end', () => {
        const response = undertimeRequestService.isUndertimeRequestValid(
            getShiftTimeRange(),
            getUndertimeTimeRange( '18:01', '18:01' ),
            '2018-08-24'
        );
        expect( response ).to.be.false;
    });

    it( 'can validate undertime request night shift - valid case 1', () => {
        const response = undertimeRequestService.isUndertimeRequestValid(
            getShiftTimeRange( '2018-08-24 22:00', '2018-08-25 07:00' ),
            getUndertimeTimeRange( '22:00', '07:00', '09:00' ),
            '2018-08-24'
        );
        expect( response ).to.be.true;
    });

    it( 'can validate undertime request night shift - valid case 2', () => {
        const response = undertimeRequestService.isUndertimeRequestValid(
            getShiftTimeRange( '2018-08-24 22:00', '2018-08-25 07:00' ),
            getUndertimeTimeRange( '24:00', '07:00', '07:00' ),
            '2018-08-24'
        );
        expect( response ).to.be.true;
    });

    it( 'can validate undertime request night shift - valid case 3', () => {
        const response = undertimeRequestService.isUndertimeRequestValid(
            getShiftTimeRange( '2018-08-24 22:00', '2018-08-25 07:00' ),
            getUndertimeTimeRange( '22:00', '23:00', '01:00' ),
            '2018-08-24'
        );
        expect( response ).to.be.true;
    });

    it( 'can validate undertime request night shift - valid case 4', () => {
        const response = undertimeRequestService.isUndertimeRequestValid(
            getShiftTimeRange( '2018-08-24 22:00', '2018-08-25 07:00' ),
            getUndertimeTimeRange( '22:00', '24:00', '02:00' ),
            '2018-08-24'
        );
        expect( response ).to.be.true;
    });

    it( 'can validate undertime request night shift - invalid case undertime is out of shift timerange 1', () => {
        const response = undertimeRequestService.isUndertimeRequestValid(
            getShiftTimeRange( '2018-08-24 22:00', '2018-08-25 07:00' ),
            getUndertimeTimeRange( '21:00', '08:00', '11:00' ),
            '2018-08-24'
        );
        expect( response ).to.be.false;
    });

    it( 'can validate undertime request night shift - invalid case undertime is out of shift timerange 2', () => {
        const response = undertimeRequestService.isUndertimeRequestValid(
            getShiftTimeRange( '2018-08-24 22:00', '2018-08-25 07:00' ),
            getUndertimeTimeRange( '09:00', '07:00', '22:00' ),
            '2018-08-24'
        );
        expect( response ).to.be.false;
    });

    it( 'can validate undertime request night shift - invalid case undertime numberOfHours', () => {
        const response = undertimeRequestService.isUndertimeRequestValid(
            getShiftTimeRange( '2018-08-24 22:00', '2018-08-25 07:00' ),
            getUndertimeTimeRange( '22:00', '07:00', '22:00' ),
            '2018-08-24'
        );
        expect( response ).to.be.false;
    });
});
