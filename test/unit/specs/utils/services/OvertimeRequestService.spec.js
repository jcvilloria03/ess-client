import OvertimeRequestService from '@/utils/services/OvertimeRequestService';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import 'es6-promise/auto';

const API_URL = process.env.API_URL;

const data = {
    date: '2018-04-04',
    messages: [
        'message_1',
        'message_2',
        'message_3'
    ],
    shifts: [
        {
            shift_id: 0,
            hours: '12:00',
            start_time: '12:00'
        }
    ]
};
const overtimeRequest = {
    id: 1,
    date: '2017-01-01',
    params: {
        shifts: {
            hours: '03:00:00',
            shift_id: 1,
            start_time: '17:00:00'
        },
        activities: {},
        workflow_id: 1,
        employee_id: 1,
        employee_request_id: 1,
        status: 'Pending'
    },
    messages: [
        {
            id: 1,
            request_id: 1,
            sender_id: 1,
            content: 'bla bla bla',
            created_at: '2017-05-05 00:00:00',
            updated_at: '2017-05-05 00:00:00'
        }
    ]
};

let overtimeRequestService;
let mock;

describe( 'OvertimeRequestService.js', () => {
    beforeEach( () => {
        overtimeRequestService = new OvertimeRequestService();
        mock = new MockAdapter( axios );
    });

    afterEach( () => {
        overtimeRequestService = null;
        mock.restore();
    });

    it( 'can fetch request with messages', ( done ) => {
        mock.onGet( 'ess/overtime_request/1' ).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );

            return [ 200, [
                overtimeRequest
            ]];
        });

        overtimeRequestService.fetchOvertimeRequest( 1 ).then( ( overtime ) => {
            expect( overtime ).to.be.an( 'array' ).to.have.lengthOf( 1 );
            expect( overtime[ 0 ]).to.eql( overtimeRequest );
            done();
        });
    });

    it( 'can submit overtime request with messages', ( done ) => {
        mock.onPost( 'ess/overtime_request' ).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );

            return [ 200, data ];
        });

        overtimeRequestService.submitOvertimeRequest( data ).then( ( overtime ) => {
            expect( overtime ).to.be.an( 'object' );
            expect( overtime ).to.eql( data );

            done();
        });
    });
});
