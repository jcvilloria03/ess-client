import EmployeeRequestService from '@/utils/services/EmployeeRequestService';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import 'es6-promise/auto';

const API_URL = process.env.API_URL;
let employeeRequestService;
let mock;

describe( 'EmployeeRequestService.js', () => {
    beforeEach( () => {
        employeeRequestService = new EmployeeRequestService();
        mock = new MockAdapter( axios );
    });

    afterEach( () => {
        employeeRequestService = null;
        mock.restore();
    });

    it( 'can decline all requests', ( done ) => {
        mock.onPost( 'ess/request/bulk_decline' ).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );

            return [ 200, [
                {
                    id: 1,
                    company_id: 1,
                    employee_id: 651,
                    workflow_id: 1,
                    position: 1,
                    request_id: 1,
                    request_type: 'leave_request',
                    status: 2,
                    params: {
                        owner: {
                            name: 'John Doe',
                            user_id: 1,
                            employee_id: 651
                        }
                    },
                    created_at: '2017-04-04 15:24:00',
                    updated_at: '2018-02-12 14:06:30'
                }
            ]];
        });

        employeeRequestService.bulkDeclineRequests([1]).then( ( approvals ) => {
            expect( approvals ).to.be.an( 'array' ).to.have.lengthOf( 1 );
            expect( approvals[ 0 ]).to.eql({
                id: 1,
                company_id: 1,
                employee_id: 651,
                workflow_id: 1,
                position: 1,
                request_id: 1,
                request_type: 'leave_request',
                status: 2,
                params: {
                    owner: {
                        name: 'John Doe',
                        user_id: 1,
                        employee_id: 651
                    }
                },
                created_at: '2017-04-04 15:24:00',
                updated_at: '2018-02-12 14:06:30'
            });

            done();
        });
    });

    it( 'can delete attachment', ( done ) => {
        mock.onDelete( 'ess/employee_request/1/attachments/1' ).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );

            return [ 204, null ];
        });

        employeeRequestService.deleteAttachment( 1, 1 ).then( ( response ) => {
            expect( response ).to.be.null;

            done();
        });
    });
    it( 'can fetch employee rest days', ( done ) => {
        mock.onGet( 'ess/employee/rest_days' ).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );

            return [ 200, {
                data: [
                    {
                        id: 1,
                        company_id: 1,
                        employee_id: 703,
                        start_date: '2018-05-31',
                        end_date: '2018-05-31',
                        employee: null
                    }
                ]
            }];
        });

        employeeRequestService.getEmployeeRestDays()
            .then( ( response ) => {
                expect( response ).to.be.an( 'array' ).to.have.lengthOf( 1 );
                expect( response[ 0 ]).to.eql({
                    id: 1,
                    company_id: 1,
                    employee_id: 703,
                    start_date: '2018-05-31',
                    end_date: '2018-05-31',
                    employee: null
                });

                done();
            });
    });
});
