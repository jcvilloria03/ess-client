import ShiftsService from '@/utils/services/ShiftsService';
import axios from 'axios';
import moment from 'moment';
import MockAdapter from 'axios-mock-adapter';
import 'es6-promise/auto';
import { DEFAULT_DATE_FORMAT } from '@/constants/employee-request';

const API_URL = process.env.API_URL;
let shiftsService;
let mock;
const employeeSchedule = {
    company_id: 2,
    end_time: '20:00',
    id: 413,
    name: 'test6',
    on_rest_day: false,
    repeat: {
        id: 6,
        schedule_id: 413,
        type: 'every_weekday',
        repeat_every: 1,
        updated_at: '2017-12-27 10:22:42'
    },
    start_date: '2017-12-27',
    start_time: '12:05',
    total_hours: '07:55'
};
const employeeShift = {
    company_id: 1,
    dates: [ '2018-05-08', '2018-05-09', '2018-05-28', '2018-06-01' ],
    employee: null,
    employee_id: 703,
    end: '2018-06-01',
    id: 27,
    schedule: employeeSchedule,
    schedule_id: 420,
    start: '2018-05-08'
};

const parsedEmployeeShifts = {
    '2018-05-08': [{
        schedule: employeeSchedule,
        shift_id: employeeShift.id,
        date: '2018-05-08',
        start: employeeSchedule.start_time,
        end: employeeSchedule.end_time,
        selected: true,
        expanded: true
    }]
};

const defaultSchedule = [
    {
        company_id: 1,
        day_of_week: 1,
        day_type: 'regular',
        id: 1,
        work_break_end: '13:00:00',
        work_break_start: '12:00:00',
        work_end: '17:00:00',
        work_start: '08:00:00'
    },
    {
        company_id: 1,
        day_of_week: 2,
        day_type: 'regular',
        id: 2,
        work_break_end: '13:00:00',
        work_break_start: '12:00:00',
        work_end: '17:00:00',
        work_start: '08:00:00'
    },
    {
        company_id: 1,
        day_of_week: 3,
        day_type: 'regular',
        id: 3,
        work_break_end: '13:00:00',
        work_break_start: '12:00:00',
        work_end: '17:00:00',
        work_start: '08:00:00'
    },
    {
        company_id: 1,
        day_of_week: 4,
        day_type: 'regular',
        id: 4,
        work_break_end: '13:00:00',
        work_break_start: '12:00:00',
        work_end: '17:00:00',
        work_start: '08:00:00'
    },
    {
        company_id: 1,
        day_of_week: 5,
        day_type: 'regular',
        id: 5,
        work_break_end: '13:00:00',
        work_break_start: '12:00:00',
        work_end: '17:00:00',
        work_start: '08:00:00'
    },
    {
        company_id: 1,
        day_of_week: 6,
        day_type: 'regular',
        id: 6,
        work_break_end: '13:00:00',
        work_break_start: '12:00:00',
        work_end: '17:00:00',
        work_start: '08:00:00'
    },
    {
        company_id: 1,
        day_of_week: 7,
        day_type: 'regular',
        id: 7,
        work_break_end: '13:00:00',
        work_break_start: '12:00:00',
        work_end: '17:00:00',
        work_start: '08:00:00'
    }
];

const parsedShift = parsedEmployeeShifts[ '2018-05-08' ][ 0 ];

let shiftRange = {};

/**
 * Gets mocked restDays.
 * @returns {Object}
 */
function getRestDayWithRepeat() {
    return {
        id: 1,
        company_id: 1,
        employee_id: 1,
        start_date: '2017-10-26',
        end_date: '2017-10-30',
        allowed_time_methods: [
            'Bundy App'
        ],
        repeat: {
            id: 1,
            rest_day_id: 1,
            repeat_every: 1,
            rest_days: [ 'monday', 'thursday' ],
            end_never: false,
            end_after: 5
        }
    };
}

/**
 * Gets date inteval.
 * @returns {Object}
 */
function getInterval() {
    return {
        start: moment( '2017-10-01' ),
        end: moment( '2017-12-31' )
    };
}
/**
 * Gets invalid date interval.
 * @return {Object}
 */
function getInvalidInterval() {
    return {
        start: moment( '2017-10-01' ),
        end: moment( '2017-09-12' )
    };
}
/**
 * Gets rest day without repeat.
 * @returns {Object}
 */
function getRestDayWithoutRepeat() {
    return {
        id: 1,
        company_id: 1,
        employee_id: 1,
        start_date: '2017-10-26',
        end_date: '2017-10-30',
        allowed_time_methods: [
            'Bundy App'
        ],
        repeat: null
    };
}

/**
 * Gets employee requests.
 */
function getEmployeeRequests() {
    return [
        {
            id: 1,
            request_type: 'leave_request',
            status: 'Pending',
            request: {
                date: '2018-08-01',
                shifts: [
                    { id: 1 }
                ]
            }
        },
        {
            id: 2,
            request_type: 'overtime_request',
            status: 'Pending',
            request: {
                date: '2018-08-04',
                shifts: [
                    { id: 2 }
                ]
            }
        }
    ];
}
describe( 'ShiftsService.js', () => {
    beforeEach( () => {
        shiftsService = new ShiftsService();
        mock = new MockAdapter( axios );
    });

    afterEach( () => {
        shiftsService = null;
        mock.restore();
    });

    it( 'can fetch employee schedules', ( done ) => {
        mock.onGet( 'ess/employee/schedules' ).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );

            return [ 200, [
                employeeSchedule
            ]];
        });

        shiftsService.getEmployeeSchedules()
            .then( ( response ) => {
                expect( response ).to.be.an( 'array' ).to.have.lengthOf( 1 );
                expect( response[ 0 ]).to.be.an( 'object' ).to.eql( employeeSchedule );

                done();
            }).catch( () => done() );
    });

    it( 'can fetch default company schedules', ( done ) => {
        mock.onGet( 'ess/company/1/default_schedule' ).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );

            return [ 200, defaultSchedule ];
        });

        shiftsService.fetchDefaultScheduleForCompany()
            .then( ( response ) => {
                expect( response ).to.be.an( 'array' ).to.have.lengthOf( 7 );
                expect( response[ 0 ]).to.be.an( 'object' ).to.eql( defaultSchedule[ 0 ]);

                done();
            }).catch( () => done() );
    });

    it( 'can fetch employee shifts', ( done ) => {
        mock.onGet( 'ess/employee/shifts' ).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );

            return [ 200, [
                employeeShift
            ]];
        });

        shiftsService.getEmployeeShifts()
            .then( ( response ) => {
                expect( response ).to.be.an( 'array' ).to.have.lengthOf( 1 );
                expect( response[ 0 ]).to.be.an( 'object' ).to.eql( employeeShift );

                done();
            }).catch( () => done() );
    });

    it( 'parses employee shifts by date', () => {
        const parsedShifts = shiftsService.parseShifts([employeeShift], true, true );
        expect( parsedShifts[ '2018-05-08' ]).to.be.an( 'array' ).to.have.lengthOf( 1 );
        expect( parsedShifts[ '2018-05-08' ][ 0 ]).to.be.an( 'object' ).to.eql(
            parsedEmployeeShifts[ '2018-05-08' ][ 0 ]
        );
    });

    it( 'parses invalid employee shifts by date', () => {
        const parsedShifts = shiftsService.parseShifts([]);
        expect( parsedShifts[ '2018-05-08' ]).to.be.undefined;
    });

    it( 'gets shift range', () => {
        shiftRange = shiftsService.getShiftRange( parsedShift );
        expect( shiftRange ).to.be.an( 'object' );

        const shiftRangeStart = moment( shiftRange.start ).format( DEFAULT_DATE_FORMAT );
        expect( shiftRangeStart ).to.be.equal( parsedShift.date );
    });

    it( 'can process repeating restday that ends on given date', () => {
        const events = shiftsService.processRestDays(
          [getRestDayWithRepeat()],
          getInterval()
        );

        expect( events ).to.be.an( 'array' ).to.have.lengthOf( 2 );
        expect( events[ 0 ].original ).not.to.be.empty;

        expect( events[ 0 ].start ).to.equal( '2017-10-26' );
        expect( events[ 0 ].end ).to.equal( '2017-10-26 23:59:59' );
        expect( events[ 1 ].start ).to.equal( '2017-10-30' );
        expect( events[ 1 ].end ).to.equal( '2017-10-30 23:59:59' );
    });

    it( 'process repeating restday with invalid interval', () => {
        const events = shiftsService.processRestDays(
          [getRestDayWithRepeat()],
          getInvalidInterval()
        );

        expect( events ).to.be.an( 'array' ).to.have.lengthOf( 0 );
    });

    it( 'process restday without repeat with invalid interval', () => {
        const events = shiftsService.processRestDays(
          [getRestDayWithoutRepeat()],
          getInvalidInterval()
        );

        expect( events ).to.be.an( 'array' ).to.have.lengthOf( 0 );
    });

    it( 'process restday without repeat with valid interval', () => {
        const events = shiftsService.processRestDays(
          [getRestDayWithoutRepeat()],
          getInterval()
        );

        expect( events ).to.be.an( 'array' ).to.have.lengthOf( 1 );
    });

    it( 'process restday without repeat with valid interval but not inside it', () => {
        const events = shiftsService.processRestDays(
          [getRestDayWithoutRepeat()],
            {
                start: '2012-10-12',
                end: '2012-12-12'
            }
        );

        expect( events ).to.be.an( 'array' ).to.have.lengthOf( 0 );
    });

    it( 'check if shift is nightshift', () => {
        expect( shiftsService.isNightShift( '08:00', '16:00' ) )
            .to.eql( false );

        expect( shiftsService.isNightShift( '22:00', '05:00' ) )
            .to.eql( true );
    });

    it( 'can check are shifts related to some employee requests', ( done ) => {
        mock.onPost( '/shift/overlapping_requests' ).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );

            return [ 200, {
                data: [
                    getEmployeeRequests()
                ]
            }];
        });
        const payload = {
            start: '2018-08-01',
            end: '2018-08-01',
            shift_ids: [ 1, 2 ]
        };

        shiftsService.shiftHasRelatedEmployeeRequests( payload )
            .then( ( response ) => {
                expect( response ).to.be.an( 'array' ).to.have.lengthOf( 2 );
                expect( response[ 0 ]).to.be.an( 'object' ).to.eql( getEmployeeRequests()[ 0 ]);
                expect( response[ 1 ]).to.be.an( 'object' ).to.eql( getEmployeeRequests()[ 1 ]);

                done();
            }).catch( () => done() );
    });

    it( 'Deduct unpaid breaks from worked minutes if clock pairs overlaps with break', () => {
        const duration = 540;
        const clockPairs = [
            {
                clockIn: { datetime: '2022-06-03 08:00' },
                clockOut: { datetime: '2022-06-03 17:00' }
            }
        ];
        const shift = {
            date: '2022-06-03',
            start: '08:00',
            end: '17:00',
            schedule: defaultSchedule[ 0 ]
        };
        const durationAfterBreaksDeduction = shiftsService.deductBreaksFromClockpairsDuration( shift, clockPairs, duration, 'asMinutes' );
        expect( durationAfterBreaksDeduction ).to.equal( 480 );
    });

    it( 'dont deduct unpaid breaks from worked minutes if clock pairs do not overlaps with break', () => {
        const duration = 480;
        const clockPairs = [
            {
                clockIn: { datetime: '2022-06-03 08:00' },
                clockOut: { datetime: '2022-06-03 12:00' }
            },
            {
                clockIn: { datetime: '2022-06-03 13:00' },
                clockOut: { datetime: '2022-06-03 17:00' }
            }
        ];
        const shift = {
            date: '2022-06-03',
            start: '08:00',
            end: '17:00',
            schedule: defaultSchedule[ 0 ]
        };
        const durationAfterBreaksDeduction = shiftsService.deductBreaksFromClockpairsDuration( shift, clockPairs, duration, 'asMinutes' );
        expect( durationAfterBreaksDeduction ).to.equal( 480 );
    });

    it( 'Deduct unpaid custom fixed breaks from worked minutes if clock pairs overlaps with break', () => {
        const duration = 540;
        const clockPairs = [
            {
                clockIn: { datetime: '2022-06-03 08:00' },
                clockOut: { datetime: '2022-06-03 17:00' }
            }
        ];
        const breaks = [
            {
                start: '12:00',
                end: '13:00',
                type: 'fixed'
            }
        ];
        const shift = {
            date: '2022-06-03',
            start: '08:00',
            end: '17:00',
            schedule: {
                ...defaultSchedule[ 0 ],
                breaks
            }
        };
        const durationAfterBreaksDeduction = shiftsService.deductBreaksFromClockpairsDuration( shift, clockPairs, duration, 'asMinutes' );
        expect( durationAfterBreaksDeduction ).to.equal( 480 );
    });

    it( 'dont deduct unpaid custom fixed breaks from worked minutes if clock pairs do not overlaps with break', () => {
        const duration = 480;
        const clockPairs = [
            {
                clockIn: { datetime: '2022-06-03 08:00' },
                clockOut: { datetime: '2022-06-03 12:00' }
            },
            {
                clockIn: { datetime: '2022-06-03 13:00' },
                clockOut: { datetime: '2022-06-03 17:00' }
            }
        ];
        const breaks = [
            {
                start: '12:00',
                end: '13:00',
                type: 'fixed'
            }
        ];
        const shift = {
            date: '2022-06-03',
            start: '08:00',
            end: '17:00',
            schedule: {
                ...defaultSchedule[ 0 ],
                breaks
            }
        };
        const durationAfterBreaksDeduction = shiftsService.deductBreaksFromClockpairsDuration( shift, clockPairs, duration, 'asMinutes' );
        expect( durationAfterBreaksDeduction ).to.equal( 480 );
    });

    it( 'Unpaid flexi breaks scenario 1', () => {
        // if employee did not take break within the time range then deduct
        // break from total duration
        const duration = 540;
        const clockPairs = [
            {
                clockIn: { datetime: '2022-06-03 15:00' },
                clockOut: { datetime: '2022-06-04 00:00' }
            }
        ];
        const breaks = [
            {
                start: '16:00',
                end: '23:00',
                break_hours: '01:00',
                type: 'flexi'
            }
        ];
        const shift = {
            date: '2022-06-03',
            start: '15:00',
            end: '00:00',
            schedule: {
                ...defaultSchedule[ 0 ],
                breaks
            }
        };
        const durationAfterBreaksDeduction = shiftsService.deductBreaksFromClockpairsDuration( shift, clockPairs, duration, 'asMinutes' );
        expect( durationAfterBreaksDeduction ).to.equal( 480 );
    });

    it( 'Unpaid flexi breaks scenario 2', () => {
        // if employee take break for 30 minutes then deduct the
        // remaining 30 minutes from the total duration
        const duration = 510;
        const clockPairs = [
            {
                clockIn: { datetime: '2022-06-03 15:00' },
                clockOut: { datetime: '2022-06-03 19:00' }
            },
            {
                clockIn: { datetime: '2022-06-03 19:30' },
                clockOut: { datetime: '2022-06-04 00:00' }
            }
        ];
        const breaks = [
            {
                start: '16:00',
                end: '23:00',
                break_hours: '01:00',
                type: 'flexi'
            }
        ];
        const shift = {
            date: '2022-06-03',
            start: '15:00',
            end: '00:00',
            schedule: {
                ...defaultSchedule[ 0 ],
                breaks
            }
        };
        const durationAfterBreaksDeduction = shiftsService.deductBreaksFromClockpairsDuration( shift, clockPairs, duration, 'asMinutes' );
        expect( durationAfterBreaksDeduction ).to.equal( 480 );
    });

    it( 'Unpaid flexi breaks scenario 3', () => {
        // if employee has consumed the full break then
        // don't deduct it from total duration
        const duration = 480;
        const clockPairs = [
            {
                clockIn: { datetime: '2022-06-03 15:00' },
                clockOut: { datetime: '2022-06-03 19:00' }
            },
            {
                clockIn: { datetime: '2022-06-03 20:00' },
                clockOut: { datetime: '2022-06-04 00:00' }
            }
        ];
        const breaks = [
            {
                start: '16:00',
                end: '23:00',
                break_hours: '01:00',
                type: 'flexi'
            }
        ];
        const shift = {
            date: '2022-06-03',
            start: '15:00',
            end: '00:00',
            schedule: {
                ...defaultSchedule[ 0 ],
                breaks
            }
        };
        const durationAfterBreaksDeduction = shiftsService.deductBreaksFromClockpairsDuration( shift, clockPairs, duration, 'asMinutes' );
        expect( durationAfterBreaksDeduction ).to.equal( 480 );
    });

    it( 'Unpaid floating breaks scenario 1', () => {
        // deduct unpaid floating break from total duration
        const duration = 540;
        const clockPairs = [
            {
                clockIn: { datetime: '2022-06-03 09:00' },
                clockOut: { datetime: '2022-06-03 18:00' }
            }
        ];
        const breaks = [
            {
                start: null,
                end: null,
                break_hours: '01:00',
                type: 'floating'
            }
        ];
        const shift = {
            date: '2022-06-03',
            start: '15:00',
            end: '00:00',
            schedule: {
                ...defaultSchedule[ 0 ],
                breaks
            }
        };
        const durationAfterBreaksDeduction = shiftsService.deductBreaksFromClockpairsDuration( shift, clockPairs, duration, 'asMinutes' );
        expect( durationAfterBreaksDeduction ).to.equal( 480 );
    });

    it( 'Unpaid floating breaks scenario 2', () => {
        // deduct unpaid floating break from total duration
        const duration = 180;
        const clockPairs = [
            {
                clockIn: { datetime: '2022-06-03 09:00' },
                clockOut: { datetime: '2022-06-03 12:00' }
            }
        ];
        const breaks = [
            {
                start: null,
                end: null,
                break_hours: '01:00',
                type: 'floating'
            }
        ];
        const shift = {
            date: '2022-06-03',
            start: '15:00',
            end: '00:00',
            schedule: {
                ...defaultSchedule[ 0 ],
                breaks
            }
        };
        const durationAfterBreaksDeduction = shiftsService.deductBreaksFromClockpairsDuration( shift, clockPairs, duration, 'asMinutes' );
        expect( durationAfterBreaksDeduction ).to.equal( 120 );
    });

    it( 'Unpaid floating breaks scenario 3', () => {
        const duration = 480;
        const clockPairs = [
            {
                clockIn: { datetime: '2022-06-03 09:00' },
                clockOut: { datetime: '2022-06-03 12:00' }
            },
            {
                clockIn: { datetime: '2022-06-03 13:00' },
                clockOut: { datetime: '2022-06-03 18:00' }
            }
        ];
        const breaks = [
            {
                start: null,
                end: null,
                break_hours: '01:00',
                type: 'floating'
            }
        ];
        const shift = {
            date: '2022-06-03',
            start: '15:00',
            end: '00:00',
            schedule: {
                ...defaultSchedule[ 0 ],
                breaks
            }
        };
        const durationAfterBreaksDeduction = shiftsService.deductBreaksFromClockpairsDuration( shift, clockPairs, duration, 'asMinutes' );
        expect( durationAfterBreaksDeduction ).to.equal( 480 );
    });

    it( 'Unpaid floating breaks scenario 4', () => {
        const duration = 510;
        const clockPairs = [
            {
                clockIn: { datetime: '2022-06-03 09:00' },
                clockOut: { datetime: '2022-06-03 12:30' }
            },
            {
                clockIn: { datetime: '2022-06-03 13:00' },
                clockOut: { datetime: '2022-06-03 18:00' }
            }
        ];
        const breaks = [
            {
                start: null,
                end: null,
                break_hours: '01:00',
                type: 'floating'
            }
        ];
        const shift = {
            date: '2022-06-03',
            start: '15:00',
            end: '00:00',
            schedule: {
                ...defaultSchedule[ 0 ],
                breaks
            }
        };
        const durationAfterBreaksDeduction = shiftsService.deductBreaksFromClockpairsDuration( shift, clockPairs, duration, 'asMinutes' );
        expect( durationAfterBreaksDeduction ).to.equal( 480 );
    });

    it( 'Night shift unpaid fixed break scenario 1', () => {
        // deduct unpaid fixed break from total duration
        const duration = 540;
        const clockPairs = [
            {
                clockIn: { datetime: '2022-06-03 21:00' },
                clockOut: { datetime: '2022-06-04 06:00' }
            }
        ];
        const breaks = [
            {
                start: '01:00',
                end: '02:00',
                break_hours: '01:00',
                type: 'fixed'
            }
        ];
        const shift = {
            date: '2022-06-03',
            start: '21:00',
            end: '06:00',
            schedule: {
                ...defaultSchedule[ 0 ],
                breaks
            }
        };
        const durationAfterBreaksDeduction = shiftsService.deductBreaksFromClockpairsDuration( shift, clockPairs, duration, 'asMinutes' );
        expect( durationAfterBreaksDeduction ).to.equal( 480 );
    });

    it( 'Night shift unpaid fixed break scenario 2', () => {
        // deduct unpaid fixed break from total duration
        const duration = 480;
        const clockPairs = [
            {
                clockIn: { datetime: '2022-06-03 21:00' },
                clockOut: { datetime: '2022-06-04 01:00' }
            },
            {
                clockIn: { datetime: '2022-06-04 02:00' },
                clockOut: { datetime: '2022-06-04 06:00' }
            }
        ];
        const breaks = [
            {
                start: '01:00',
                end: '02:00',
                break_hours: '01:00',
                type: 'fixed'
            }
        ];
        const shift = {
            date: '2022-06-03',
            start: '21:00',
            end: '06:00',
            schedule: {
                ...defaultSchedule[ 0 ],
                breaks
            }
        };
        const durationAfterBreaksDeduction = shiftsService.deductBreaksFromClockpairsDuration( shift, clockPairs, duration, 'asMinutes' );
        expect( durationAfterBreaksDeduction ).to.equal( 480 );
    });

    it( 'Night shift unpaid fixed break scenario 3', () => {
        // deduct unpaid fixed break from total duration
        const duration = 510;
        const clockPairs = [
            {
                clockIn: { datetime: '2022-06-03 21:00' },
                clockOut: { datetime: '2022-06-04 01:30' }
            },
            {
                clockIn: { datetime: '2022-06-04 02:00' },
                clockOut: { datetime: '2022-06-04 06:00' }
            }
        ];
        const breaks = [
            {
                start: '01:00',
                end: '02:00',
                break_hours: '01:00',
                type: 'fixed'
            }
        ];
        const shift = {
            date: '2022-06-03',
            start: '21:00',
            end: '06:00',
            schedule: {
                ...defaultSchedule[ 0 ],
                breaks
            }
        };
        const durationAfterBreaksDeduction = shiftsService.deductBreaksFromClockpairsDuration( shift, clockPairs, duration, 'asMinutes' );
        expect( durationAfterBreaksDeduction ).to.equal( 480 );
    });

    it( 'Night shift unpaid flexi break scenario 1', () => {
        // deduct unpaid flexi break from total duration
        const duration = 540;
        const clockPairs = [
            {
                clockIn: { datetime: '2022-06-03 21:00' },
                clockOut: { datetime: '2022-06-04 06:00' }
            }
        ];
        const breaks = [
            {
                start: '23:00',
                end: '03:00',
                break_hours: '01:00',
                type: 'flexi'
            }
        ];
        const shift = {
            date: '2022-06-03',
            start: '21:00',
            end: '06:00',
            schedule: {
                ...defaultSchedule[ 0 ],
                breaks
            }
        };
        const durationAfterBreaksDeduction = shiftsService.deductBreaksFromClockpairsDuration( shift, clockPairs, duration, 'asMinutes' );
        expect( durationAfterBreaksDeduction ).to.equal( 480 );
    });

    it( 'Night shift unpaid flexi break scenario 2', () => {
        // deduct unpaid flexi break from total duration
        const duration = 480;
        const clockPairs = [
            {
                clockIn: { datetime: '2022-06-03 21:00' },
                clockOut: { datetime: '2022-06-04 00:00' }
            },
            {
                clockIn: { datetime: '2022-06-04 01:00' },
                clockOut: { datetime: '2022-06-04 06:00' }
            }
        ];
        const breaks = [
            {
                start: '23:00',
                end: '03:00',
                break_hours: '01:00',
                type: 'flexi'
            }
        ];
        const shift = {
            date: '2022-06-03',
            start: '21:00',
            end: '06:00',
            schedule: {
                ...defaultSchedule[ 0 ],
                breaks
            }
        };
        const durationAfterBreaksDeduction = shiftsService.deductBreaksFromClockpairsDuration( shift, clockPairs, duration, 'asMinutes' );
        expect( durationAfterBreaksDeduction ).to.equal( 480 );
    });

    it( 'Night shift unpaid flexi break scenario 3', () => {
        // deduct unpaid flexi break from total duration
        const duration = 510;
        const clockPairs = [
            {
                clockIn: { datetime: '2022-06-03 21:00' },
                clockOut: { datetime: '2022-06-04 00:30' }
            },
            {
                clockIn: { datetime: '2022-06-04 01:00' },
                clockOut: { datetime: '2022-06-04 06:00' }
            }
        ];
        const breaks = [
            {
                start: '23:00',
                end: '03:00',
                break_hours: '01:00',
                type: 'flexi'
            }
        ];
        const shift = {
            date: '2022-06-03',
            start: '21:00',
            end: '06:00',
            schedule: {
                ...defaultSchedule[ 0 ],
                breaks
            }
        };
        const durationAfterBreaksDeduction = shiftsService.deductBreaksFromClockpairsDuration( shift, clockPairs, duration, 'asMinutes' );
        expect( durationAfterBreaksDeduction ).to.equal( 480 );
    });

    it( 'Night shift unpaid flexi break scenario 4', () => {
        // deduct unpaid flexi break from total duration
        const duration = 420;
        const clockPairs = [
            {
                clockIn: { datetime: '2022-06-03 21:00' },
                clockOut: { datetime: '2022-06-04 00:00' }
            },
            {
                clockIn: { datetime: '2022-06-04 01:00' },
                clockOut: { datetime: '2022-06-04 05:00' }
            }
        ];
        const breaks = [
            {
                start: '23:00',
                end: '03:00',
                break_hours: '01:00',
                type: 'flexi'
            }
        ];
        const shift = {
            date: '2022-06-03',
            start: '21:00',
            end: '06:00',
            schedule: {
                ...defaultSchedule[ 0 ],
                breaks
            }
        };
        const durationAfterBreaksDeduction = shiftsService.deductBreaksFromClockpairsDuration( shift, clockPairs, duration, 'asMinutes' );
        expect( durationAfterBreaksDeduction ).to.equal( 420 );
    });

    it( 'Night shift unpaid flexi break scenario 5', () => {
        // deduct unpaid flexi break from total duration
        const duration = 480;
        const clockPairs = [
            {
                clockIn: { datetime: '2022-06-03 21:00' },
                clockOut: { datetime: '2022-06-04 05:00' }
            }
        ];
        const breaks = [
            {
                start: '23:00',
                end: '03:00',
                break_hours: '01:00',
                type: 'flexi'
            }
        ];
        const shift = {
            date: '2022-06-03',
            start: '21:00',
            end: '06:00',
            schedule: {
                ...defaultSchedule[ 0 ],
                breaks
            }
        };
        const durationAfterBreaksDeduction = shiftsService.deductBreaksFromClockpairsDuration( shift, clockPairs, duration, 'asMinutes' );
        expect( durationAfterBreaksDeduction ).to.equal( 420 );
    });
});
