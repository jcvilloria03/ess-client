import TimeDisputeRequestService from '@/utils/services/TimeDisputeRequestService';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import 'es6-promise/auto';

const API_URL = process.env.API_URL;
let timeDisputeRequestService;
let mock;
const timeDisputeRequest = {
    activities: [
        {
            created_at: '2018-05-08 21:49:44',
            id: 1,
            params: {
                owner: {
                    employee_id: 703,
                    name: 'John do',
                    user_id: 1
                }
            },
            request_id: 9,
            type: 'request_created',
            updated_at: '2018-05-08 21:49:44'
        }
    ],
    employee_id: 1,
    employee_request_id: 1,
    end_date: '2018-10-12',
    id: 1,
    messages: [],
    owner: {
        employee_id: 1,
        name: 'John Doe',
        user_id: 1
    },
    params: {
        shifts: [
            {
                shift_id: 1,
                hours_worked: {
                    new_state: [
                        {
                            time: '05:00',
                            type: 'regular'
                        }
                    ],
                    old_state: [
                        {
                            time: '05:00',
                            type: 'regular'
                        }
                    ]
                },
                timesheet: {
                    new_state: [
                        {
                            tags: [
                                'tag 1'
                            ],
                            time: '05:00',
                            type: 'clock_in'
                        },
                        {
                            tags: [
                                'tag 1'
                            ],
                            time: '07:00',
                            type: 'clock_out'
                        }
                    ],
                    old_state: []
                },
                request: {
                    activities: [
                        {
                            created_at: '2018-05-08 21:49:44',
                            id: 1,
                            params: {
                                owner: {
                                    employee_id: 703,
                                    name: 'John do',
                                    user_id: 1
                                }
                            },
                            request_id: 9,
                            type: 'request_created',
                            updated_at: '2018-05-08 21:49:44'
                        }
                    ],
                    approver_responses: [],
                    company_id: 1,
                    created_at: '2018-05-08 21:49:42',
                    employee_id: 1,
                    id: 1,
                    messages: [],
                    params: {
                        owner: {
                            employee_id: 2,
                            name: 'Jo Doe',
                            user_id: 2
                        },
                        position: 1,
                        request_id: 1,
                        request_type: 'time_dispute_request',
                        status: 'Pending',
                        updated_at: '2018-05-08 21:49:42',
                        workflow_id: 1
                    }
                },
                shifts: [
                    {
                        company_id: 1,
                        dates: [ '2018-01-31', '2018-02-22' ],
                        employee: null,
                        employee_id: 1,
                        end: null,
                        id: 1,
                        schedule: {
                            affected_employees: [
                                {
                                    id: 1,
                                    created_at: '2017-12-27 14:15:26',
                                    rel_id: 1,
                                    schedule_id: 416,
                                    type: 'employee',
                                    updated_at: '2017-12-27 14:15:26'
                                }
                            ],
                            allowed_methods: [],
                            auto_assign: false,
                            breaks: [],
                            company_id: 1,
                            end_time: '18:00',
                            id: 1,
                            name: 'test1',
                            repeat: {},
                            start_date: '2017-12-27',
                            start_time: '08:00',
                            tags: [],
                            total_hours: '10:00',
                            type: 'fixed'
                        },
                        schedule_id: 1,
                        start: '2018-03-01'
                    }
                ],
                start_date: '2018-10-10',
                status: 'Pending',
                timesheets: [
                    {
                        company_uid: 1,
                        employee_uid: 1,
                        state: true,
                        tags: ['tag1'],
                        timestamp: 1525441199
                    }
                ]
            }
        ]
    }
};

const data = {
    start_date: '2018-10-10',
    end_date: '2018-10-12',
    messages: [
        'hello'
    ],
    shifts: [
        {
            shift_id: 442,
            timesheet: {
                old_state: [
                    {
                        type: 'clock_in',
                        timestamp: 1529478000, // 20. jun 2018 9:00:00 GMT+02:00
                        tags: [
                            'Tag1'
                        ]
                    },
                    {
                        type: 'clock_out',
                        timestamp: 1529496000, // 20. jun 2018 14:00:00 GMT+02:00
                        tags: [
                            'Tag1'
                        ]
                    }
                ],
                new_state: [
                    {
                        type: 'clock_in',
                        timestamp: 1529481600, // 20. jun 2018 10:00:00 GMT+02:00
                        tags: [
                            'Tag1'
                        ]
                    },
                    {
                        type: 'clock_out',
                        time: '15:00',
                        tags: [
                            'Tag1'
                        ]
                    }
                ]
            },
            hours_worked: {
                old_state: [
                    {
                        time: '05:00',
                        type: 'regular'
                    }
                ],
                new_state: [
                    {
                        time: '05:00',
                        type: 'regular'
                    }
                ]
            }
        }
    ]
};

describe( 'TimeDisputeRequestService.js', () => {
    beforeEach( () => {
        timeDisputeRequestService = new TimeDisputeRequestService();
        mock = new MockAdapter( axios );
    });

    afterEach( () => {
        timeDisputeRequestService = null;
        mock.restore();
    });

    it( 'can fetch request with messages', ( done ) => {
        mock.onGet( 'ess/time_dispute_request/1' ).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );

            return [ 200, [
                timeDisputeRequest
            ]];
        });

        timeDisputeRequestService.fetchTimeDisputeRequest( 1 ).then( ( messages ) => {
            expect( messages ).to.be.an( 'array' ).to.have.lengthOf( 1 );
            expect( messages[ 0 ]).to.eql( timeDisputeRequest );

            done();
        });
    });

    it( 'can store time correction request', ( done ) => {
        mock.onPost( 'ess/time_dispute_request' ).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );

            return [ 200, data ];
        });

        timeDisputeRequestService.submitTimeDisputeRequest( data ).then( ( response ) => {
            expect( response ).to.be.an( 'object' );
            expect( response ).to.eql({
                id: 1
            });
        });

        done();
    });

    it( 'transform timesheet time records for indication - case 1', ( done ) => {
        const timesheet = {
            old_state: [
                {
                    type: 'clock_in',
                    timestamp: 1529474400, // 20. jun 2018 8:00:00 GMT+02:00 DST
                    tags: [
                        'Tag1'
                    ]
                },
                {
                    type: 'clock_out',
                    timestamp: 1529504880, // 20. jun 2018 16:28:00 GMT+02:00 DST
                    tags: [
                        'Tag1'
                    ]
                }
            ],
            new_state: [
                {
                    type: 'clock_in',
                    timestamp: 1529474400, // 20. jun 2018 8:00:00 GMT+02:00 DST
                    tags: [
                        'Tag1'
                    ]
                },
                {
                    type: 'clock_out',
                    timestamp: 1529506800, // 20. jun 2018 17:00:00 GMT+02:00
                    tags: [
                        'Tag1'
                    ]
                },
                {
                    type: 'clock_in',
                    timestamp: 1529510400, // 20. jun 2018 18:00:00 GMT+02:00
                    tags: [
                        'Tag1'
                    ]
                },
                {
                    type: 'clock_out',
                    timestamp: 1529514000, // 20. jun 2018 19:00:00 GMT+02:00
                    tags: [
                        'Tag1'
                    ]
                }
            ]
        };

        const expected = [
            {
                type: 'clock_in',
                timestamp: 1529474400, // 20. jun 2018 8:00:00 GMT+02:00 DST
                tags: [
                    'Tag1'
                ]
            },
            {
                type: 'clock_out',
                timestamp: 1529504880, // 20. jun 2018 16:28:00 GMT+02:00 DST
                tags: [
                    'Tag1'
                ],
                action: 'old'
            },
            {
                type: 'clock_out',
                timestamp: 1529506800, // 20. jun 2018 17:00:00 GMT+02:00
                tags: [
                    'Tag1'
                ],
                action: 'new'
            },
            {
                type: 'clock_in',
                timestamp: 1529510400, // 20. jun 2018 18:00:00 GMT+02:00
                tags: [
                    'Tag1'
                ],
                action: 'added'
            },
            {
                type: 'clock_out',
                timestamp: 1529514000, // 20. jun 2018 19:00:00 GMT+02:00
                tags: [
                    'Tag1'
                ],
                action: 'added'
            }
        ];

        const actual = timeDisputeRequestService.getTimeDataForIndication( timesheet, true );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'transform timesheet time records for indication - case 2', ( done ) => {
        const timesheet = {
            old_state: [
                {
                    type: 'clock_in',
                    timestamp: 1529474400, // 20. jun 2018 8:00:00 GMT+02:00 DST,
                    tags: [
                        'Tag1'
                    ]
                },
                {
                    type: 'clock_out',
                    timestamp: 1529504880, // 20. jun 2018 16:28:00 GMT+02:00 DST
                    tags: [
                        'Tag1'
                    ]
                }
            ],
            new_state: [
                {
                    type: 'clock_in',
                    timestamp: 1529474400, // 20. jun 2018 8:00:00 GMT+02:00 DST,
                    tags: [
                        'Tag1'
                    ]
                },
                {
                    type: 'clock_out',
                    timestamp: 1529506800, // 20. jun 2018 17:00:00 GMT+02:00
                    tags: [
                        'Tag1'
                    ]
                }
            ]
        };

        const expected = [
            {
                type: 'clock_in',
                timestamp: 1529474400, // 20. jun 2018 8:00:00 GMT+02:00 DST,
                tags: [
                    'Tag1'
                ]
            },
            {
                type: 'clock_out',
                timestamp: 1529504880, // 20. jun 2018 16:28:00 GMT+02:00 DST
                tags: [
                    'Tag1'
                ],
                action: 'old'
            },
            {
                type: 'clock_out',
                timestamp: 1529506800, // 20. jun 2018 17:00:00 GMT+02:00
                tags: [
                    'Tag1'
                ],
                action: 'new'
            }
        ];

        const actual = timeDisputeRequestService.getTimeDataForIndication( timesheet, true );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'transform timesheet time records for indication - case 3', ( done ) => {
        const timesheet = {
            old_state: [
                {
                    type: 'clock_in',
                    timestamp: 1529474400, // 20. jun 2018 8:00:00 GMT+02:00 DST,
                    tags: [
                        'Tag1'
                    ]
                },
                {
                    type: 'clock_out',
                    timestamp: 1529504880, // 20. jun 2018 16:28:00 GMT+02:00 DST
                    tags: [
                        'Tag1'
                    ]
                }
            ],
            new_state: [
                {
                    type: 'clock_out',
                    timestamp: 1529506800, // 20. jun 2018 17:00:00 GMT+02:00
                    tags: [
                        'Tag1'
                    ]
                }
            ]
        };

        const expected = [
            {
                type: 'clock_in',
                timestamp: 1529474400, // 20. jun 2018 8:00:00 GMT+02:00 DST,
                tags: [
                    'Tag1'
                ],
                action: 'old'
            },
            {
                type: 'clock_in',
                timestamp: 0,
                tags: [
                    'Tag1'
                ],
                action: 'new'
            },
            {
                type: 'clock_out',
                timestamp: 1529504880, // 20. jun 2018 16:28:00 GMT+02:00 DST
                tags: [
                    'Tag1'
                ],
                action: 'old'
            },
            {
                type: 'clock_out',
                timestamp: 1529506800, // 20. jun 2018 17:00:00 GMT+02:00
                tags: [
                    'Tag1'
                ],
                action: 'new'
            }
        ];

        const actual = timeDisputeRequestService.getTimeDataForIndication( timesheet, true );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'transform timesheet time records for indication - case 4', ( done ) => {
        const timesheet = {
            old_state: [
                {
                    type: 'clock_in',
                    timestamp: 1529474400, // 20. jun 2018 8:00:00 GMT+02:00 DST,
                    tags: [
                        'Tag1'
                    ]
                },
                {
                    type: 'clock_out',
                    timestamp: 1529504880, // 20. jun 2018 16:28:00 GMT+02:00 DST
                    tags: [
                        'Tag1'
                    ]
                }
            ],
            new_state: []
        };

        const expected = [
            {
                type: 'clock_in',
                timestamp: 1529474400, // 20. jun 2018 8:00:00 GMT+02:00 DST,
                tags: [
                    'Tag1'
                ],
                action: 'old'
            },
            {
                type: 'clock_in',
                timestamp: 0,
                tags: [
                    'Tag1'
                ],
                action: 'new'
            },
            {
                type: 'clock_out',
                timestamp: 1529504880, // 20. jun 2018 16:28:00 GMT+02:00 DST
                tags: [
                    'Tag1'
                ],
                action: 'old'
            },
            {
                type: 'clock_out',
                timestamp: 0,
                tags: [
                    'Tag1'
                ],
                action: 'new'
            }
        ];

        const actual = timeDisputeRequestService.getTimeDataForIndication( timesheet, true );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'transform timesheet time records for indication - case 5', ( done ) => {
        const timesheet = {
            old_state: [
                {
                    type: 'clock_in',
                    timestamp: 1529496000, // 20. jun 2018 14:00:00 GMT+02:00
                    tags: [
                        'Tag1'
                    ]
                },
                {
                    type: 'clock_out',
                    timestamp: 1529504880, // 20. jun 2018 16:28:00 GMT+02:00 DST
                    tags: [
                        'Tag1'
                    ]
                }
            ],
            new_state: [
                {
                    type: 'clock_in',
                    timestamp: 1529474400, // 20. jun 2018 8:00:00 GMT+02:00 DST,
                    tags: [
                        'Tag1'
                    ]
                },
                {
                    type: 'clock_out',
                    timestamp: 1529481600, // 20. jun 2018 10:00:00 GMT+02:0028',
                    tags: [
                        'Tag1'
                    ]
                },
                {
                    type: 'clock_in',
                    timestamp: 1529496000, // 20. jun 2018 14:00:00 GMT+02:00
                    tags: [
                        'Tag1'
                    ]
                },
                {
                    type: 'clock_out',
                    timestamp: 1529504880, // 20. jun 2018 16:28:00 GMT+02:00 DST
                    tags: [
                        'Tag1'
                    ]
                }
            ]
        };

        const expected = [
            {
                type: 'clock_in',
                timestamp: 1529496000, // 20. jun 2018 14:00:00 GMT+02:00
                tags: [
                    'Tag1'
                ]
            },
            {
                type: 'clock_out',
                timestamp: 1529504880, // 20. jun 2018 16:28:00 GMT+02:00 DST
                tags: [
                    'Tag1'
                ]
            },
            {
                type: 'clock_in',
                timestamp: 1529474400, // 20. jun 2018 8:00:00 GMT+02:00 DST,
                tags: [
                    'Tag1'
                ],
                action: 'added'
            },
            {
                type: 'clock_out',
                timestamp: 1529481600, // 20. jun 2018 10:00:00 GMT+02:0028',
                tags: [
                    'Tag1'
                ],
                action: 'added'
            }
        ];

        const actual = timeDisputeRequestService.getTimeDataForIndication( timesheet, true );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'transform timesheet worked hours for indication - case 1', ( done ) => {
        const workedHours = {
            old_state: [
                {
                    time: '05:00',
                    type: 'regular'
                }
            ],
            new_state: [
                {
                    time: '05:00',
                    type: 'regular'
                }
            ]
        };

        const expected = [
            {
                time: '05:00',
                type: 'regular'
            }
        ];

        const actual = timeDisputeRequestService.getTimeDataForIndication( workedHours, false );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'transform timesheet worked hours for indication - case 2', ( done ) => {
        const workedHours = {
            old_state: [
                {
                    time: '05:00',
                    type: 'regular'
                }
            ],
            new_state: [
                {
                    time: '06:00',
                    type: 'regular'
                }
            ]
        };

        const expected = [
            {
                time: '05:00',
                type: 'regular',
                action: 'old'
            },
            {
                time: '06:00',
                type: 'regular',
                action: 'new'
            }
        ];

        const actual = timeDisputeRequestService.getTimeDataForIndication( workedHours, false );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'transform timesheet worked hours for indication - case 3', ( done ) => {
        const workedHours = {
            old_state: [
                {
                    type: 'regular',
                    time: '05:00'
                }
            ],
            new_state: [
                {
                    type: 'regular',
                    time: '05:00'
                },
                {
                    type: 'overtime',
                    time: '01:00'
                },
                {
                    type: 'night_shift',
                    time: '01:00'
                },
                {
                    type: 'overtime_night_shift',
                    time: '01:00'
                }
            ]
        };

        const expected = [
            {
                type: 'regular',
                time: '05:00'
            },
            {
                type: 'overtime',
                time: '01:00',
                action: 'added'
            },
            {
                type: 'night_shift',
                time: '01:00',
                action: 'added'
            },
            {
                type: 'overtime_night_shift',
                time: '01:00',
                action: 'added'
            }
        ];

        const actual = timeDisputeRequestService.getTimeDataForIndication( workedHours, false );

        const actualJSON = JSON.stringify( actual );
        const expectedJSON = JSON.stringify( expected );
        expect( actualJSON ).to.eql( expectedJSON );

        done();
    });

    it( 'get number of indication data changes', ( done ) => {
        const indicationData = [
            {
                type: 'clock_in',
                timestamp: 1529474400, // 20. jun 2018 8:00:00 GMT+02:00 DST,
                tags: [
                    'Tag1'
                ],
                action: 'old'
            },
            {
                type: 'clock_in',
                time: 'none',
                tags: [
                    'Tag1'
                ],
                action: 'new'
            },
            {
                type: 'clock_out',
                timestamp: 1529504880, // 20. jun 2018 16:28:00 GMT+02:00 DST
                tags: [
                    'Tag1'
                ],
                action: 'old'
            },
            {
                type: 'clock_out',
                time: 'none',
                tags: [
                    'Tag1'
                ],
                action: 'new'
            }
        ];

        const actual = timeDisputeRequestService.getNumberOfChanges( indicationData );
        expect( actual ).to.eql( 2 );

        done();
    });
});
