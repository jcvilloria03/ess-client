import AnnouncementService from '@/utils/services/AnnouncementService';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import 'es6-promise/auto';

const API_URL = process.env.API_URL;
let announcementService;
let mock;

describe( 'AnnouncementService.js', () => {
    beforeEach( () => {
        announcementService = new AnnouncementService();
        mock = new MockAdapter( axios );
    });

    afterEach( () => {
        announcementService = null;
        mock.restore();
    });

    it( 'can get announcement by id', ( done ) => {
        const existingAnnouncement = {
            id: 1,
            sender_id: 1,
            subject: 'Holiday',
            message: 'Lorem ipsum dolor sit amet, ex iriure definitionem per,in est exerci repudiare, usu zril perfecto ea. No vim nisl patrioque, at sed ferri mnesarchum. Ad movet quando vivendum usu, his esse propriae intellegat no. Ei duo euripidis dissentiet, appareat platonem accommodare eam ea. At sea laboramus intellegebat, eam saperet probatus id.',
            read_receipt: false,
            allow_response: false,
            sender: {
                name: 'J Wallace Bird',
                user_id: 1,
                employee_id: 651
            },
            recipient_groups: [
                {
                    id: null,
                    name: 'All Employees',
                    type: 'employee'
                }
            ],
            created_at: '2017-05-05 00:00:00',
            recipients: {
                data: [{
                    id: 1,
                    recipient_id: 2,
                    seen: false,
                    ip_address: null,
                    recipient: null,
                    seen_at: '16:08 March 12, 2018'
                }]
            },
            replies: {
                data: []
            }
        };
        mock.onGet( 'ess/announcement/1' ).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );

            return [ 200, existingAnnouncement ];
        });

        announcementService.getAnnouncementById( 1 ).then( ( announcement ) => {
            expect( announcement ).to.be.an( 'object' );
            expect( announcement ).to.eql( existingAnnouncement );

            done();
        });
    });

    it( 'gets announcements', ( done ) => {
        const existingAnnouncements = {
            data: [
                {
                    id: 1,
                    sender_id: 1,
                    subject: 'Holiday',
                    message: 'Lorem ipsum dolor sit amet, ex iriure definitionem per,in est exerci repudiare, usu zril perfecto ea. No vim nisl patrioque, at sed ferri mnesarchum. Ad movet quando vivendum usu, his esse propriae intellegat no. Ei duo euripidis dissentiet, appareat platonem accommodare eam ea. At sea laboramus intellegebat, eam saperet probatus id.',
                    read_receipt: false,
                    allow_response: false,
                    sender: {
                        name: 'J Wallace Bird',
                        user_id: 1,
                        employee_id: 651
                    },
                    recipient_groups: [
                        {
                            id: null,
                            name: 'All Employees',
                            type: 'employee'
                        }
                    ],
                    created_at: '2017-05-05 00:00:00',
                    recipients: {
                        data: [{
                            id: 1,
                            recipient_id: 2,
                            seen: false,
                            ip_address: null,
                            recipient: null,
                            seen_at: '16:08 March 12, 2018'
                        }]
                    },
                    replies: {
                        data: []
                    }
                },
                {
                    id: 2,
                    sender_id: 1,
                    subject: 'Holiday',
                    message: 'Lorem ipsum dolor sit amet, ex iriure definitionem per,in est exerci repudiare, usu zril perfecto ea. No vim nisl patrioque, at sed ferri mnesarchum. Ad movet quando vivendum usu, his esse propriae intellegat no. Ei duo euripidis dissentiet, appareat platonem accommodare eam ea. At sea laboramus intellegebat, eam saperet probatus id.',
                    read_receipt: false,
                    allow_response: false,
                    sender: {
                        name: 'J Wallace Bird',
                        user_id: 1,
                        employee_id: 651
                    },
                    recipient_groups: [
                        {
                            id: null,
                            name: 'All Employees',
                            type: 'employee'
                        }
                    ],
                    created_at: '2017-05-05 00:00:00',
                    recipients: {
                        data: [{
                            id: 1,
                            recipient_id: 2,
                            seen: false,
                            ip_address: null,
                            recipient: null,
                            seen_at: '16:08 March 12, 2018'
                        }]
                    },
                    replies: {
                        data: []
                    }
                }
            ]
        };

        mock.onPost( 'ess/employee/announcements?page=1' ).reply( 200, existingAnnouncements );

        announcementService.getAnnouncements({}, 1 ).then( ( announcements ) => {
            expect( announcements ).to.be.an( 'object' );
            expect( announcements ).to.eql( existingAnnouncements );

            done();
        });
    });

    it( 'can store announcement', ( done ) => {
        mock.onPost( 'ess/announcement' ).reply( 201, {
            id: 13
        });

        announcementService.submitAnnouncement({
            subject: 'Test1',
            message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            read_receipt: true,
            allow_response: true,
            affected_employees: [
                {
                    id: 651,
                    type: 'employee'
                }
            ]
        }).then( ( announcement ) => {
            expect( announcement ).to.be.an( 'object' );
            expect( announcement ).to.eql({
                id: 13
            });

            done();
        });
    });

    it( 'can store announcement reply', ( done ) => {
        const storedReply = {
            message: 'test 1',
            sender_id: 2,
            params: {
                sender: {
                    employee_id: 652,
                    user_id: 2,
                    name: 'Sam Hood'
                }
            },
            announcement_id: 11,
            updated_at: '2018-03-15 12:23:56',
            created_at: '2018-03-15 12:23:56',
            id: 3,
            announcement: {
                id: 11,
                sender_id: 1,
                subject: 'Employees shifts',
                message: 'Lorem ipsum dolor sit amet, ex iriure definitionem per,in est exerci repudiare, usu zril perfecto ea. No vim nisl patrioque, at sed ferri mnesarchum. Ad movet quando vivendum usu, his esse propriae intellegat no. Ei duo euripidis dissentiet, appareat platonem accommodare eam ea. At sea laboramus intellegebat, eam saperet probatus id.',
                read_receipt: true,
                allow_response: true,
                sender: {
                    name: 'J Wallace Bird',
                    user_id: 1,
                    employee_id: 651
                },
                relations: [
                    {
                        id: null,
                        name: 'All employees',
                        type: 'employee'
                    }
                ],
                created_at: '2018-03-13 13:09:49',
                updated_at: '2018-03-13 13:09:49'
            }
        };
        mock.onPost( 'ess/announcement/11/reply' ).reply( 201, storedReply );

        announcementService.submitReply( 11, 'test 1' ).then( ( reply ) => {
            expect( reply ).to.be.an( 'object' );
            expect( reply ).to.eql( storedReply );

            done();
        });
    });

    it( 'Gets view count', ( done ) => {
        mock.onPost( 'ess/employee/count' ).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );
            expect( JSON.parse( config.data ).affected_employees ).to.be.an( 'array' ).to.have.lengthOf( 1 );

            return [ 200, { count: 1 }];
        });

        announcementService.getViewCount([{
            id: 651,
            type: 'employee'
        }]).then( ( count ) => {
            expect( count ).to.equal( 1 );

            done();
        });
    });
});
