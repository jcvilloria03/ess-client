import SearchCompanyService from '@/utils/services/SearchCompanyService';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import 'es6-promise/auto';

const API_URL = process.env.API_URL;
let searchCompanyService;
let mock;

describe( 'SearchCompanyService.js', () => {
    beforeEach( () => {
        searchCompanyService = new SearchCompanyService();
        mock = new MockAdapter( axios );
    });

    afterEach( () => {
        searchCompanyService = null;
        mock.restore();
    });

    it( 'searches location, department, position or employee name.', ( done ) => {
        const affectedEmployees = [
            {
                id: 658,
                name: 'Christopher James Montes Santana - PM00A5',
                type: 'employee'
            },
            {
                id: 672,
                name: 'Christopher James Miles Morriso - PM00A37',
                type: 'employee'
            },
            {
                id: 686,
                name: 'Christopher James Sample Schmidt - PM00A15',
                type: 'employee'
            }
        ];
        mock.onGet( '/ess/affected_employees/search', {
            params: {
                term: 'HR',
                limit: 10,
                include_admins: false
            }
        }).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );

            return [ 200, affectedEmployees ];
        });

        searchCompanyService.fetchEntitledEmployees( 'HR', 10 ).then( ( employees ) => {
            expect( employees ).to.be.an( 'array' ).to.have.lengthOf( 3 );
            expect( employees ).to.eql( affectedEmployees );

            done();
        });
    });
});
