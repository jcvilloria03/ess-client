import LeaveRequestsService from '@/utils/services/LeaveRequestsService';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import 'es6-promise/auto';

const API_URL = process.env.API_URL;
let leaveRequestsService;
let mock;

const leaveCredit = {
    id: 1,
    start_date: '2017-01-01',
    end_date: '2019-01-01',
    leave_type_id: 1,
    leaves: 12,
    total_leave_hours: 48,
    workflow_id: 1,
    employee_id: 1,
    status: 0,
    messages: [
        {
            id: 1,
            request_id: 1,
            sender_id: 1,
            content: 'bla bla bla',
            created_at: '2017-05-05 00:00:00',
            updated_at: '2017-05-05 00:00:00'
        }
    ]
};

const selectedLeaves = [
    {
        date: '2018-05-08',
        start: '06:00',
        end: '12:00',
        schedule_id: 1,
        schedule: {
            start_date: '2018-05-06',
            start_time: '06:00',
            end_time: '12:00',
            total_hours: '06:00'
        }
    },
    {
        date: '2018-05-09',
        start: '06:00',
        end: '12:00',
        schedule_id: 2,
        schedule: {
            start_date: '2018-05-06',
            start_time: '06:00',
            end_time: '12:00',
            total_hours: '06:00'
        }
    }
];

const defaultScheduleData = [
    {
        company_id: 1,
        day_of_week: 1,
        day_type: 'regular',
        id: 1,
        work_break_end: '13:00:00',
        work_break_start: '12:00:00',
        work_end: '17:00:00',
        work_start: '08:00:00'
    },
    {
        company_id: 1,
        day_of_week: 2,
        day_type: 'regular',
        id: 2,
        work_break_end: '13:00:00',
        work_break_start: '12:00:00',
        work_end: '17:00:00',
        work_start: '08:00:00'
    },
    {
        company_id: 1,
        day_of_week: 3,
        day_type: 'regular',
        id: 3,
        work_break_end: '13:00:00',
        work_break_start: '12:00:00',
        work_end: '17:00:00',
        work_start: '08:00:00'
    },
    {
        company_id: 1,
        day_of_week: 4,
        day_type: 'regular',
        id: 4,
        work_break_end: '13:00:00',
        work_break_start: '12:00:00',
        work_end: '17:00:00',
        work_start: '08:00:00'
    },
    {
        company_id: 1,
        day_of_week: 5,
        day_type: 'regular',
        id: 5,
        work_break_end: '13:00:00',
        work_break_start: '12:00:00',
        work_end: '17:00:00',
        work_start: '08:00:00'
    },
    {
        company_id: 1,
        day_of_week: 6,
        day_type: 'regular',
        id: 6,
        work_break_end: '13:00:00',
        work_break_start: '12:00:00',
        work_end: '17:00:00',
        work_start: '08:00:00'
    },
    {
        company_id: 1,
        day_of_week: 7,
        day_type: 'regular',
        id: 7,
        work_break_end: '13:00:00',
        work_break_start: '12:00:00',
        work_end: '17:00:00',
        work_start: '08:00:00'
    }
];

const taEmployee = {
    products: [
        { name: 'time and attendance' }
    ],
    hours_per_day: '3.00'
};

const payrollEmployee = {
    products: [
        { name: 'payroll' }
    ],
    payroll_group: {
        hours_per_day: '3.00'
    }
};
/**
 * Gets payload data for filed leave calculation handler for test purpos.
 *
 * @param {String} unit
 * @param {Object} employee
 *
 * @returns {Object}
 */
function getPayloadDataForFiledLeaveCalculation( unit = 'hours', employee = taEmployee ) {
    return {
        filteredLeavesByDate: selectedLeaves,
        unit,
        employee,
        defaultScheduleData
    };
}

describe( 'LeaveRequestsService.js', () => {
    beforeEach( () => {
        leaveRequestsService = new LeaveRequestsService();
        mock = new MockAdapter( axios );
    });

    afterEach( () => {
        leaveRequestsService = null;
        mock.restore();
    });

    it( 'can fetch request with messages', ( done ) => {
        mock.onGet( 'ess/leave_request/1' ).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );

            return [ 200, [
                leaveCredit
            ]];
        });

        leaveRequestsService.fetchLeaveRequest( 1 ).then( ( messages ) => {
            expect( messages ).to.be.an( 'array' ).to.have.lengthOf( 1 );
            expect( messages[ 0 ]).to.eql({
                id: 1,
                start_date: '2017-01-01',
                end_date: '2019-01-01',
                leave_type_id: 1,
                leaves: 12,
                total_leave_hours: 48,
                workflow_id: 1,
                employee_id: 1,
                status: 0,
                messages: [
                    {
                        id: 1,
                        request_id: 1,
                        sender_id: 1,
                        content: 'bla bla bla',
                        created_at: '2017-05-05 00:00:00',
                        updated_at: '2017-05-05 00:00:00'
                    }
                ]
            });

            done();
        });
    });

    it( 'can calculate filed leave units - TA only in hours', () => {
        const response = leaveRequestsService.handleFiledLeaveTotalValueCalculation(
            getPayloadDataForFiledLeaveCalculation()
         );

        expect( response ).to.eql( '12.00' );
    });

    it( 'can calculate filed leave units - TA only in days', () => {
        const response = leaveRequestsService.handleFiledLeaveTotalValueCalculation(
            getPayloadDataForFiledLeaveCalculation( 'days' )
         );
        expect( response ).to.eql( '4.00' );
    });

    it( 'can calculate filed leave units - Payroll only in hours', () => {
        const response = leaveRequestsService.handleFiledLeaveTotalValueCalculation(
            getPayloadDataForFiledLeaveCalculation( 'hours', payrollEmployee )
         );
        expect( response ).to.eql( '12.00' );
    });

    it( 'can calculate filed leave units - Payroll only in days', () => {
        const response = leaveRequestsService.handleFiledLeaveTotalValueCalculation(
            getPayloadDataForFiledLeaveCalculation( 'days', payrollEmployee )
         );
        expect( response ).to.eql( '4.00' );
    });

    it( 'can validate filed leave time range - regular shift with invalid start_time', () => {
        const response = leaveRequestsService.isLeaveTimeBetweenScheduleTimeRange(
            {
                start_time: '06:59',
                end_time: '15:00',
                schedule: {
                    start_time: '07:00',
                    end_time: '15:00'
                }
            }
        );
        expect( response ).to.be.false;
    });

    it( 'can validate filed leave time range - regular shift with invalid end_time', () => {
        const response = leaveRequestsService.isLeaveTimeBetweenScheduleTimeRange(
            {
                start_time: '07:00',
                end_time: '15:01',
                schedule: {
                    start_time: '07:00',
                    end_time: '15:00'
                }
            }
        );
        expect( response ).to.be.false;
    });

    it( 'can validate filed leave time range - regular shift with invalid start_time and end_time', () => {
        const response = leaveRequestsService.isLeaveTimeBetweenScheduleTimeRange(
            {
                start_time: '06:00',
                end_time: '18:01',
                schedule: {
                    start_time: '07:00',
                    end_time: '15:00'
                }
            }
        );
        expect( response ).to.be.false;
    });

    it( 'can validate filed leave time range - regular shift with valid start_time and end_time', () => {
        const response = leaveRequestsService.isLeaveTimeBetweenScheduleTimeRange(
            {
                start_time: '07:00',
                end_time: '15:00',
                schedule: {
                    start_time: '07:00',
                    end_time: '15:00'
                }
            }
        );
        expect( response ).to.be.true;
    });

    it( 'can validate filed leave time range - night shift with valid start_time and end_time', () => {
        const response = leaveRequestsService.isLeaveTimeBetweenScheduleTimeRange(
            {
                start_time: '21:59',
                end_time: '07:00',
                schedule: {
                    start_time: '22:00',
                    end_time: '07:00'
                }
            }
        );
        expect( response ).to.be.false;
    });

    it( 'can validate filed leave time range - night shift with valid start_time and end_time', () => {
        const response = leaveRequestsService.isLeaveTimeBetweenScheduleTimeRange(
            {
                start_time: '22:00',
                end_time: '07:01',
                schedule: {
                    start_time: '22:00',
                    end_time: '07:00'
                }
            }
        );
        expect( response ).to.be.false;
    });

    it( 'can validate filed leave time range - night shift with valid start_time and end_time', () => {
        const response = leaveRequestsService.isLeaveTimeBetweenScheduleTimeRange(
            {
                start_time: '22:00',
                end_time: '23:59',
                schedule: {
                    start_time: '22:00',
                    end_time: '07:00'
                }
            }
        );
        expect( response ).to.be.true;
    });

    it( 'can validate filed leave time range - night shift with valid start_time and end_time', () => {
        const response = leaveRequestsService.isLeaveTimeBetweenScheduleTimeRange(
            {
                start_time: '22:00',
                end_time: '00:00',
                schedule: {
                    start_time: '22:00',
                    end_time: '07:00'
                }
            }
        );
        expect( response ).to.be.true;
    });

    it( 'can validate filed leave time range - night shift with valid start_time and end_time', () => {
        const response = leaveRequestsService.isLeaveTimeBetweenScheduleTimeRange(
            {
                start_time: '00:00',
                end_time: '03:59',
                schedule: {
                    start_time: '22:00',
                    end_time: '07:00'
                }
            }
        );
        expect( response ).to.be.true;
    });

    it( 'can validate filed leave time range - night shift with invalid start_time and end_time', () => {
        const response = leaveRequestsService.isLeaveTimeBetweenScheduleTimeRange(
            {
                start_time: '11:00',
                end_time: '09:59',
                schedule: {
                    start_time: '22:00',
                    end_time: '07:00'
                }
            }
        );
        expect( response ).to.be.false;
    });

    it( 'can validate filed leave time range - night shift with invalid start_time and end_time', () => {
        const response = leaveRequestsService.isLeaveTimeBetweenScheduleTimeRange(
            {
                start_time: '11:00',
                end_time: '07:00',
                schedule: {
                    start_time: '22:00',
                    end_time: '07:00'
                }
            }
        );
        expect( response ).to.be.false;
    });

    it( 'can validate filed leave time range - night shift with invalid start_time and end_time', () => {
        const response = leaveRequestsService.isLeaveTimeBetweenScheduleTimeRange(
            {
                start_time: '22:00',
                end_time: '09:00',
                schedule: {
                    start_time: '22:00',
                    end_time: '07:00'
                }
            }
        );
        expect( response ).to.be.false;
    });
});
