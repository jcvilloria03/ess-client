import NotificationsService from '@/utils/services/NotificationsService';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import 'es6-promise/auto';

const API_URL = process.env.API_URL;
let notificationsService;
let mock;

describe( 'NotificationsService.js', () => {
    beforeEach( () => {
        notificationsService = new NotificationsService();
        mock = new MockAdapter( axios );
    });

    afterEach( () => {
        notificationsService = null;
        mock.restore();
    });

    it( 'can fetch notifications', ( done ) => {
        mock.onGet( 'ess/notifications?page=1' ).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );

            return [ 200, {
                data: [
                    {
                        id: 1,
                        clicked: true,
                        activity_id: 1,
                        created_at: '2018-01-31 12:12:12'
                    }
                ]
            }];
        });

        notificationsService.fetchNotificationsData( 1 ).then( ( notifications ) => {
            expect( notifications ).to.be.an( 'array' ).to.have.lengthOf( 1 );
            expect( notifications[ 0 ]).to.eql({
                id: 1,
                clicked: true,
                activity_id: 1,
                created_at: '2018-01-31 12:12:12'
            });

            done();
        });
    });

    it( 'can update notification clicked status', ( done ) => {
        mock.onPut( 'ess/notifications/1/clicked' ).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );

            return [ 200, {
                activity: {
                    id: 1,
                    creator_id: 2,
                    owner_id: 3,
                    params: 'string',
                    type: 'notification',
                    updated_at: '2018-01-18 12:45:14'
                },
                activity_id: 1,
                created_at: '2018-01-31 12:12:12',
                id: 1,
                user_id: 1
            }];
        });

        notificationsService.updateNotificationClickedStatus( 1 ).then( ( notification ) => {
            expect( notification ).to.be.an( 'object' );
            expect( notification ).to.eql({
                activity: {
                    id: 1,
                    creator_id: 2,
                    owner_id: 3,
                    params: 'string',
                    type: 'notification',
                    updated_at: '2018-01-18 12:45:14'
                },
                activity_id: 1,
                created_at: '2018-01-31 12:12:12',
                id: 1,
                user_id: 1
            });

            done();
        });
    });

    it( 'can update notifications  status', ( done ) => {
        mock.onPut( 'ess/notifications_status' ).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );

            return [ 200, {
                active: false,
                id: 1,
                user_id: 1
            }];
        });

        notificationsService.updateNotificationStatus().then( ( notificationStatus ) => {
            expect( notificationStatus ).to.be.an( 'object' );
            expect( notificationStatus ).to.eql({
                active: false,
                id: 1,
                user_id: 1
            });

            done();
        });
    });

    it( 'gets user notifications status', ( done ) => {
        mock.onGet( 'ess/notifications_status' ).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );

            return [ 200, {
                id: 1,
                user_id: 1,
                active: false
            }];
        });

        notificationsService.getUserNotificationsStatus().then( ( notificationStatus ) => {
            expect( notificationStatus ).to.be.an( 'object' );
            expect( notificationStatus ).to.eql({
                id: 1,
                user_id: 1,
                active: false
            });

            done();
        });
    });
});
