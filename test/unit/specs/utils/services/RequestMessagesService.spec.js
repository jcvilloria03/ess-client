import RequestMessagesService from '@/utils/services/RequestMessagesService';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import 'es6-promise/auto';

const API_URL = process.env.API_URL;
let requestMessagesService;
let mock;

describe( 'RequestMessagesService.js', () => {
    beforeEach( () => {
        requestMessagesService = new RequestMessagesService();
        mock = new MockAdapter( axios );
    });

    afterEach( () => {
        requestMessagesService = null;
        mock.restore();
    });

    it( 'can store request  message', ( done ) => {
        mock.onPost( 'ess/request/send_message' ).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );

            return [ 201, {
                id: 1,
                request_id: 1,
                sender_id: 1,
                content: 'bla bla bla',
                created_at: '2017-05-05 00:00:00',
                updated_at: '2017-05-05 00:00:00'
            }];
        });

        requestMessagesService.storeRequestMessage( 1, 'bla bla bla' ).then( ( message ) => {
            expect( message ).to.be.an( 'object' );
            expect( message ).to.eql({
                id: 1,
                request_id: 1,
                sender_id: 1,
                content: 'bla bla bla',
                created_at: '2017-05-05 00:00:00',
                updated_at: '2017-05-05 00:00:00'
            });

            done();
        });
    });
});
