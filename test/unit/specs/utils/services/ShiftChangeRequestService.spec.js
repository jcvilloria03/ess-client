import ShiftChangeRequestService from '@/utils/services/ShiftChangeRequestService';
import { shiftsService } from '@/utils/services/ShiftsService';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import 'es6-promise/auto';

const API_URL = process.env.API_URL;
let shiftChangeRequestService;
let mock;
const shiftChangeRequest = {
    activities: [
        {
            created_at: '2018-05-08 21:49:44',
            id: 1,
            params: {
                owner: {
                    employee_id: 703,
                    name: 'John do',
                    user_id: 1
                }
            },
            request_id: 9,
            type: 'request_created',
            updated_at: '2018-05-08 21:49:44'
        }
    ],
    employee_id: 1,
    employee_request_id: 1,
    end_date: '2018-10-12',
    id: 1,
    messages: [],
    owner: {
        employee_id: 1,
        name: 'John Doe',
        user_id: 1
    },
    params: {
        shifts: {
            old: [
                {
                    shift_id: 443,
                    date: '2018-05-01'
                }
            ],
            new: [
                {
                    shift_id: 443,
                    date: '2018-08-31'
                }
            ]
        }
    }
};

const data = {
    start_date: '2018-10-10',
    end_date: '2018-10-12',
    messages: [
        'hello'
    ],
    shifts: {
        old: [
            {
                shift_id: 443,
                date: '2018-05-01'
            }
        ],
        new: [
            {
                shift_id: 443,
                date: '2018-08-31'
            }
        ]
    }
};

const restDay = {
    date: '2018-06-13',
    isAssigned: true,
    name: 'Rest Day',
    rest_day_id: 2,
    schedule: {
        name: 'Rest Day'
    }
};

const shiftsWithAllowedRestDay = [{
    date: '2018-05-08',
    end: '21:00',
    name: 'test13',
    isAssigned: true,
    schedule: {
        start_date: '2018-05-06',
        start_time: '16:00',
        end_time: '21:00',
        on_rest_day: true
    },
    shift_id: 27,
    start: '16:00'
}];

const shiftsWithoutRestDay = [{
    date: '2018-05-08',
    end: '20:00',
    name: 'test13',
    isAssigned: true,
    schedule: {
        start_date: '2018-05-06',
        start_time: '16:00',
        end_time: '20:00',
        on_rest_day: false
    },
    shift_id: 27,
    start: '16:00'
}];
const dayShifts = [{
    date: '2018-05-08',
    end: '12:00',
    name: 'test13',
    isAssigned: true,
    schedule: {
        start_date: '2018-05-06',
        start_time: '06:00',
        end_time: '12:00',
        on_rest_day: false
    },
    shift_id: 27,
    start: '06:00'
}];
const morningShiftsWithoutRestDay = [{
    date: '2018-05-08',
    end: '21:00',
    name: 'test13',
    isAssigned: true,
    schedule: {
        start_date: '2018-05-06',
        start_time: '06:00',
        end_time: '21:00',
        on_rest_day: false
    },
    shift_id: 27,
    start: '06:00'
}];
const nightShiftWithouthRestDay = [{
    date: '2018-05-08',
    end: '07:00',
    name: 'test13',
    isAssigned: true,
    schedule: {
        start_date: '2018-05-06',
        start_time: '23:00',
        end_time: '07:00',
        on_rest_day: false
    },
    shift_id: 27,
    start: '23:00'
}];
const nightShiftWithRestDay = [{
    date: '2018-05-08',
    end: '07:00',
    name: 'test13',
    isAssigned: true,
    schedule: {
        start_date: '2018-05-06',
        start_time: '23:00',
        end_time: '07:00',
        on_rest_day: true
    },
    shift_id: 27,
    start: '23:00'
}];

describe( 'ShiftChangeRequestService.js', () => {
    beforeEach( () => {
        shiftChangeRequestService = new ShiftChangeRequestService();
        mock = new MockAdapter( axios );
    });

    afterEach( () => {
        shiftChangeRequestService = null;
        mock.restore();
    });

    it( 'can fetch request with messages', ( done ) => {
        mock.onGet( 'ess/shift_change_request/1' ).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );

            return [ 200, [
                shiftChangeRequest
            ]];
        });

        shiftChangeRequestService.fetchShiftChangeRequest( 1 ).then( ( messages ) => {
            expect( messages ).to.be.an( 'array' ).to.have.lengthOf( 1 );
            expect( messages[ 0 ]).to.eql( shiftChangeRequest );

            done();
        });
    });

    it( 'can store shift change request', ( done ) => {
        mock.onPost( 'ess/shift_change_request' ).reply( ( config ) => {
            expect( config.baseURL ).to.equal( API_URL );

            return [ 200, data ];
        });

        shiftChangeRequestService.submitShiftChangeRequest( data ).then( ( response ) => {
            expect( response ).to.be.an( 'object' );
            expect( response ).to.eql({
                id: 1
            });
        });

        done();
    });

    it( 'can validate is allowed to add a rest day - valid case 1', () => {
        const response = shiftChangeRequestService.daySchedulesDoesNotAllowRestDay( restDay, shiftsWithAllowedRestDay );
        expect( response ).to.eql( false );
    });

    it( 'can validate is allowed to add a rest day - invalid case 2', () => {
        const response = shiftChangeRequestService.daySchedulesDoesNotAllowRestDay( restDay, shiftsWithoutRestDay );
        expect( response ).to.eql( true );
    });

    it( 'can validate is allowed to add a shift on a rest day - valid case 3', () => {
        const response = shiftChangeRequestService.isNewEventAllowedOnRestDay( shiftsWithAllowedRestDay[ 0 ], [restDay]);
        expect( response ).to.eql( false );
    });

    it( 'can validate is allowed to add a shift on a rest day - invalid case 4', () => {
        const response = shiftChangeRequestService.isNewEventAllowedOnRestDay( shiftsWithoutRestDay[ 0 ], [restDay]);
        expect( response ).to.eql( true );
    });

    it( 'can validate is allowed to add a nightShift on a rest day - valid case 5', () => {
        const response = shiftChangeRequestService.isNewEventAllowedOnRestDay( nightShiftWithRestDay[ 0 ], [restDay]);
        expect( response ).to.eql( false );
    });

    it( 'can validate is allowed to add a nightShift on a rest day - invalid case 6', () => {
        const response = shiftChangeRequestService.isNewEventAllowedOnRestDay( nightShiftWithouthRestDay[ 0 ], [restDay]);
        expect( response ).to.eql( true );
    });

    it( 'can validate is rest day overlaping previouse day night shift - valid case 7', () => {
        const response = shiftChangeRequestService.isRestDayOverlapingPreviouseDayNightShift( restDay, nightShiftWithRestDay[ 0 ]);
        expect( response ).to.eql( false );
    });

    it( 'can validate is event to be assigned overlaping previouse day events - valid case 8', () => {
        const response = shiftChangeRequestService.isEventOverlapingWithPreviouseDayEvents( restDay, nightShiftWithRestDay );
        expect( response ).to.eql( false );
    });

    it( 'can validate is event to be assigned overlaping previouse day events - invalid case 9', () => {
        const response = shiftChangeRequestService.isEventOverlapingWithPreviouseDayEvents( restDay, nightShiftWithouthRestDay );
        expect( response ).to.eql( true );
    });

    it( 'can validate is event to be assigned overlaping previouse day events - valid case 10', () => {
        const response = shiftChangeRequestService.isEventOverlapingWithPreviouseDayEvents( shiftsWithAllowedRestDay[ 0 ], nightShiftWithRestDay );
        expect( response ).to.eql( false );
    });

    it( 'can validate is event to be assigned overlaping previouse day events - invalid case 11', () => {
        const response = shiftChangeRequestService.isEventOverlapingWithPreviouseDayEvents( morningShiftsWithoutRestDay[ 0 ], nightShiftWithouthRestDay );
        expect( response ).to.eql( true );
    });

    it( 'can validate is night shift to be assigned allowed on next day rest day - valid case 12', () => {
        const response = shiftChangeRequestService.isEventOverlapingWithPreviouseDayEvents( nightShiftWithRestDay[ 0 ], restDay );
        expect( response ).to.eql( false );
    });

    it( 'can validate is night shift to be assigned allowed on next day rest day - invalid case 13', () => {
        const response = shiftChangeRequestService.isEventOverlapingWithPreviouseDayEvents( nightShiftWithouthRestDay[ 0 ], restDay );
        expect( response ).to.eql( false );
    });

    it( 'can validate is event to be assigned overlaping day events - valid case 14', () => {
        const dayShift = dayShifts[ 0 ];
        const shiftStart = shiftsService.getShiftDateTime( dayShift.date, dayShift.start );
        const shiftEnd = shiftsService.getShiftDateTime( dayShift.date, dayShift.end );
        const response = shiftChangeRequestService.isShiftOverlapingWithDayShifts(
            dayShift,
            shiftStart,
            shiftEnd,
            shiftsWithoutRestDay
        );

        expect( response ).to.eql( false );
    });

    it( 'can validate is night shift to be assigned allowed on next day rest day - valid case 15', () => {
        const dayShift = shiftsWithoutRestDay[ 0 ];
        const shiftStart = shiftsService.getShiftDateTime( dayShift.date, dayShift.start );
        const shiftEnd = shiftsService.getShiftDateTime( dayShift.date, dayShift.end );
        const response = shiftChangeRequestService.isShiftOverlapingWithDayShifts(
            dayShift,
            shiftStart,
            shiftEnd,
            shiftsWithoutRestDay
        );

        expect( response ).to.eql( true );
    });

    it( 'can validate is rest day overlaping previouse day night shift - valid case 16', () => {
        const response = shiftChangeRequestService.isRestDayOverlapingPreviouseDayNightShift( restDay, [restDay]);
        expect( response ).to.eql( true );
    });

    it( 'can validate is rest day overlaping previouse day night shift - valid case 17', () => {
        const response = shiftChangeRequestService.isRestDayOverlapingPreviouseDayNightShift( shiftsWithoutRestDay[ 0 ], shiftsWithoutRestDay );
        expect( response ).to.eql( false );
    });
    it( 'can validate is night shift overlaping next day or current day rest day - valid case 18', () => {
        const response = shiftChangeRequestService.isNightShiftAllowedOnRestDay( nightShiftWithRestDay[ 0 ], restDay );
        expect( response ).to.eql( false );
    });
    it( 'can validate is night shift overlaping next day or current day rest day - invalid case 18', () => {
        const response = shiftChangeRequestService.isNightShiftAllowedOnRestDay( nightShiftWithouthRestDay[ 0 ], restDay );
        expect( response ).to.eql( true );
    });
});
