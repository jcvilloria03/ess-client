import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import Authentication from '@/utils/Authentication';
import * as jwtHelpers from '@/utils/helpers/jwtHelpers';

let auth;
let axiosMockAdapter;
const API_URL = process.env.API_URL;

const changePasswordPayload = {
    current_password: '123456',
    new_password: '1234567',
    new_password_confirmation: '1234567'
};

describe( 'Authentication.js', () => {
    beforeEach( () => {
        auth = new Authentication();

        axiosMockAdapter = new MockAdapter( axios );
    });

    afterEach( () => {
        auth = null;

        axiosMockAdapter.restore();
        axiosMockAdapter = null;
    });

    it( 'can login and fetch token and data', ( done ) => {
        const username = 'test@example.com';
        const password = 'super-secret';
        // We'll pretend that we have actual token that has not expired.
        sinon.stub( jwtHelpers, 'isTokenExpired' ).returns( false );

        auth.auth0 = {
            oauthToken( parameters, callback ) {
                expect( parameters ).to.be.an( 'object' ).to.eql({
                    username,
                    password,
                    grantType: 'password'
                });

                callback( null, {
                    accessToken: 'asd123',
                    idToken: 'mnb456'
                });
            },

            userInfo( accessToken, callback ) {
                expect( accessToken ).to.equal( 'asd123' );

                callback( null, {});
            }
        };

        auth.login({ username, password }).then( () => {
            expect( auth.isAuthenticated() ).to.be.true;

            jwtHelpers.isTokenExpired.restore();

            done();
        });
    });

    it( 'throws and error when login fails because of invalid credentials', ( done ) => {
        const username = 'test@example.com';
        const password = 'super-secret';

        auth.auth0 = {
            oauthToken( parameters, callback ) {
                expect( parameters ).to.be.an( 'object' ).to.eql({
                    username,
                    password,
                    grantType: 'password'
                });

                callback({ description: 'Invalid credentials' }, null );
            },

            userInfo( accessToken, callback ) {
                expect( accessToken ).to.equal( 'asd123' );

                callback( null, {});
            }
        };

        auth.login({ username, password }).then( () => {
            expect( true ).to.be.false; // Fail in this case.

            done();
        }).catch( ( error ) => {
            expect( error ).to.equal( 'Invalid credentials' );

            done();
        });
    });

    it( 'throws and error when fails to get auth0 user info', ( done ) => {
        const username = 'test@example.com';
        const password = 'super-secret';

        axiosMockAdapter.onGet( 'ess/employee/user' ).reply( 200, {
            employee_id: 1,
            user_id: 1
        });

        auth.auth0 = {
            oauthToken( parameters, callback ) {
                expect( parameters ).to.be.an( 'object' ).to.eql({
                    username,
                    password,
                    grantType: 'password'
                });

                callback( null, {
                    accessToken: 'asd123',
                    idToken: 'mnb456',
                    expiresIn: 50000
                });
            },

            userInfo( accessToken, callback ) {
                expect( accessToken ).to.equal( 'asd123' );

                callback({ description: 'Error getting user info from auth0' }, null );
            }
        };

        auth.login({ username, password }).then( () => {
            expect( true ).to.be.false; // Fail in this case.

            done();
        }).catch( ( error ) => {
            expect( error ).to.equal( 'Error getting user info from auth0' );

            done();
        });
    });

    it( 'can change users password', ( done ) => {
        axiosMockAdapter.onPost( 'ess/change_password', changePasswordPayload )
            .reply( ( config ) => {
                expect( config.baseURL ).to.equal( API_URL );

                return [ 200, [
                    { password_changed: true }
                ]];
            });

        auth.sendChangePasswordRequest( changePasswordPayload )
            .then( ( response ) => {
                expect( response.password_changed ).to.be.true;

                done();
            }).catch( () => done() );
    });
});
