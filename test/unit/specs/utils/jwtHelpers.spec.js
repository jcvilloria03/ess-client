import { isTokenExpired } from '@/utils/helpers/jwtHelpers';

describe( 'jwtHelper.js', () => {
    describe( 'isTokenExpired()', () => {
        it( 'returns true if token expiration date has passed', () => {
            expect( isTokenExpired( 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjEwMH0.JFxLndjbPWumFQXkKlbG9SplwDBVDyoH-ZSVsriUWbY' ) )
                .to.be.true;
        });

        it( 'returns false if token has not yet expired', () => {
            expect( isTokenExpired( 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjkxMTExMTExMTExfQ.Bs_rSU_DHwPOw41ZA1AlSSsbXUjLgssc6z_CbGBbOis' ) )
            .to.be.false;
        });
    });
});
