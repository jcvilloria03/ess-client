import { shallow, mount } from 'avoriaz';
import A from '../../../../../src/components/mobile/A';

describe( 'A.vue', () => {
    it( 'renders slot context', () => {
        const messageWrapper = {
            render( h ) {
                return h( 'span' );
            }
        };
        const vm = shallow( A, {
            slots: {
                default: messageWrapper
            }
        });

        expect( vm.find( 'span' ).length ).to.equal( 1 );
    });

    it( 'applies passed attributes', () => {
        const vm = mount( A, {
            attrs: { href: '/test' }
        });

        expect( vm.vm.$attrs.href ).to.equal( '/test' );
    });
});
