import { mount } from 'avoriaz';
import Button from '../../../../../src/components/mobile/Button';

describe( 'Button.vue', () => {
    it( 'displays the correct button label', () => {
        const vm = mount( Button, {
            propsData: { id: 'test', label: 'sample' }
        });
        expect( vm.text() ).to.equal( 'sample' );
    });

    it( 'applies the correct ID', () => {
        const vm = mount( Button, {
            propsData: { id: 'test', label: 'sample' }
        });
        expect( vm.is( '#test' ) ).to.equal( true );
    });

    it( 'applies passed disabled attribute', () => {
        const spy = sinon.stub();
        const vm = mount( Button, {
            propsData: { id: 'test', label: 'sample', disabled: true, onClick: spy }
        });

        vm.trigger( 'click' );
        expect( spy.called ).to.equal( false );

        vm.setProps({ disabled: false });

        vm.trigger( 'click' );
        expect( spy.called ).to.equal( true );
    });

    it( 'applies the button type attribute passed', () => {
        const vm = mount( Button, {
            propsData: { id: 'test', label: 'sample' }
        });
        expect( vm.getAttribute( 'type' ) ).to.equal( 'button' );

        vm.setProps({ buttonType: 'submit' });
        expect( vm.getAttribute( 'type' ) ).to.equal( 'submit' );

        vm.setProps({ buttonType: 'reset' });
        expect( vm.getAttribute( 'type' ) ).to.equal( 'reset' );
    });
});
