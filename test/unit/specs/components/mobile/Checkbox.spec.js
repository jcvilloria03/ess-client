import { mount } from 'avoriaz';
import sinon from 'sinon';
import Checkbox from '@/components/mobile/Checkbox';

describe( 'Checkbox.vue', () => {
    it( 'applies passed attributes', () => {
        const wrapper = mount( Checkbox, {
            propsData: {
                id: 'checkbox',
                label: 'My Checkbox'
            }
        });

        const label = wrapper.find( 'label' )[ 0 ];

        expect( wrapper.contains( '#checkbox' ) ).to.equal( true );
        expect( label.text() ).to.equal( 'My Checkbox' );
    });

    it( 'emits focus event', () => {
        const wrapper = mount( Checkbox, {
            propsData: {
                id: 'checkbox',
                label: 'My Checkbox'
            }
        });

        const emitStub = sinon.stub( wrapper.vm, '$emit' );
        wrapper.find( 'input' )[ 0 ].trigger( 'focus' );
        expect( emitStub ).to.be.calledWith( 'onFocus' );
        emitStub.restore();
    });

    it( 'emits blur event', () => {
        const wrapper = mount( Checkbox, {
            propsData: {
                id: 'checkbox',
                label: 'My Checkbox'
            }
        });

        const emitStub = sinon.stub( wrapper.vm, '$emit' );
        wrapper.find( 'input' )[ 0 ].trigger( 'blur' );
        expect( emitStub ).to.be.calledWith( 'onBlur' );
        emitStub.restore();
    });
});
