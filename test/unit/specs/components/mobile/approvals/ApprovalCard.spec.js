import { mount } from 'avoriaz';
import ApprovalCard from '@/components/mobile/approvals/ApprovalCard';
import { auth } from '@/utils/Authentication';
import { employeeRequestService } from '@/utils/services/EmployeeRequestService';

/**
 * Get approval object for testing.
 * @return {Object}
 */
function getApproval() {
    return {
        request_type: 'leave_request',
        request_id: 1,
        request: {
            id: 1,
            leave_type: {
                name: 'Sick Leave'
            }
        },
        params: {
            owner: {
                name: 'Test owner'
            }
        },
        status: 'Pending'
    };
}

describe( 'ApprovalCard', () => {
    beforeEach( () => {
        auth.setUserData({ user_id: 1 });
    });

    afterEach( () => {
        auth.clearUserData();
    });

    it( 'renders some approval data', () => {
        const wrapper = mount( ApprovalCard, {
            propsData: {
                approval: getApproval(),
                messagesRoute: 'approvals.leave-request.messages'
            }
        });

        expect( wrapper.text() ).to.contain( 'Test owner' );
    });

    it( 'can approve request', ( done ) => {
        const approvedEventHandler = sinon.stub();
        sinon.stub( employeeRequestService, 'bulkApproveRequests' )
            .returns( Promise.resolve({ employee_requests: [
                {
                    ...getApproval(),
                    status: 'Approved'
                }
            ]}) );

        const wrapper = mount( ApprovalCard, {
            propsData: {
                approval: getApproval(),
                messagesRoute: 'approvals.leave-request.messages'
            }
        });

        wrapper.vm.$on( 'approved', approvedEventHandler );
        expect( wrapper.vm.approval.status ).to.equal( 'Pending' );

        wrapper.vm.approved();

        setTimeout( () => {
            wrapper.update();

            expect( approvedEventHandler.called ).to.be.true;

            employeeRequestService.bulkApproveRequests.restore();

            done();
        });
    });

    it( 'can decline request', ( done ) => {
        const declinedEventHandler = sinon.stub();
        sinon.stub( employeeRequestService, 'bulkDeclineRequests' )
            .returns( Promise.resolve({ requests: [
                {
                    ...getApproval(),
                    status: 'Declined'
                }
            ]}) );

        const wrapper = mount( ApprovalCard, {
            propsData: {
                approval: getApproval(),
                messagesRoute: 'approvals.leave-request.messages'
            }
        });

        wrapper.vm.$on( 'declined', declinedEventHandler );
        expect( wrapper.vm.approval.status ).to.equal( 'Pending' );

        wrapper.vm.declined();

        setTimeout( () => {
            wrapper.update();

            expect( declinedEventHandler.called ).to.be.true;

            employeeRequestService.bulkDeclineRequests.restore();

            done();
        });
    });
});
