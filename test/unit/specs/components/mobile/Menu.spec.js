import sinon from 'sinon';
import { shallow } from 'avoriaz';
import Menu from '@/components/mobile/Menu';
import { sockets } from '@/utils/Sockets';
import { auth } from '@/utils/Authentication';
import { notificationsService } from '@/utils/services/NotificationsService';

const USER_ID = 1;
let socketListener;

/**
 * Shallow wrap Menu component.
 */
function wrapShallow() {
    return shallow( Menu, {
        globals: {
            $echo: {
                private() {
                    return {
                        listen( event, callback ) {
                            socketListener = { event, callback };
                        }
                    };
                },
                leave: sinon.stub()
            }
        }
    });
}

describe( 'Menu', () => {
    beforeEach( () => {
        sinon.stub( sockets, 'init' );
        sinon.stub( notificationsService, 'fetchNotificationsData' ).returns( Promise.resolve([]) );
        sinon.stub( notificationsService, 'getUserNotificationsStatus' ).returns( Promise.resolve({ active: true }) );
        auth.setUserData({ user_id: USER_ID });
    });

    afterEach( () => {
        sockets.init.restore();
        notificationsService.fetchNotificationsData.restore();
        notificationsService.getUserNotificationsStatus.restore();
        auth.clearUserData();
    });

    it( 'initializes sockets plugin', () => {
        wrapShallow();

        expect( sockets.init.calledOnce ).to.be.true;
    });

    it( 'fetches users notifications', ( done ) => {
        wrapShallow();

        setTimeout( () => {
            expect( notificationsService.fetchNotificationsData.called ).to.be.true;

            done();
        });
    });

    it( 'fetches general status of users notifications for marking the bell', ( done ) => {
        wrapShallow();

        setTimeout( () => {
            expect( notificationsService.getUserNotificationsStatus.called ).to.be.true;

            done();
        });
    });

    it( 'can log user out', () => {
        sinon.stub( auth, 'logout' );

        const wrapper = wrapShallow();

        wrapper.vm.logMeOut();

        expect( auth.logout.calledOnce ).to.be.true;
        expect( wrapper.vm.$echo.leave.called ).to.be.true;

        auth.logout.restore();
    });

    it( 'can update users general notifications status', () => {
        sinon.stub( notificationsService, 'updateNotificationStatus' )
            .returns( Promise.resolve() );

        const wrapper = wrapShallow();

        wrapper.setData({ hasNewNotifications: true });

        wrapper.vm.toggleNotificationsAndUpdateStatus();

        expect( notificationsService.updateNotificationStatus.called ).to.be.true;
        expect( wrapper.data().hasNewNotifications ).to.be.false;

        notificationsService.updateNotificationStatus.restore();
    });

    it( 'listens for new notifications over sockets', () => {
        sinon.stub( notificationsService, 'updateNotificationStatus' )
            .returns( Promise.resolve() );

        const wrapper = wrapShallow();

        expect( wrapper.data().notifications ).to.be.an( 'array' ).to.have.lengthOf( 0 );
        expect( socketListener.event ).to.equal( 'UserNotificationReceived' );

        // Simulate event from the socket
        socketListener.callback({ notification: { id: 3, clicked: false }});

        expect( wrapper.data().notifications ).to.be.an( 'array' ).to.have.lengthOf( 1 );
        expect( wrapper.data().notifications[ 0 ]).to.be.an( 'object' ).to.eql({ id: 3, clicked: false });
        expect( wrapper.data().hasNewNotifications ).to.be.true;

        notificationsService.updateNotificationStatus.restore();
    });
});
