import sinon from 'sinon';
import { shallow } from 'avoriaz';
import router from '@/router';
import Approvals from '@/components/mobile/dashboard/Approvals';
import { employeeRequestService } from '@/utils/services/EmployeeRequestService';

describe( 'Approvals dashboard widget', () => {
    it( 'fetches and displays latest pending approvals for the user', ( done ) => {
        sinon.stub( employeeRequestService, 'getEmployeeApprovals' )
            .withArgs( 1, { statuses: ['pending']})
            .returns( Promise.resolve({
                data: [
                    {
                        id: 1,
                        request_type: 'leave_request',
                        request: {
                            leave_type: {
                                name: 'Sick Leave'
                            }
                        },
                        params: {
                            owner: {
                                name: 'John Doe'
                            }
                        }
                    },
                    {
                        id: 2,
                        request_type: 'leave_request',
                        request: {
                            leave_type: {
                                name: 'Sick Leave'
                            }
                        },
                        params: {
                            owner: {
                                name: 'Jane Doe'
                            }
                        }
                    }
                ],
                meta: {
                    pagination: {
                        total: 2
                    }
                }
            }) );

        const wrapper = shallow( Approvals, {
            globals: {
                $router: router
            }
        });

        setTimeout( () => {
            expect( wrapper.vm.totalCount ).to.equal( 2 );
            expect( wrapper.vm.approvals ).to.be.an( 'array' ).to.have.lengthOf( 2 );

            employeeRequestService.getEmployeeApprovals.restore();

            done();
        });
    });
});
