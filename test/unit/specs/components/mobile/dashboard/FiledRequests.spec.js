import { shallow } from 'avoriaz';
import router from '@/router';
import FiledRequests from '@/components/mobile/dashboard/FiledRequests';
import { employeeRequestService } from '@/utils/services/EmployeeRequestService';

describe( 'FiledRequests dashboard widget', () => {
    it( 'displays latest requests submitted by the user', ( done ) => {
        sinon.stub( employeeRequestService, 'getEmployeeRequests' ).returns( Promise.resolve({ data: [
            {
                id: 1,
                status: 'Pending',
                request_type: 'leave_request',
                request: {
                    leave_type: {
                        name: 'Sick Leave'
                    }
                },
                params: {
                    owner: {
                        name: 'Jane Doe'
                    }
                }
            },
            {
                id: 2,
                status: 'Approved',
                request_type: 'leave_request',
                request: {
                    leave_type: {
                        name: 'Sick Leave'
                    }
                },
                params: {
                    owner: {
                        name: 'Jane Doe'
                    }
                }
            },
            {
                id: 3,
                status: 'Declined',
                request_type: 'leave_request',
                request: {
                    leave_type: {
                        name: 'Sick Leave'
                    }
                },
                params: {
                    owner: {
                        name: 'Jane Doe'
                    }
                }
            }
        ]}) );

        const wrapper = shallow( FiledRequests, {
            globals: {
                $router: router
            }
        });

        setTimeout( () => {
            expect( wrapper.text() ).to.contain( 'Pending' );
            expect( wrapper.text() ).to.contain( 'Approved' );
            expect( wrapper.text() ).to.contain( 'Declined' );

            employeeRequestService.getEmployeeRequests.restore();

            done();
        });
    });
});
