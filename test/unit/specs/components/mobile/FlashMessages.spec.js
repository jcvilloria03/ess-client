import Vue from 'vue';
import FlashMessages, { STORAGE_KEY } from '@/components/mobile/flash-messages';

Vue.use( FlashMessages );

describe( 'FlashMessages', () => {
    afterEach( () => {
        Vue.prototype.$flashMessage.closeAll();
    });

    it( 'can queue flesh message to be showed later', ( done ) => {
        expect( document.querySelectorAll( '.sl-c-alert-list .sl-c-alert' ) ).to.have.lengthOf( 0 );

        Vue.prototype.$flashMessage.queue( 'Some message', 'success' );

        const message = JSON.parse( localStorage.getItem( STORAGE_KEY ) );

        setTimeout( () => {
            expect( document.querySelectorAll( '.sl-c-alert-list .sl-c-alert' ) ).to.have.lengthOf( 0 );

            expect( message ).to.eql({
                message: 'Some message',
                type: 'success',
                options: {
                    timeout: 3000,
                    showIcon: false,
                    closable: false
                }
            });

            done();
        });
    });

    it( 'can flash a message', ( done ) => {
        expect( document.querySelectorAll( '.sl-c-alert-list .sl-c-alert' ) ).to.have.lengthOf( 0 );

        Vue.prototype.$flashMessage.show( 'Some message', 'success' );

        Vue.nextTick( () => {
            const alertElements = document.querySelectorAll( '.sl-c-alert-list .sl-c-alert' );

            expect( alertElements ).to.have.lengthOf( 1 );
            expect( alertElements[ 0 ].textContent ).to.contain( 'Some message' );
            expect( alertElements[ 0 ].classList.contains( 'sl-c-alert--success' ) ).to.be.true;

            done();
        });
    });

    it( 'can show queued flesh message', ( done ) => {
        Vue.prototype.$flashMessage.queue( 'Some message', 'success' );
        Vue.prototype.$flashMessage.showQueued();

        setTimeout( () => {
            const alertElements = document.querySelectorAll( '.sl-c-alert-list .sl-c-alert' );

            expect( alertElements ).to.have.lengthOf( 1 );
            expect( alertElements[ 0 ].textContent ).to.contain( 'Some message' );
            expect( alertElements[ 0 ].classList.contains( 'sl-c-alert--success' ) ).to.be.true;

            expect( localStorage.getItem( STORAGE_KEY ) ).to.be.null;

            done();
        });
    });

    it( 'can close all flash messages', ( done ) => {
        Vue.prototype.$flashMessage.show( 'Some message', 'success' );

        Vue.prototype.$flashMessage.closeAll();

        Vue.nextTick( () => {
            expect( document.querySelectorAll( '.sl-c-alert-list .sl-c-alert' ) ).to.have.lengthOf( 0 );

            done();
        });
    });

    it( 'hides the message automaticaly after given timeout period', ( done ) => {
        expect( document.querySelectorAll( '.sl-c-alert-list .sl-c-alert' ) ).to.have.lengthOf( 0 );

        Vue.prototype.$flashMessage.show( 'Some message', 'success', { timeout: 20 });

        Vue.nextTick( () => {
            expect( document.querySelectorAll( '.sl-c-alert-list .sl-c-alert' ) ).to.have.lengthOf( 1 );

            setTimeout( () => {
                expect( document.querySelectorAll( '.sl-c-alert-list .sl-c-alert' ) ).to.have.lengthOf( 0 );

                done();
            }, 30 );
        });
    });
});
