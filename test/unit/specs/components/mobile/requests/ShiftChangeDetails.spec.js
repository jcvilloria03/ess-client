import { mount } from 'avoriaz';
import ShiftChangeDetails from '@/components/mobile/requests/ShiftChangeDetails';
import { auth } from '@/utils/Authentication';

/**
 * Get request with new assigned shift object for testing.
 * @return {Object}
 */
function getRequestNewAssignedShift() {
    return {
        id: 1,
        start_date: '2018-05-29',
        end_date: '2018-05-29',
        params: {
            company_id: 1,
            shift_change_dates: [{
                date: '2018-05-29',
                new_state: {
                    rest_day: false,
                    shifts_ids: [ 1, 11 ],
                    schedules_ids: [111]
                },
                old_state: {
                    shifts_ids: [1],
                    rest_days_ids: [1]
                }
            }]
        },
        shifts: [{
            id: 1,
            schedule_id: 111,
            employee_id: 1,
            schedule: {
                name: 'Test schedule',
                company_id: 1,
                start: '07:00',
                end: '16:00'
            }
        }, {
            id: 11,
            schedule_id: 112,
            employee_id: 1,
            schedule: {
                name: 'Schedule Monthly',
                company_id: 1,
                start: '07:00',
                end: '17:00'
            }
        }],
        schedules: [{
            id: 111,
            name: 'Test schedule',
            company_id: 1,
            start: '07:00',
            end: '16:00'
        }]
    };
}

/**
 * Get request with unassigned shift object for testing.
 * @return {Object}
 */
function getRequestUnassignedShift() {
    return {
        id: 1,
        start_date: '2018-05-29',
        end_date: '2018-05-29',
        params: {
            company_id: 1,
            shift_change_dates: [{
                date: '2018-05-29',
                new_state: {
                    rest_day: false,
                    shifts_ids: [],
                    schedules_ids: []
                },
                old_state: {
                    shifts_ids: [1],
                    rest_days_ids: []
                }
            }]
        },
        shifts: [{
            id: 1,
            schedule_id: 111,
            employee_id: 1,
            schedule: {
                name: 'Test schedule',
                company_id: 1,
                start: '07:00',
                end: '16:00'
            }
        }],
        schedules: []
    };
}

/**
 * Get request with new assigned rest day object for testing.
 * @return {Object}
 */
function getRequestNewAssignedRestDay() {
    return {
        id: 1,
        start_date: '2018-05-29',
        end_date: '2018-05-29',
        params: {
            company_id: 1,
            shift_change_dates: [{
                date: '2018-05-29',
                new_state: {
                    rest_day: true,
                    shifts_ids: [],
                    schedules_ids: []
                },
                old_state: {
                    shifts_ids: [1],
                    rest_days_ids: []
                }
            }]
        },
        shifts: [{
            id: 1,
            schedule_id: 111,
            employee_id: 1,
            schedule: {
                name: 'Test schedule',
                company_id: 1,
                start: '07:00',
                end: '16:00'
            }
        }],
        schedules: []
    };
}

/**
 * Get request with new assigned shift and no old shifts object for testing.
 * @return {Object}
 */
function getRequestNewAssignedShiftNoOldShifts() {
    return {
        id: 1,
        start_date: '2018-05-29',
        end_date: '2018-05-29',
        params: {
            company_id: 1,
            shift_change_dates: [{
                date: '2018-05-29',
                new_state: {
                    rest_day: false,
                    shifts_ids: [1],
                    schedules_ids: []
                },
                old_state: {
                    shifts_ids: [],
                    rest_days_ids: []
                }
            }]
        },
        shifts: [{
            id: 1,
            schedule_id: 111,
            employee_id: 1,
            schedule: {
                name: 'Test schedule',
                company_id: 1,
                start: '07:00',
                end: '16:00'
            }
        }],
        schedules: []
    };
}

describe( 'ShiftChangeDetails', () => {
    beforeEach( () => {
        auth.setUserData({ user_id: 1 });
    });

    afterEach( () => {
        auth.clearUserData();
    });

    it( 'renders shift change details data with added shift', () => {
        const wrapper = mount( ShiftChangeDetails, {
            propsData: {
                request: getRequestNewAssignedShift()
            }
        });

        expect( wrapper.text() ).to.contain(
            'OLD Schedule Test schedule Time 07:00 to 16:00  Schedule Rest Day' );
        expect( wrapper.text() ).to.contain(
            'NEW Schedule Test schedule Time 07:00 to 16:00  Schedule Schedule Monthly Time 07:00 to 17:00  Schedule Test schedule Time 07:00 to 16:00'
        );
    });

    it( 'renders shift change details data with unassigned shift', () => {
        const wrapper = mount( ShiftChangeDetails, {
            propsData: {
                request: getRequestUnassignedShift()
            }
        });

        expect( wrapper.text() ).to.contain( 'OLD Schedule Test schedule Time 07:00 to 16:00' );
        expect( wrapper.text() ).to.contain( 'NEW  ' );
    });

    it( 'renders shift change details data with new assigned rest day', () => {
        const wrapper = mount( ShiftChangeDetails, {
            propsData: {
                request: getRequestNewAssignedRestDay()
            }
        });

        expect( wrapper.text() ).to.contain(
            'OLD Schedule Test schedule Time 07:00 to 16:00' );
        expect( wrapper.text() ).to.contain( 'NEW    Schedule Rest Day' );
    });

    it( 'renders shift change details data with assigned shift and no old shifts', () => {
        const wrapper = mount( ShiftChangeDetails, {
            propsData: {
                request: getRequestNewAssignedShiftNoOldShifts()
            }
        });

        expect( wrapper.text() ).to.contain( 'OLD   ' );
        expect( wrapper.text() ).to.contain( 'NEW  Schedule Test schedule Time 07:00 to 16:00' );
    });
});
