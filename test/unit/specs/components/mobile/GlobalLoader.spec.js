import Vue from 'vue';
import Loader from '@/components/mobile/Loader';
import GlobalLoader from '@/components/mobile/GlobalLoader';

Vue.component( 'sal-mask', Loader );
Vue.use( GlobalLoader );

describe( 'Global Loader', () => {
    it( 'covers the screen with the Salarium mask', ( done ) => {
        const Component = Vue.extend({});
        const vm = new Component();

        vm.$loader.show();

        Vue.nextTick( () => {
            expect( document.querySelector( '.sl-c-mask' ) ).to.not.be.null;

            done();
        });
    });

    it( 'can be hidden', ( done ) => {
        const Component = Vue.extend({});
        const vm = new Component();

        vm.$loader.show();
        vm.$loader.hide();

        Vue.nextTick( () => {
            expect( document.querySelector( '.sl-c-mask' ) ).to.be.null;

            done();
        });
    });
});
