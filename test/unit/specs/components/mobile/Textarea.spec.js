import { mount } from 'avoriaz';
import sinon from 'sinon';
import Textarea from '@/components/mobile/Textarea';

describe( 'Textarea.vue', () => {
    it( 'applies passed attributes', () => {
        const wrapper = mount( Textarea, {
            propsData: {
                id: 'textarea',
                label: 'My Textarea'
            }
        });

        const label = wrapper.find( 'label' )[ 0 ];

        expect( wrapper.contains( '#textarea' ) ).to.equal( true );
        expect( label.text() ).to.equal( 'My Textarea' );
    });

    it( 'emits focus event', () => {
        const wrapper = mount( Textarea, {
            propsData: {
                id: 'textarea',
                label: 'My Textarea'
            }
        });

        const emitStub = sinon.stub( wrapper.vm, '$emit' );
        wrapper.find( 'textarea' )[ 0 ].trigger( 'focus' );
        expect( emitStub ).to.be.calledWith( 'onFocus' );
        emitStub.restore();
    });

    it( 'emits blur event', () => {
        const wrapper = mount( Textarea, {
            propsData: {
                id: 'textarea',
                label: 'My Textarea'
            }
        });

        const emitStub = sinon.stub( wrapper.vm, '$emit' );
        wrapper.find( 'textarea' )[ 0 ].trigger( 'blur' );
        expect( emitStub ).to.be.calledWith( 'onBlur' );
        emitStub.restore();
    });
});
