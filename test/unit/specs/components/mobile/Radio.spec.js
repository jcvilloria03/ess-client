import { mount } from 'avoriaz';
import sinon from 'sinon';
import Radio from '@/components/mobile/Radio';

describe( 'Radio.vue', () => {
    it( 'applies passed attributes', () => {
        const wrapper = mount( Radio, {
            propsData: {
                id: 'radio',
                label: 'My Radio'
            }
        });

        const label = wrapper.find( 'label' )[ 0 ];

        expect( wrapper.contains( '#radio' ) ).to.equal( true );
        expect( label.text() ).to.equal( 'My Radio' );
    });

    it( 'emits focus event', () => {
        const wrapper = mount( Radio, {
            propsData: {
                id: 'radio',
                label: 'My Radio'
            }
        });

        const emitStub = sinon.stub( wrapper.vm, '$emit' );
        wrapper.find( 'input' )[ 0 ].trigger( 'focus' );
        expect( emitStub ).to.be.calledWith( 'onFocus' );
        emitStub.restore();
    });

    it( 'emits blur event', () => {
        const wrapper = mount( Radio, {
            propsData: {
                id: 'radio',
                label: 'My Radio'
            }
        });

        const emitStub = sinon.stub( wrapper.vm, '$emit' );
        wrapper.find( 'input' )[ 0 ].trigger( 'blur' );
        expect( emitStub ).to.be.calledWith( 'onBlur' );
        emitStub.restore();
    });
});
