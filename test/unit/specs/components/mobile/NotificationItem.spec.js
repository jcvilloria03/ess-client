import { shallow } from 'avoriaz';
import NotificationItem from '@/components/mobile/NotificationItem';
import { notificationsService } from '@/utils/services/NotificationsService';
import { auth } from '@/utils/Authentication';
import router from '@/router';

const USER_ID = 1;

/**
 * Get test notification.
 * @return {Object}
 */
function getNotification() {
    return {
        id: 1,
        activity: {
            type: 'request_pending_approval',
            owner_id: USER_ID + 1,
            params: {
                request_id: 234,
                request_type: 'leave_request',
                owner_name: 'Some owner',
                employee_name: 'Some owner',
                leave_type: {
                    name: 'Sick Leave'
                }
            }
        },
        clicked: false,
        created_at: '2018-02-28 10:30:12'
    };
}

describe( 'NotificationItem', () => {
    beforeEach( () => {
        sinon.stub( auth, 'getUserId' ).returns( USER_ID );
        sinon.stub( auth, 'isEmailVerified' ).returns( true );
        sinon.stub( router, 'push' );
    });

    afterEach( () => {
        auth.getUserId.restore();
        auth.isEmailVerified.restore();
        router.push.restore();
    });

    it( 'can update notification status when it\'s clicked', ( done ) => {
        sinon.stub( notificationsService, 'updateNotificationClickedStatus' )
            .returns( Promise.resolve() );

        const wrapper = shallow( NotificationItem, {
            router,
            propsData: {
                notification: getNotification()
            }
        });

        expect( wrapper.vm.notification.clicked ).to.be.false;

        wrapper.vm.onClick();

        setTimeout( () => {
            expect( notificationsService.updateNotificationClickedStatus.called ).to.be.true;
            expect( wrapper.vm.notification.clicked ).to.be.true;
            expect( wrapper.vm.$router.push.calledWith({
                name: 'approvals.leave-request.details',
                params: { requestId: 234 }
            }) ).to.be.true;

            notificationsService.updateNotificationClickedStatus.restore();

            done();
        });
    });

    it( 'will not send request to update notification status if it has already been clicked', ( done ) => {
        sinon.stub( notificationsService, 'updateNotificationClickedStatus' )
            .returns( Promise.resolve() );

        const wrapper = shallow( NotificationItem, {
            router,
            propsData: {
                notification: {
                    ...getNotification(),
                    clicked: true
                }
            }
        });

        expect( wrapper.vm.notification.clicked ).to.be.true;

        wrapper.vm.onClick();

        setTimeout( () => {
            expect( notificationsService.updateNotificationClickedStatus.called ).to.be.false;
            expect( wrapper.vm.notification.clicked ).to.be.true;
            expect( wrapper.vm.$router.push.calledWith({
                name: 'approvals.leave-request.details',
                params: { requestId: 234 }
            }) ).to.be.true;

            notificationsService.updateNotificationClickedStatus.restore();

            done();
        });
    });

    it( 'formats the message', () => {
        const wrapper = shallow( NotificationItem, {
            propsData: {
                notification: {
                    ...getNotification(),
                    clicked: true
                }
            }
        });

        expect( wrapper.vm.message ).to.equal( 'Some owner filed a Sick Leave request.' );
        expect( wrapper.text() ).to.contain( 'Some owner filed a Sick Leave request.' );
    });
});
