import { mount } from 'avoriaz';
import Input from '../../../../../src/components/mobile/Input';

describe( 'Input.vue', () => {
    it( 'sets the correct default data', () => {
        const defaultData = Input.data();
        expect( defaultData.error ).to.equal( null );
    });

    it( 'applies the correct component ID', () => {
        const vm = mount( Input, {
            propsData: { id: 'test' }
        });

        expect( vm.contains( '#test' ) ).to.equal( true );
    });

    it( 'checks for errors if field is empty and required property is set to true', () => {
        const vm = mount( Input, {
            propsData: { id: 'test', required: true }
        });

        vm.vm.validate( '' );
        expect( vm.data().error ).to.equal( 'This field is required.' );
    });

    it( 'validates input length when min property has a value', () => {
        const vm = mount( Input, {
            propsData: { id: 'test', min: 3 }
        });

        vm.vm.validate( '12' );
        expect( vm.data().error ).to.equal( 'This field must be at least 3 characters.' );
    });

    it( 'validates input value when min property has a value', () => {
        const vm = mount( Input, {
            propsData: { id: 'test', min: 3, type: 'number' }
        });
        vm.vm.validate( '1' );
        expect( vm.data().error ).to.equal( 'This field requires a minimum value of 3.' );
    });

    it( 'validates input length when max property has a value', () => {
        const vm = mount( Input, {
            propsData: { id: 'test', max: 3 }
        });
        vm.vm.validate( '12345' );
        expect( vm.data().error ).to.equal( 'This field cannot exceed 3 characters.' );
    });

    it( 'validates input value when min property has a value', () => {
        const vm = mount( Input, {
            propsData: { id: 'test', max: 3, type: 'number' }
        });
        vm.vm.validate( '4' );
        expect( vm.data().error ).to.equal( 'This field accepts a maximum value of 3.' );
    });

    it( 'checks for valid email when type is set to email', () => {
        const vm = mount( Input, {
            propsData: { id: 'test', type: 'email' }
        });
        vm.vm.validate( 'hehehe' );
        expect( vm.data().error ).to.equal( 'Please input a valid email.' );
    });

    it( 'checks for valid number when type is set to number', () => {
        const vm = mount( Input, {
            propsData: { id: 'test', type: 'number' }
        });
        vm.vm.validate( 'hehehe' );
        expect( vm.data().error ).to.equal( 'Please input a valid number.' );
    });

    it( 'checks for spaces when type is set to password', () => {
        const vm = mount( Input, {
            propsData: { id: 'test', type: 'password' }
        });
        vm.vm.validate( '12 345' );
        expect( vm.data().error ).to.equal( 'Password contains invalid characters.' );
    });

    it( 'can accept custom validators', () => {
        const vm = mount( Input, {
            propsData: { id: 'test', validator() { return 'Invalid. Because I can.'; } }
        });
        vm.vm.validate( '' );
        expect( vm.data().error ).to.equal( 'Invalid. Because I can.' );
    });

    it( 'can accept custom onBlur handler', () => {
        const spy = sinon.spy();
        const vm = mount( Input, {
            propsData: { id: 'test', onBlur: spy }
        });
        vm.vm.validate( '12345' );
        expect( spy.calledOnce ).to.equal( true );
    });

    it( 'clears errors when validation passed', () => {
        const vm = mount( Input, {
            propsData: { id: 'test', type: 'number' }
        });
        vm.vm.validate( 'hehehe' );
        expect( vm.data().error ).to.equal( 'Please input a valid number.' );
        vm.vm.validate( '12345' );
        expect( vm.data().error ).to.equal( null );
    });
});
