import { mount } from 'avoriaz';
import sinon from 'sinon';
import Select from '@/components/mobile/Select';

describe( 'Select.vue', () => {
    it( 'applies passed attributes', () => {
        const wrapper = mount( Select, {
            propsData: {
                id: 'select',
                label: 'My Select',
                options: []
            }
        });

        const label = wrapper.find( 'label' )[ 0 ];

        expect( wrapper.contains( '#select' ) ).to.equal( true );
        expect( label.text() ).to.equal( 'My Select' );
    });

    it( 'emits focus event', () => {
        const wrapper = mount( Select, {
            propsData: {
                id: 'select',
                label: 'My Select',
                options: []
            }
        });

        const emitStub = sinon.stub( wrapper.vm, '$emit' );
        wrapper.find( 'select' )[ 0 ].trigger( 'focus' );
        expect( emitStub ).to.be.calledWith( 'onFocus' );
        emitStub.restore();
    });

    it( 'emits blur event', () => {
        const wrapper = mount( Select, {
            propsData: {
                id: 'select',
                label: 'My Select',
                options: []
            }
        });

        const emitStub = sinon.stub( wrapper.vm, '$emit' );
        wrapper.find( 'select' )[ 0 ].trigger( 'blur' );
        expect( emitStub ).to.be.calledWith( 'onBlur' );
        emitStub.restore();
    });
});
