// https://eslint.org/docs/user-guide/configuring

module.exports = {
    root: true,
    parser: 'babel-eslint',
    parserOptions: {
        ecmaVersion: 6,
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true
        }
    },
    'globals': {
        '$': true,
        'jQuery': true
    },
    env: {
        browser: true,
        node: true,
        es6: true
    },
    extends: 'airbnb-base',
    // required to lint *.vue files
    plugins: [
        'html',
        'jsx-a11y'
    ],
    // check if imports actually resolve
    'settings': {
        'import/resolver': {
            'webpack': {
                'config': 'build/webpack.base.conf.js'
            }
        }
    },
    // add your custom rules here
    'rules': {
        "import/no-dynamic-require": 1,
        'import/extensions': ['error', 'always', { 'js': 'never', 'vue': 'never' }],
        'import/no-extraneous-dependencies': ['error', { 'optionalDependencies': ['test/unit/index.js'] }],
        'no-debugger': 2,
        'array-bracket-spacing': [ 'error', 'always', { 'singleValue': false, 'objectsInArrays': false, 'arraysInArrays': false } ],
        'arrow-parens': [ 'error', 'always' ],
        'arrow-body-style': [ 2, 'as-needed' ],
        'comma-dangle': [ 2, 'never' ],
        'comma-spacing': [ 'error', { 'before': false, 'after': true } ],
        'comma-style': [ 'error', 'last' ],
        'class-methods-use-this': 0,
        'computed-property-spacing': [ 'error', 'always' ],
        'import/imports-first': 0,
        'import/newline-after-import': 0,
        'import/no-named-as-default': 0,
        'import/no-unresolved': 2,
        'import/prefer-default-export': 0,
        'indent': [ 2, 4, { 'SwitchCase': 1 } ],
        'jsx-a11y/aria-props': 2,
        'jsx-a11y/heading-has-content': 0,
        'jsx-a11y/label-has-for': 2,
        'jsx-a11y/mouse-events-have-key-events': 2,
        'jsx-a11y/role-has-required-aria-props': 2,
        'jsx-a11y/role-supports-aria-props': 2,
        'max-len': 0,
        'newline-per-chained-call': 0,
        'no-console': 0,
        'no-duplicate-imports': 'error',
        'no-extra-semi': 'error',
        'no-lonely-if': 'error',
        "no-mixed-operators": [
            0,
            {
                "groups": [
                    [ "+", "-", "*", "/", "%", "**" ],
                    [ "&", "|", "^", "~", "<<", ">>", ">>>" ],
                    [ "==", "!=", "===", "!==", ">", ">=", "<", "<=" ],
                    [ "&&", "||" ],
                    [ "in", "instanceof" ]
                ],
                "allowSamePrecedence": true
            }
        ],
        "no-multiple-empty-lines": [ "error", { "max": 1 } ],
        "no-restricted-syntax": [ "error", "ForInStatement", "LabeledStatement", "WithStatement" ],
        "no-trailing-spaces": "error",
        "no-underscore-dangle": 0,
        "no-unneeded-ternary": "error",
        "no-nested-ternary": 0,
        "no-unused-expressions": 0,
        "no-use-before-define": 2,
        "no-var": "error",
        "object-curly-spacing": [ "error", "always", { "arraysInObjects": false, "objectsInObjects": false } ],
        "prefer-arrow-callback": "error",
        "prefer-const": "error",
        "prefer-template": 2,
        "require-jsdoc": [ "error", { "require": { "FunctionDeclaration": true, "MethodDefinition": true, "ClassDeclaration": true } } ],
        "semi-spacing": [ "error", { "before": false, "after": true } ],
        "space-in-parens": [ "error", "always", { "exceptions": [ "{}", "[]", "empty" ] } ],
        "no-param-reassign": [ "error", { "props": false } ]
    }
}


