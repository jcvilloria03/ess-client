#!/bin/bash
cd /srv

chown slmuser:slmuser ess-client

cd /srv/ess-client

# copy dev .env file from S3 check version. recheck. and check.
#aws s3 cp s3://codedeploy-us-west-2-essv3-env-dev/.env .env

# copy .env file from S3
if [ "$DEPLOYMENT_GROUP_NAME" == "ess-dev" ]
then
    aws s3 cp s3://codedeploy-us-west-2-essv3-env-dev/ess-client-dev.env .env
fi

if [ "$DEPLOYMENT_GROUP_NAME" == "ess-client-stg" ]
then
    aws s3 cp s3://v3-stg-codedeploy-env/v3-stg-ess-client.env .env
fi

if [ "$DEPLOYMENT_GROUP_NAME" == "ess-client-ft" ]
then
    aws s3 cp s3://v3-stg-codedeploy-env/v3-ft-ess-client.env .env
fi

if [ "$DEPLOYMENT_GROUP_NAME" == "ess-client-demo" ]
then
    aws s3 cp s3://v3-stg-codedeploy-env/v3-demo-ess-client.env .env
fi

rm -rf node_modules/
